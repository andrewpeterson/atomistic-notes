main:
	pdflatex --shell-escape book
	bibtex book
	pdflatex --shell-escape book
	pdflatex --shell-escape book

clean:
	rm -rf book.aux book.log book.out book.toc _minted-book book.bbl book.blg chapter.* _minted-chapter topics/*.aux

fast:
	pdflatex --shell-escape book

chapter:
	pdflatex -shell-escape -jobname=chapter "\includeonly{topics/metals}\input{book}"
