import numpy as np
from matplotlib import pyplot

fig, ax = pyplot.subplots()
fig.subplots_adjust(top=0.99, right=0.99)

with open('dihedralscan.txt', 'r') as f:
    lines = f.read().splitlines()
degrees = np.array([float(line.split()[0]) for line in lines])
energies = np.array([float(line.split()[1]) for line in lines])

ax.plot(degrees, energies - min(energies))
ax.fill_between(degrees, energies - min(energies), color='0.8')
ax.set_xlabel('dihedral angle ($^\circ$)')
ax.set_ylabel('$E_\mathrm{pot}$, eV')
ax.set_ylim(bottom=0.)
ax.set_xlim(min(degrees), max(degrees))
fig.savefig('out.pdf')
