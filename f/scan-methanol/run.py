#!/usr/bin/env python3
#SBATCH --time=02:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=1G


import numpy as np
import os
from ase.build import molecule
from ase.calculators.nwchem import NWChem
import ase.io
from ase.optimize import BFGS
from ase.constraints import FixInternals

atoms = molecule('CH3OH')
calculator = NWChem(label='calc/nwchem',
                    dft={'xc': 'B3LYP',
                         'maxiter': 20000,
                         'convergence': {'damp': 99}, },
                    basis='6-31+G*')
atoms.set_calculator(calculator)

# Start with a relaxed structure before scanning.
os.mkdir('optimizations')
label = 'optimizations/initial'
opt = BFGS(atoms, trajectory=label + '.traj', logfile=label + '.log')
opt.run(fmax=0.01)

# Output will be written to a "trajectory" file.
traj = ase.io.Trajectory('dihedralscan.traj', 'w')
indices = (2, 0, 1, 3)  # Atom indices of the dihedral.
angles = np.arange(180., -180., -1.)  # Dihedral angles to analyze.

for angle in angles:
    atoms.set_dihedral(*indices, angle)
    dihedral = [atoms.get_dihedral(*indices) * np.pi / 180., indices]
    constraint = FixInternals(dihedrals=[dihedral])
    atoms.set_constraint(constraint)
    label = 'optimizations/qn{:d}'.format(int(angle))
    opt = BFGS(atoms, trajectory=label + '.traj', logfile=label + '.log')
    opt.run(fmax=0.01)
    traj.write(atoms)
    with open('dihedralscan.txt', 'a') as f:
        f.write('{:10.5f} {:10.5f}\n'.format(angle,
                                             atoms.get_potential_energy()))
