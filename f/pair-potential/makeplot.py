#!/usr/bin/env python

import matplotlib
from matplotlib import pyplot
import numpy as np


class LJ:
    """Lennard Jones potential."""
    def __init__(self, epsilon, sigma):
        self._epsilon = epsilon
        self._sigma = sigma

    def __call__(self, r):
        e = self._epsilon
        s = self._sigma
        return 4. * e * ((s / r)**12 - (s / r)**6)


get_potential_energy = LJ(1.0, 1.0)

positions = np.linspace(0.95, 3., num=1000)
potential_energies = [get_potential_energy(r) for r in positions]

matplotlib.rcParams['font.size'] = 25.
fig = pyplot.figure()
ax = fig.add_subplot(111)
ax.plot(positions, potential_energies)
ax.set_xticks([])
ax.set_yticks([])
ax.set_xlabel('$r$')
ax.set_ylabel('$E_\mathrm{pot}$')
ax.set_ylim(-1.5, 2.)
fig.savefig('out.pdf')
