#!/usr/bin/env python
"""
Starter script for matplotlib figures.
"""

import numpy as np
from matplotlib import pyplot


def makefig(figsize=(5., 5.), nrows=1, ncols=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):
    """
    figsize: canvas size
    nrows, ncols: number of horizontal and vertical images
    lm, rm, bm, tm, hg, vg: margins and "gaps"
    hr is horizontal ratio, and can be fed in as for example
    (1., 2.) to make the second axis twice as wide as the first.
    [same for vr]
    """
    nv, nh = nrows, ncols
    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv

    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - (nv - 1.) * vg) * np.array(vr)

    fig = pyplot.figure(figsize=figsize)
    axes = []
    bottompoint = 1. - tm
    for iv, v in enumerate(range(nv)):
        leftpoint = lm
        bottompoint -= axheights[iv]
        for ih, h in enumerate(range(nh)):
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            leftpoint += hg + axwidths[ih]
        bottompoint -= vg
    return fig, axes


fig, axes = makefig(figsize=(8., 4.), ncols=3, hr=(1, 1, 2.0), hg=0.05)

class CoulombPotential:
    """Function creator for Coulomb potential."""
    def __init__(self, charge, offset, center):
        self.charge = charge
        self.offset = offset
        self.center = center

    def __call__(self, x):
        newx = abs(x - self.center)
        potential = - self.charge / newx
        return potential - self.offset

atom1 = CoulombPotential(1., 0., 0.)
atom2 = CoulombPotential(3., -1., 0.)
atom2shift = CoulombPotential(3., -1., 2.)

numpoints = 200

xs = np.linspace(-3., 3., num=numpoints)
ys = atom1(xs)
axes[0].plot(xs, ys)
axes[0].fill_between(xs, ys, [-30]*len(ys), color='#8bc3e7')

xs = np.linspace(-3., 3., num=numpoints)
ys = atom2(xs)
axes[1].plot(xs, ys)
axes[1].fill_between(xs, ys, [-30]*len(ys), color='#8bc3e7')

xs = np.linspace(-3., 9., num=numpoints)
ys = atom1(xs) + atom2shift(xs)
axes[2].plot(xs, ys)
axes[2].fill_between(xs, ys, [-30]*len(ys), color='#8bc3e7')


for ax in axes:
    ax.set_ylim(-5., 2.)
    ax.set_xlim(-3., 3.)
    ax.set_xticks([])
    ax.set_yticks([])
axes[2].set_xlim(-3., 9.)

axes[0].set_title('Atom A')
axes[1].set_title('Atom B')
axes[2].set_title('Atom A + Atom B')

fig.savefig('out.pdf')
