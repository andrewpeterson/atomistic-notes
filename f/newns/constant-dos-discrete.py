
import numpy as np
from matplotlib import pyplot
from matplotlib import rcParams
rcParams['font.size'] = 40.


def get_hybrids(E1, E2):
    """Return the hybridization energy, based on beta and ignoring S."""
    Emin, Emax = sorted((E1, E2))
    delta = Emax - Emin
    if delta < tol:
        return Emin - beta, Emin + beta
    return Emin - beta**2 / delta, Emax + beta**2 / delta


def plot_level(E, zone, color='k'):
    """Plot an energy level."""
    ax.plot([zone, zone + 0.8], [E]*2, lw=2,
            color=color)


def connect_levels(E1, E2, zone1, color='k'):
    """Connect two energy levels."""
    ax.plot([zone1 + 0.8, zone1 + 1.0], [E1, E2], lw=1,
            color=color)

fig = pyplot.figure()
ax = fig.add_subplot(111)

beta = 1.  # resonance integral
tol = 0.05  # tolerance for determining two energy levels are identical
Ea = 0.  # adsorbate energy

Ems = np.linspace(-5., 5., num=9)

# Plot end states.
plot_level(Ea, 0)
for Em in Ems:
    plot_level(Em, 2)

# Plot hybridization.
for Em in Ems:
    El, Eh = get_hybrids(Ea, Em)
    plot_level(El, 1, '0.5')
    plot_level(Eh, 1, '0.5')


def highlight(Em, color):
    """Examples highlighted."""
    El, Eh = get_hybrids(Ea, Em)
    plot_level(El, 1, color)
    plot_level(Eh, 1, color)
    connect_levels(Ea, El, 0, color)
    connect_levels(Ea, Eh, 0, color)
    connect_levels(El, Em, 1, color)
    connect_levels(Eh, Em, 1, color)

highlight(Ems[3], 'g')
highlight(Ems[8], 'r')
highlight(Ems[4], 'b')

ax.set_yticks([])
ax.set_xticks([0.4, 1.4, 2.4])
ax.set_xticklabels(['$\\varepsilon_\mathrm{a}$',
                    '$\\varepsilon_\mathrm{hybrid}$',
                    '$\\varepsilon_\mathrm{band}$',
                    ])

fig.savefig('constant-dos-discrete.pdf')
