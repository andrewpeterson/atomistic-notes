#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot


def makefig(figsize=(5., 5.), nrows=1, ncols=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):
    """
    figsize: canvas size
    nrows, ncols: number of horizontal and vertical images
    lm, rm, bm, tm, hg, vg: margins and "gaps"
    hr is horizontal ratio, and can be fed in as for example
    (1., 2.) to make the second axis twice as wide as the first.
    [same for vr]
    """
    nv, nh = nrows, ncols
    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv

    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - (nv - 1.) * vg) * np.array(vr)

    fig = pyplot.figure(figsize=figsize)
    axes = []
    bottompoint = 1. - tm
    for iv, v in enumerate(range(nv)):
        leftpoint = lm
        bottompoint -= axheights[iv]
        for ih, h in enumerate(range(nh)):
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            leftpoint += hg + axwidths[ih]
        bottompoint -= vg
    return fig, axes


def get_dos(energy):
    """Returns the density of states at a specified energy.
    Array-compatible."""
    return delta0 / np.pi / ((energy - ea)**2 + delta0**2)

ea = 3.
delta0 = 1.
energies = np.linspace(-10., 10., 1000)
dos = get_dos(energies)

fig, axes = makefig(figsize=(3., 6.), rm=0.01, tm=0.01, bm=0.05, lm=0.09)
ax = axes[0]

ax.plot(dos, energies, color='blue')
ax.plot([0.1] * len(energies), energies, color='0.5')
ax.plot([0., 0.05], [ea, ea], color='red')

ax.text(0.105, -7., 'metal dos', color='gray')
ax.text(0.01, ea - 0.5, 'free atom energy', color='red')
ax.text(0.12, ea + 1.7, 'adsorbate-projected\ndos', color='blue')

ax.set_xticks([])
ax.set_yticks([])
ax.set_xlabel('density of states')
ax.set_ylabel('energy')

fig.savefig('constant-dos.pdf')
