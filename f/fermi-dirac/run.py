import numpy as np
from scipy import constants
from scipy.interpolate import interp1d
from matplotlib import pyplot
from matplotlib.patches import FancyArrowPatch


def makefig():
    """Create figure and axes."""
    figsize = (10., 3.)
    n = 3  # images on each side of arrow
    lm, rm, bm, tm = 0.08, 0.01, 0.20, 0.11  # margins
    ig, ag = 0.01, 0.2  # image and arrow gaps
    w = (1. - 2. * (n - 1) * ig - ag - lm - rm) / (2 * n)  # ax width
    h = 1. - bm - tm  # ax height
    le = lm + n * w + (n - 1) * ig  # left axes end
    rs = le + ag  # right axes start
    fig = pyplot.figure(figsize=figsize)
    occax, dosax = [], []
    for index in range(3):
        occax.append(fig.add_axes([lm + index * (w + ig), bm, w, h]))
        dosax.append(fig.add_axes([rs + index * (w + ig), bm, w, h]))
    # Add a blocky arrow
    arrow = FancyArrowPatch((le + 0.1 * ag, 0.5), (rs - 0.5 * ag, 0.5),
                            transform=fig.transFigure,
                            figure=fig,
                            arrowstyle='simple',
                            fc='black',
                            ec='black',
                            mutation_scale=30,
                            linewidth=5)
    fig.patches.append(arrow)
    return fig, occax, dosax


def build_arbitrary_dos():
    """Just creates an arbitrary DOS by stacking Gaussians.
    Each Gaussian is specified by mean, standard deviation, and scale,
    where scale is the height relative to 1."""
    gaussians = [(-1.2, 0.4, 0.3), (-0.7, 0.2, 0.6), (-0.25, 0.15, .5),
                 (0.15, 0.2, .6), (0.60, 0.18, 0.7), (1.05, 0.21, .7)]
    DOSs = np.zeros_like(energies)
    for parameters in gaussians:
        mean, stdev, scale = parameters
        DOSs += scale * np.exp(-0.5 * ((energies - mean) / stdev)**2)
    return DOSs


fig, occax, dosax = makefig()

delta_e = 1.  # eV, energy range to plot
mu = 0.  # eV, Fermi level
Ts = [0., 300., 1000.]  # K, temperatures to plot
kB = constants.k / constants.eV  # Boltzmann constant
lw = 2.  # linewidth

energies = np.linspace(-delta_e, +delta_e, num=10000)
DOSs = build_arbitrary_dos()
dos0 = interp1d(x=energies, y=DOSs)(0.)  # DOS at Fermi level

first = True
for T, oax, dax in zip(Ts, occax, dosax):
    occupations = [1. / (1. + np.exp((e - mu) / (kB * T))) for e in energies]
    oax.plot(occupations, energies, label=f'$T={T:.0f}$ K', lw=lw, zorder=3)
    oax.fill_betweenx(energies, occupations, color='0.8')
    oax.plot([0., 1.], [0.] * 2, ':')
    oax.set_ylim([energies[0], energies[-1]])
    oax.set_xlim([0., 1.1])
    dax.plot(DOSs, energies, zorder=2, lw=3, color='k')
    fillwidths = occupations * DOSs
    dax.fill_betweenx(energies, fillwidths, color='0.8', zorder=1)
    dax.plot([0., dos0], [0.] * 2, '-', lw=0.5, color='C1')
    oax.set_title(f'{T:.0f} K')
    dax.set_title(f'{T:.0f} K')
    dax.set_xlim([0., 0.8])
    dax.set_ylim([energies[0], energies[-1]])
    oax.set_xlabel('occupations,\nfractional')
    dax.set_xlabel('DOS,\na.u.')
    dax.set_xticks([0.])
    if not first:
        oax.set_yticklabels([])
        dax.set_yticklabels([])
    else:
        oax.set_ylabel(r'$\varepsilon - \mu$, eV')
        dax.set_ylabel(r'$\varepsilon - \mu$, eV')
    first = False

fig.savefig('out.pdf')
