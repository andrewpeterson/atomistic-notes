"""This script shows an example of how to build, view and save a couple of
simple systems in ASE. This can be run directly in python or through an
interface like the ipython notebook."""

from ase.build import molecule, nanotube
from ase.visualize import view

# Ethanol example.
atoms = molecule('CH3CH2OH')
print(atoms.positions)  # print cartesian coordinates, in A, to screen
print(atoms.get_chemical_symbols())  # print the element order
view(atoms)  # display the atoms
# Write to disk. Note dozens of formats are available.
atoms.write('ethanol.eps', rotation='45y')  # An image file.
atoms.write('ethanol.png', rotation='45y')  # An image file.
atoms.write('ethanol.html')  # Open with a web browser.
atoms.write('ethanol.traj')  # Open with ASE.

# Nanotube example.
atoms = nanotube(m=3, n=3, length=4)
view(atoms)
