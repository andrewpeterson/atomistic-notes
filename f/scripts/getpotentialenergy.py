#!/usr/bin/env python3
#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=1G

from ase.build import molecule
from ase.calculators.nwchem import NWChem


atoms = molecule('CH3CH2OH')
calculator = NWChem(label='calc/nwchem',
                    dft={'xc': 'B3LYP',
                         'maxiter': 2000},
                    basis='6-31+G*')
atoms.set_calculator(calculator)

energy = atoms.get_potential_energy()  # <-- This triggers DFT!

# Write the energy to a file called 'energy.txt'.
with open('energy.txt', 'w') as f:
    f.write(str(energy))
