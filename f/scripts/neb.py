#!/usr/bin/env python

import numpy as np
from ase.build import fcc100
from ase.calculators.emt import EMT
from ase import Atom
from ase.constraints import FixAtoms
from ase.optimize import BFGS
from ase.mep import NEB, interpolate
from ase.vibrations import Vibrations


###############################################
# Build and relax the initial and final states.
###############################################

# We'll build a somewhat silly-looking surface, just to introduce
# some assymetry and make the problem more interesting.
lattice_constant = 4.2  # Angstroms
initial = fcc100('Pt', (3, 3, 3), a=lattice_constant, vacuum=10.)
initial[19].symbol = 'Ni'
initial[21].symbol = 'Au'
# Initially place the adsorbate 2 Angstroms above the mean
# position of the three neighbors.
neighbors = [18, 19, 21, 22]
site = np.average([initial[index].position for index in neighbors],
                  axis=0)
site += (0., 0., 2.)
adsorbate = Atom('Cu', position=site)
initial.append(adsorbate)
initial.calc = EMT()
constraint = FixAtoms(indices=[atom.index for atom in initial if
                               atom.tag >= 2])
initial.set_constraint(constraint)
opt = BFGS(initial, trajectory='opt-initial.traj', logfile='opt-initial.log')
opt.run(fmax=0.01)

final = initial.copy()
neighbors = [19, 20, 22, 23]
site = np.average([final[index].position for index in neighbors],
                  axis=0)
site += (0., 0., 2.)
final[-1].position = site  # Move it instead of add it.
final.calc = EMT()
opt = BFGS(final, trajectory='opt-final.traj', logfile='opt-final.log')
opt.run(fmax=0.01)


###############################################
# Create and run the nudged elastic band (NEB).
###############################################

# Make a list of images (the "band") with the initial as first,
# the final as last, and copies of the initial in between.
# We will update the positions later.
# It is important that each image be an independent atoms object;
# here, we do this by making copies and attaching a fresh calculator
# to each.
images = [initial]

for index in range(6):
    image = initial.copy()
    image.calc = EMT()
    images.append(image)

images.append(final)

# Update the interior images by interpolating between endstates.
interpolate(images)

# Create the NEB, and call the optimizer.
neb = NEB(images)
opt = BFGS(neb, trajectory='neb.traj', logfile='neb.log')
opt.run(fmax=0.01)

# Turn on climbing image, and re-run.
neb.climb = True
opt = BFGS(neb, trajectory='neb-climb.traj', logfile='neb-climb.log')
opt.run(fmax=0.01)

###################################################################
# Perform normal-mode analyses on initial, final, and saddle point.
###################################################################

vib = Vibrations(initial, name='vib-initial', indices=[-1])
vib.run()
vib.summary(log='vib-initial.txt')
vib.write_mode()

vib = Vibrations(final, name='vib-final', indices=[-1])
vib.run()
vib.summary(log='vib-final.txt')
vib.write_mode()

saddle = images[3]
vib = Vibrations(saddle, name='vib-saddle', indices=[-1])
vib.run()
vib.summary(log='vib-saddle.txt')
vib.write_mode()

# We can verify we have a saddle point by:
# 1. All forces are ~zero.
# 2. One imaginary frequency.
# Let's check the max force on any atom:
atoms = images[3]
forces = atoms.get_forces()
forcemagnitudes = (forces**2).sum(axis=1)
print(forcemagnitudes.max())
