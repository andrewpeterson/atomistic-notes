#!/usr/bin/env python

from ase.build import molecule

atoms = molecule('CH3CH2OH')
moments = atoms.get_moments_of_inertia()
print(moments)

atoms = molecule('CO2')
moments = atoms.get_moments_of_inertia()
print(moments)
