#!/usr/bin/env python
#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=1G

from ase import Atoms
from ase.optimize import BFGS
from ase.calculators.nwchem import NWChem
from ase.vibrations import Vibrations

atoms = Atoms('H2', positions=[(0, 0, 0), (0, 0, 2.0)])

calculator = NWChem(label='calc/nwchem',
                    maxiter=200,
                    xc='B3LYP',
                    basis='6-31+G*')
atoms.set_calculator(calculator)

opt = BFGS(atoms, trajectory='qn.traj', logfile='qn.log')
opt.run(fmax=0.01)

vib = Vibrations(atoms)
vib.run()
vib.summary(log='vibrations.txt')  # Prints text summary.
vib.write_mode()  # Writes trajectories you can view with ase-gui.
