#!/usr/bin/env python

from ase import Atom, Atoms
from ase.build.surface import fcc110
from ase.visualize import view
from ase.calculators.emt import EMT
from ase.optimize import QuasiNewton, FIRE, BFGS, MDMin
from ase.constraints import FixAtoms

# Build the atoms.
atoms = fcc110(symbol='Au', size=(3, 3, 3), vacuum=10.)
adsorbate = Atoms([Atom('Pt', atoms[22].position + (0.1, 0.2, 2.0))])
atoms.extend(adsorbate)
view(atoms)

# Freeze the bottom layer of Au.
fixed_atoms = [atom.index for atom in atoms if atom.tag == 3]
atoms.set_constraint(FixAtoms(indices=fixed_atoms))

# Attach a calculator. EMT is a cheap & dirty method.
calculator = EMT()
atoms.set_calculator(calculator)

# Check the energy before optimization.
print(atoms.get_potential_energy())

# Optimize. Try changing MDMin to other methods (BFGS, QuasiNewton..)
# and check to see the number of steps or found minimuum changes.
dynamics = MDMin(atoms, logfile='qn-con.log', trajectory='qn-con.traj')
dynamics.run(fmax=0.05)

# Check the energy again after optimization.
print(atoms.get_potential_energy())
