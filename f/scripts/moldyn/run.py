from ase import Atom
from ase.build import fcc100
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.calculators.emt import EMT
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.verlet import VelocityVerlet
from ase import units

# We'll build a toy system, starting in kind of a high-energy initial state
# so we see some action.
atoms = fcc100('Cu', (3, 3, 3), vacuum=20)
ads = Atom('Pt', atoms[22].position + (0., 0., 3.))
atoms.append(ads)

constraint = FixAtoms(indices=[atom.index for atom in atoms if atom.tag == 3])
atoms.set_constraint(constraint)
atoms.calc = EMT()

# Give the atoms an initial velocity distribution:
MaxwellBoltzmannDistribution(atoms, temperature_K=1000.)

# Set up and run the molecular dynamics.
dyn = VelocityVerlet(atoms, dt=5.0 * units.fs,
                     trajectory='md.traj',
                     logfile='md.log')
dyn.run(100)
