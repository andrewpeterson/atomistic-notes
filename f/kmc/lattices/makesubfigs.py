#!/usr/bin/env python

from matplotlib import pyplot

class OneDimLatticePlot:
    """Creates a one-dimensional lattice plot, ready to put adsorbates at
    lattice positions."""

    def __init__(self, size=9):
        """size is the number of lattice locations."""
        self._size = size

    def _initialize_chart(self):
        size = self._size
        squaresize = (1., 1.)  # inches
        lm = 0.20
        rm = 0.20
        tm = 0.20
        bm = 0.25
        figwidth = lm + size * squaresize[0] + rm
        figheight = bm + squaresize[1] + tm
        self._fig = pyplot.figure(figsize=(figwidth, figheight))
        self._ax = self._fig.add_axes((lm/figwidth, bm/figheight,
                                      (figwidth-lm-rm)/figwidth,
                                      (figheight - bm - tm) / figheight))
        for x in range(size - 1):
            self._ax.plot([x + .5] * 2, [0., 1.], 'k-')


    def make(self, filename, values=None):
        """Saves the figure with associated values in the lattice cells."""
        self._initialize_chart()
        if values == None:
            values = []
        for index, value in enumerate(values):
            self._ax.text(index, 0.5, value, ha='center', va='center',
                          fontsize=36., color='blue')
        self._ax.set_xlim(-0.5, self._size - .5)
        self._ax.set_ylim(0., 1.)
        self._ax.set_yticks([])
        self._ax.set_xticks(range(self._size))
        self._fig.savefig(filename)



odp = OneDimLatticePlot()
odp.make('outcomes/blank.svg')
odp.make('outcomes/00.svg', ['SH', 'H', '', '', '', '', '', '', ''])
odp.make('outcomes/01.svg', ['', 'SH', 'H', '', '', '', '', '', ''])
odp.make('outcomes/08.svg', ['H', 'SH', '', '', '', '', '', '', ''])
odp.make('outcomes/15.svg', ['', '', '', '', '', '', '', 'H', 'SH'])
odp.make('outcomes/16.svg', ['H', 'H', '', '', '', '', '', '', ''])
odp.make('outcomes/23.svg', ['', '', '', '', '', '', '', 'H', 'H'])


