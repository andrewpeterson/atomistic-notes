#!/usr/bin/env python

from matplotlib import pyplot

fig = pyplot.figure(figsize=(6., 2.))
lm = 0.02
rm = 0.02
bm = 0.22
tm = 0.22
axwidth = 1. - lm - rm
axheight = 1. - bm - tm
ax = fig.add_axes((lm, bm, axwidth, axheight))
ax2 = ax.twiny()

left = 0.
for index in range(16):
    ax.barh(0., 5., left=left, color='purple')
    ax.text(left + 2.5, 0.4, '%02i' % index,
            ha='center', va='center')
    left += 5.
    print('Zone %02i is (%.3f, %.3f)' %
          (index, left - 5., left))
for index in range(8):
    ax.barh(0., 2., left=left, color='yellow')
    if index % 2 == 0:
        ax.text(left + 1.0, 0.5, '%02i' % (index + 16),
                ha='center', va='center', fontsize=8.)
    else:
        ax.text(left + 1.0, 0.3, '%02i' % (index + 16),
                ha='center', va='center', fontsize=8.)
    left += 2.
    print('Zone %02i is (%.3f, %.3f)' %
          (index + 16, left - 2., left))

ktotal = left

ax.set_yticks([])
ax.set_xlabel('$k$, s$^{-1}$')

ax2_tick_locations = [0., ktotal / 2., ktotal]
ax2.set_xticks(ax2_tick_locations)
ax2.set_xticklabels([0., 0.5, 1.])

xlim = ax.get_xlim()
ax2.set_xlim(xlim)
ax.set_frame_on(False)
ax2.set_frame_on(False)

fig.savefig('outcomes/choice-probability.svg')
