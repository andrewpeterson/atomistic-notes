import numpy as np
from matplotlib import pyplot


fig, ax = pyplot.subplots()

k = 0.5

xs = np.linspace(0.4, 0.6)
ys = k * np.exp(-k * xs)
ax.fill_between(xs, ys, color='C1')
ax.plot([xs[0]] * 2, [0., ys[0]], 'k:')
ax.plot([xs[-1]] * 2, [0., ys[-1]], 'k:')

xs = np.linspace(0., 4. / k)
ys = k * np.exp(-k * xs)
label = r'$k_{\mathrm{tot}} = $'
label += f'{k:.1f}'
label += r' s$^{-1}$'
ax.plot(xs, ys, lw=2.,
        label=label)
ax.set_xlabel('time, s')
ax.set_ylabel('probability distribution (1/s)')
ax.set_xlim(min(xs), max(xs))
ax.set_ylim(bottom=0.)
ax.legend()


fig.tight_layout()
fig.savefig('exponential.pdf')
