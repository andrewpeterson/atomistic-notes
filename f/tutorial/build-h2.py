#!/usr/bin/env python

from ase import Atoms, Atom
from ase.calculators.emt import EMT

# Build a Au dimer, constraining an atom.
atoms = Atoms([Atom('H', (0., 0., 0.)),
               Atom('H', (1.1, 0., 0.))],
              cell=(10., 10., 10.))
atoms.center()

calculator = EMT()
atoms.calc = calculator

energy = atoms.get_potential_energy()
forces = atoms.get_forces()
print(energy)
print(forces)
