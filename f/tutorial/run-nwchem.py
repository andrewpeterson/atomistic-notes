#!/usr/bin/env python3
#SBATCH --time=10:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=batch
#SBATCH --mem=1G

# ^^^^^ Don't touch the lines above here! ^^^^^
# (unless you know what you are doing)
# submit this script by typing `sbatch <scriptname>.py`.

from ase.build import molecule
from ase.calculators.nwchem import NWChem

calculator = NWChem(label='calc/nwchem',
                    dft={'xc': 'B3LYP',
                         'maxiter': 200,
                         'convergence': {'damp': 50}, },
                    basis='6-31+G*')

atoms = molecule('CH4')
atoms.calc = calculator
energy = atoms.get_potential_energy()
print(energy)
