
from ase.build import bulk
from ase.visualize import view

atoms = bulk('Au', 'fcc', cubic=True)
atoms = atoms.repeat([10] * 3)

view(atoms)

