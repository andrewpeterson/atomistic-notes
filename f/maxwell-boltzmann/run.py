import numpy as np
from matplotlib import pyplot

fig, axes = pyplot.subplots(ncols=2)

# Maxwell-Boltzmann.
a = 0.01
vs = np.linspace(0., 35., num=1000)
fs = [v**2 * np.exp(- a * v**2) for v in vs]
ax = axes[1]
ax.fill_betweenx(vs, fs, color='0.5')
ax.plot(fs, vs, '-')
ax.set_xlim(left=0.)
ax.set_ylim(bottom=0., top=max(vs))
ax.set_title('Maxwell-Boltzmann')
ax.set_xlabel('probability')
ax.set_ylabel('velocity')
ax.set_xticks([])
ax.set_yticks([])

# Boltzmann.
ax = axes[0]
es = np.linspace(0., 5., num=1000)
fs = [np.exp(-e) for e in es]
ax.fill_betweenx(es, fs, color='0.5')
ax.plot(fs, es, '-')
ax.set_xlim(left=0.)
ax.set_ylim(bottom=0., top=max(es))
ax.set_title('Boltzmann')
ax.set_xlabel('probability')
ax.set_ylabel('energy')
ax.set_xticks([])
ax.set_yticks([])

fig.savefig('out.pdf')
