import numpy as np
import ase.io
from matplotlib import pyplot

traj = ase.io.Trajectory('md.traj')
potential_energies = [image.get_potential_energy() for image in traj]
kinetic_energies = [image.get_kinetic_energy() for image in traj]
potential_energies = np.array(potential_energies)
kinetic_energies = np.array(kinetic_energies)
indices = range(len(traj))

fig, ax = pyplot.subplots()
fig.subplots_adjust(top=0.99, right=0.99)
ax.plot(indices, potential_energies, label='$E_\mathrm{pot}$')
ax.plot(indices, kinetic_energies, label='$E_\mathrm{kin}$')
ax.plot(indices, potential_energies + kinetic_energies, ':',
        color='0.8', label='$E_\mathrm{total}$')
ax.legend()
ax.set_xlabel('image in trajectory')
ax.set_ylabel('energy, eV')
fig.savefig('out.pdf')
