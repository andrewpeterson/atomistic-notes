#!/usr/bin/env python

from ase import Atoms, Atom
from ase.calculators.emt import EMT
from ase.md import VelocityVerlet
from ase.constraints import FixAtoms

# Build a Au dimer, constraining an atom.
atoms = Atoms([Atom('Au', (0., 0., 0.)),
               Atom('Au', (2.2, 0., 0.))],
              cell=(10., 10., 10.))
atoms.center()
atoms.set_calculator(EMT())
atoms.set_constraint(FixAtoms(indices=[0]))

# Run molecular dynamics, to see a vibration.
dynamics = VelocityVerlet(atoms, dt=0.1, trajectory='md.traj', logfile='md.log')
dynamics.run(steps=1000)
