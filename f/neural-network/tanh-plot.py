import numpy as np
from matplotlib import pyplot


fig = pyplot.figure(figsize=(3.5, 1.5))
ax = fig.add_axes((0.2, 0.3, 0.75, 0.65))

xs = np.arange(-5., 5., step=0.01)
ys = np.tanh(xs)
ax.plot(xs, ys, lw=3., color="#348ABD")
ax.grid(True)
ax.set_ylim((-1.2, 1.2))
ax.set_ylabel(r'$\tanh(x)$')
ax.set_xlabel('$x$')
fig.savefig('tanh.pdf')
