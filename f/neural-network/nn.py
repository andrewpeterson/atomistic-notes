"""
Alternative Observed Node Styles
================================
.. module:: daft
This model is the same as `the classic </examples/classic>`_ model but the
"observed" :class:`Node` is indicated by a double outline instead of shading.
This particular example uses the ``inner`` style but ``outer`` is also an
option for a different look.
"""

from matplotlib import rc
rc("font", family="serif", size=12)
rc("text", usetex=True)

import daft

# Page.
pgm = daft.PGM([6.0, 6.0], origin=[0.3, 0.3], observed_style="inner")

# Nodes.
for index in range(1, 6):
    key = 'x%i' % index
    label = r"$x_%i$" % index
    pgm.add_node(daft.Node(key, label, 0.5, 6.5 - index, fixed=True))

for index in range(1, 4):
    key = 'N1%i' % index
    label = r"N$_{1,%i}$" % index
    pgm.add_node(daft.Node(key, label, 2.0, 6.0 - index))

for index in range(1, 4):
    key = 'N2%i' % index
    label = r"N$_{2,%i}$" % index
    pgm.add_node(daft.Node(key, label, 3.0, 6.0 - index))

for index in range(1, 4):
    key = 'N3%i' % index
    label = r"N$_{3,%i}$" % index
    pgm.add_node(daft.Node(key, label, 4.0, 6.0 - index))

pgm.add_node(daft.Node('E', r'$E(\vec{x})$', 5.5, 4.0, observed=True))

# Connections.
def connect():
    for index in range(1, 6):
        key1 = 'x%i' % index
        for index in range(1,4):
            key2 = 'N1%i' % index
            pgm.add_edge(key1, key2)

    for index in range(1, 4):
        key1 = 'N1%i' % index
        for index in range(1, 4):
            key2 = 'N2%i' % index
            pgm.add_edge(key1, key2)

    for index in range(1, 4):
        key1 = 'N2%i' % index
        for index in range(1, 4):
            key2 = 'N3%i' % index
            pgm.add_edge(key1, key2)

    for index in range(1, 4):
        key1 = 'N3%i' % index
        pgm.add_edge(key1, 'E')

#connect()



# Render to create fig, ax.
pgm.render()

# Add text.
pgm.ax.text(0.5, 1.5, '...', ha='center', va='center')

# Save.
pgm.figure.savefig("nn-no-arrows.pdf")
pgm.figure.savefig("nn-no-arrows.png", dpi=300)
