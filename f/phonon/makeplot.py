import numpy as np
from matplotlib import pyplot

fig, ax = pyplot.subplots(figsize=(5., 3.))

kas = np.linspace(-np.pi / 2., +np.pi / 2., num=1000)
omegas = np.abs(np.sin(kas))

ax.plot(kas, omegas)
ax.set_yticks([])
ax.set_xticks([-np.pi / 2., 0., np.pi / 2.])
ax.set_xticklabels([r'$-\frac{\pi}{2}$', '0', r'$+\frac{\pi}{2}$', ])
ax.set_xlabel(r'$ka$')
ax.set_ylabel(r'$\omega$')
ax.set_ylim(bottom=0.)
ax.set_xlabel(r'$ka$')

fig.savefig('out.pdf')
