\begingroup
\chapter{Local minima: stable molecules and phases}


\section{Stable regions of the potential energy surface}
Recall that the potential energy surface is how the energy stored in a molecule or a system changes when you change the position of each atom in the system.
That is,

\[ E = f(\v{q}) \]

\noindent The most important regions of the potential energy surface are regions close to local minima; that is where the system is likely to stay.
These regions correspond to molecules, phases, or other stable structures.
(In this topic, we won't be dealing with kinetic energy, so all energies $E$ are potential unless otherwise stated.)

Also, note that due to temperature (as well as the Heisenberg uncertainty principle), the system is always in motion. 
Therefore, it in principle does not stay at any fixed point, and it may never actually be at the local minima except for a fleeting moment.
In a one-dimensional problem, it would occasionally pass through the local minimum.
However, in multiple dimensions, it probably never really goes through this point exactly (even ignoring the Heisenberg principle), but just oscillates around it.

Therefore, from the atomistic interpretation, the local minimum is our definition of a molecule (or other stable state, such as a phase or conformer), and can be considered an average configuration of that state. 
Thus, perhaps the most common and basic task in atomistics is to find a local minimum on the potential energy surface, and we will begin the course by looking for these.

\paragraph{Stationary points.}
There can be lots of individual minima on the potential energy surface.
Mathematically, these are defined as stationary points (locally flat, that is, $\partial E/\partial q_i = 0$ in all directions) that are concave upwards (positive second derivatives).

To clarify on the term `stationary point':
In your introductory calculus classes, you probably learned that when the first derivative of a function is equal to zero, you have found a local extremum---either a minimum or maximum point.
This is true when working in one-dimensional space, but in multi-dimensional space there are many features where all first derivatives are equal to zero, but that don't correspond to either local minima or maxima.
These other features are typically referred to as saddle points, and the first-order saddle point is of special significance to us, and we'll be discussing them in future lectures when we discuss reaction pathways.

But for now, know that if you have found a point where all first derivatives are zero:

\[ \left( \frac{\partial E}{\partial x_i} \right)_{x_{j \ne i}} = 0 \text{, for all } x_i \]

\noindent
Then you have found a \emph{stationary point} on the potential energy surface, which could be a local minimum, a local maximum, or one of various types of saddle points.
It is only a local minimum if it is concave upwards, that is

\[ \left( \frac{\partial^2 E}{\partial x_i^2} \right)_{x_{j \ne i}} > 0, \text{ for all } x_i \]

\paragraph{Local minima versus the global minimum.}
In all (non-trivial) PES's, there will be multiple local minima.
These can correspond to different conformers, different phases, or different ends of a reaction (initial versus final state), for example.
A local minimum implies we have found a stable region in which the system is likely to dwell.

However, if all the local minima in a PES can be found, one of them will be lower in energy than the rest.\footnote{Of course, there could be multiple minima with the same energy, but this typically only occurs if the states are symmetrically identical.}
This lowest-energy state is the global minimum.

As we'll discuss in a future lecture (\secref{sxn:qualitative-thermo}), the global minimum is not necessary the most likely state of the system.
As we'll see, the entropy can play a role in which state is more likely, particularly at higher temperatures.

\section{What is deep?}

\begin{figure}
\centering
$\vcenter{\hbox{\includegraphics[width=0.25\textwidth]{f/scan-methanol/atoms.pdf}}}$
\hspace{2em}
$\vcenter{\hbox{\includegraphics[width=0.5\textwidth]{f/scan-methanol/out.pdf}}}$
\caption{The potential energy surface created by rotating the OH bond in methanol.\label{fig:scan-methanol}}
\end{figure}

When we speak of local minima, how deep does it need to be?
When is it really a stable state, and when is it just noise in the PES?
(And the units of energy that we typically use are eV, which is probably not intuitive to most people in the room.)

As a more concrete example, consider the position of the OH bond in methanol.
If we relax the molecule, we'll find that the hydrogen bound to the oxygen prefers to sit in the space between two hydrogens of the methyl group, as shown in Figure~\ref{fig:scan-methanol}.
We can consider making a potential energy surface where we rotate the \ce{O-H} bond around---this is called a dihedral scan.
Then we can ask the question: should we picture that the \ce{O-H} bond is ``locked'' in place---that is, it is kinetically hindered from moving around---or should we expect it to freely rotate around?

From elementary statistical mechanics, we know that the probability of finding a system in a state with energy $E_i$ is described by the exponential distribution:

\[ p_i \propto \exp \left\{ - \frac{E_i}{k_\mathrm{B} T} \right\} \]

This can give us some idea of how deep minima in the potential energy surface need to be to be significant.
Let's assign zero energy to the bottom of our well (the local minimum), and make a table of probabilities based on energy levels:

\begin{center}
\begin{tabular}{rr}
\hline \hline
$E_i/k_\mathrm{B} T$ & $p_i \propto$ \\
\hline
0 & 1 \\
0.5 & 0.606 \\
1 & 0.37 \\
5 & 0.006 \\
10 & 4 $\times$ 10$^{-5}$ \\
50 & 2 $\times$ 10$^{-22}$ \\
100 & 4 $\times$ 10$^{-44}$ \\
\hline
\end{tabular}
\end{center}

Of course, this doesn't yet tell us anything about rates / kinetics.
But you can interpret it as if you observe 10$^5$ systems with a well of 10 $k_\mathrm{B}T$, that we can expect one of those 100,000 systems to have made it to to the top of the well.

At room temperature (300~K), $k_\mathrm{B}T$ is about 0.025 eV.
So certainly a well on this order (0.025 eV) is easily escaped.
By 0.25 eV it's getting to the point where the well is getting hard to get out of.
And by 2.5 eV the well is quite deep!
This also starts to give us some intuition on how to think about the units of eV.\footnote{If you're used to kJ/mol, 1 eV is about 96 kJ/mol.}

So we can say that a well is
\begin{itemize}
\item ``shallow'' if the depth of it is $\sim k_\mathrm{B}T$ (or less)
\item ``deep'' if the depth is $\gg k_\mathrm{B}T$
\end{itemize}

If we come back to the methanol rotation example, we can calculate the wells of this potential energy surface by scanning across different dihedral angles.
The script for this is shown in \secref{code:methanol-rotation} and the output in Figure~\ref{fig:scan-methanol}.
The wells here are on the order of 0.05 eV, or about 2 $\times$ $k_\mathrm{B} T$ at room temperature.
Thus, we can expect the system to easily overcome this barrier, and thus the \ce{O-H} group in methanol likely rotates significantly at room temperature.\footnote{With the caveat: this calculation was run at a fairly ``cheap'' level of electronic structure theory, so take the result with a grain of salt.}

\section{Optimization methods \label{sxn:optimization-methods}}

Finding a local minimum on the potential energy surface can be considered an optimization problem.
We will present various means of optimization; starting with the simplest and building up to the most advanced.

\paragraph{Using only energies.}
Imagine we have a one-dimensional system; that is, we only have to specify the position of one atom along one dimension to fully specify our system.
Let's assume we start at some point on the potential energy surface, and we want to find the local minimum.
The simplest thing would be to perturb $x$ and see how much $E$ changes.
For example, see Figure~\ref{optimization-energy-only}; where if we move from state 1 to state 2 we went up in energy.  
This suggests maybe we should have gone the other direction.
By ``feeling around'', we can eventually satisfy ourselves that we have found a local minimum, and we could all figure out how to write an algorithm to automate this process.

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/optimization-energy-only/drawing.pdf}
\caption{\label{optimization-energy-only}
Optimization using energy only.
}
\end{figure}

This clearly isn't a very efficient way to find the minimum.
Recall that:

\begin{itemize}
\item We actually have 3$N$ dimensions, so it is a \textit{lot} more complicated to bound the true minimum than shown in Figure~\ref{optimization-energy-only}!
\item Each point takes a long time to calculate. E.g., hours!
\end{itemize}

\noindent
Thus, we can expect such approaches to be rather tedious.

\paragraph{Hellmann--Feynman Theorem.}
However, when we solve the electronic structure problem to find the potential energy, we actually also find the forces.
In simple terms, the Hellmann--Feynman theorem tells us~\cite{Feynman1939}:
\begin{quote}
Once the electronic structure is solved for (with quantum mechanics), the forces on the atoms can be found with electrostatics.
\end{quote}
That is, most electronic structure calculators give forces ``for free'' once the electronic structure is calculated.

If we have the forces, this always points downhill on the potential energy surface.
In one dimension, recall that

\[ F_i = - \frac{d E_\mathrm{pot}}{d x_i} \]

\noindent
So in our previous example, we could perhaps make it a lot more efficient because we now know which direction to go in each step, and we can start walking downhill.
There are methods that take just this approach, of walking downhill according to the forces.
(In ASE, they go by names like MDMin and FIRE.)

Having the forces is also nice for a second reason: they can tell us when we have reached a minimum.
This occurs when the forces go to zero---since they are derivatives of the energy, this tells us we have found an extremum.\footnote{Or really a ``stationary point'', which includes local minima, local maxima, and saddle points. But if we are using an optimization algorithm to follow forces downhill, we'll almost certainly be finding the local minima. In any case, we can verify this by looking at the second derivative. We'll discuss this more in future chapters when we search for saddle points, a much more complicated task.}

\paragraph{Newton's method.}
Can we do better?
Let's start with a Taylor series expansion (TSE) in one dimension, truncated after the second derivative:

\[ E(q_0 + \Delta q) = E(q_0) + \left. \frac{dE}{dq}\right|_{q_0} \, \Delta q + \frac{1}{2} \left. \frac{d^2 E}{dq^2}\right|_{q_0} \Delta q^2 + \cdots \]

\noindent
Here, $E(q_0)$ is the potential energy at some point $q_0$ on the PES; that is, some initial guess of the position.
$E(q_0 + \Delta q)$ is the energy at a new position, which we would like to be a local minimum.
If the new point is truly a local minimum, then its derivative with respect to $\delta q$, the perturbation distance, must be zero:

\[ 0 = \frac{dE(q_0 + \Delta q)}{d \Delta q} = \left. \frac{dE}{dq}\right|_{q_0} +  \left. \frac{d^2 E}{dq^2}\right|_{q_0} \Delta q + \cdots \]

\noindent
Then, we can solve for the step ($\Delta q$) that we should take in order to find the minimum (under the approximation of truncating the Taylor series at the 2nd-order term):

\[ \Delta q = - \frac{E'(q_0)}{E''(q_0)} \]

Interestingly, if we are in a region of the PES where the potential well is harmonic---that is, exactly described by a parabola---then this will get us to the local minimum in only a single attempt.
In atomistics, when we are near local minima we typically are mathematically close to a harmonic form.
We will see this later when we talk about vibrational modes of molecules; the vibrations look like harmonic oscillators.
So when we are getting close to a minimum, this should be a pretty fast way to nail it down.

So, if we knew the first derivative and the second derivative of $E$ with respect to $q$ (evaluated at $q_0$), then we would have a very good guess of the right step---both in terms of direction and size---to take to get to the optimum.  
Do we know these?
\begin{itemize}
\item
We typically know the first derivative; this is the (negative) force, which comes from the Hellmann--Feynman theorem.
That is, the force comes directly from the electronic structure calculation.
\item
We typically do not know the second derivative,\footnote{There is no Hellmann--Feynman theorem for the second derivative, but some codes do have ways of ``analytically'' evaluating the Hessian---or matrix of second derivatives---within an electronic structure calculation. However, even when this is possible it tends to be computationally expensive, perhaps scaling an order-of-magnitude worse than an energy- or force-only calculation.~\cite{Johnson1994,Sharada2012} However, if an analytical Hessian is available, it's worth seeing if it can benefit the optimization algorithm.} so we have to estimate this.
For a single variable we can estimate this with a finite-difference approximation as:

\begin{equation}\label{eq:second-fda}
\frac{d^2 E}{dq^2} \approx \frac{ \left.\frac{dE}{dq}\right|_{q + \delta q} - \left. \frac{dE}{dq}\right|_{q - \delta q}}{2 \, \delta q}
= \frac{F(q - \delta q) - F(q + \delta q)}{2 \, \delta q}
\end{equation}

\noindent
However, note that making this estimate takes two extra electronic structure calculations.
So, we have to determine if it's worth it to build the second derivative with extra force calls---will the improved accuracy of our next step be worth it?

\end{itemize}


\paragraph{Vector form: the ``Hessian matrix''.}
Here, we repeat the derivation in vector form.
In a real atomistic system, we have $3N$ independent variables; that is, the Cartesian positions of all $N$ atoms in the system.
Let $\v{q}$ be the tuple of all positions (the generalized coordinates).
Then we write the Taylor series expansion as

\[ E(\v{q}_0 + \v{\Delta q}) \approx E(\v{q}_0) + \left(\v{\nabla} E(\vec{q}_0)\right)\T \v{\Delta q} + \frac{1}{2} \v{\Delta q}\T \m{H} \, \v{\Delta q} + \cdots
\]

\noindent
where $\m{H}$ is called the \emph{Hessian matrix}, whose components are made up of the partial derivatives:

\[ H_{ij} \equiv \frac{\partial^2 E}{\partial q_i \partial q_j} \]

Again, we set the first derivative w.r.t. $\vec{\Delta q}$ equal to zero, leading to the condition:

\[ \m{H} \, \v{\Delta q} = - \v{\nabla} E(\v{q}) \]

\noindent
which gives a prediction of the optimal step size in vector form $\v{\Delta q}$.
That is, we need to solve the above linear system for $\v{\Delta q}$, which involves inverting our Hessian matrix.

However, note that now to approximate the Hessian we would do two new calculations per degree of freedom!
If we have, for example, 30 atoms, this means 90 degrees of freedom and about 180 calculations to approximate this Hessian: all that before taking a single step in the optimization.
Again, consider that each calculation might take an hour on an expensive piece of computing equipment.

\paragraph{Quasi-Newton methods.}
Instead, many methods exist which build \textit{approximations} to the Hessian on the fly, through information that comes out of moving around on the potential energy surface.
For example, consider taking a step in one dimension: we know the force at the initial point and at the new point, from the difference in these forces we can estimate the local curvature, similar to equation~\eqref{eq:second-fda}.

These methods continually refine the guess of the (multi-dimensional) Hessian as the method proceeds.
The most famous method is known as BFGS.~\cite{Broyden1970,Fletcher1970,Goldfarb1970,Shanno1970a}
We'll leave it to numerical methods classes to suggest how the Hessian is built.
Note also that the BFGS method avoids the need to invert the Hessian matrix, which can be costly for huge simulations with many degrees of freedom, but in the types of simulations we run in this class, the bottleneck is typically from the electronic structure problem (that is, a ``force call''), not the Hessian inversion.

\paragraph{Line search.}
A modification is often made on top of these methods to make sure that they don't overshoot.
See Figure~\ref{linesearch}; too large of a step could actually lead to the energy increasing instead of decreasing.
If this happens, the linesearch modification would reject that step and try a smaller step.


\begin{figure}
\centering
\includegraphics[width=3.0in]{f/descent/drawing2.pdf}
\caption{Example of overshooting. \label{linesearch}}
\end{figure}

This is considered one of the fastest methods, and in ASE is called BFGSLineSearch.
BFGSLineSearch is typically considered the best general optimizer in ASE, and if you choose ``QuasiNewton'' you will get BFGSLineSearch.
However, knowing how these work in practice is important when you try to solve different sorts of problems.
We will come back to this in the future.


\paragraph{Constraints.}
It is often smart to put one or more constraints on the system.
Most commonly, these constraints freeze the spatial position of one or more atoms; however, all sorts of creative constraints are possible.
A couple of examples of freezing atoms are:

\begin{itemize}

\item
In the methanol rotation example, we fixed the dihedral angle (in 1$^\circ$ increments), and relaxed all other degrees of freedom subject to this constraint.
This allowed us to produce the potential energy surface along this chosen coordinate.

\item
In our ethanol example, we could choose to fix the position of the central carbon atom---that is, not allow the algorithm to move it around space.
This immediately removes three degrees of freedom from the problem.
Since we know that just moving the whole molecule around space will not change its energy, this does not change our result.

Note that nonlinear molecules, in a vacuum, have $3N-6$ degrees of freedom: 3 degrees are removed for translations in the three dimensions and 3 degrees are removed for rotations about the three axes.
Linear molecules have $3N-5$ degrees of freedom as there are only two axes of rotation, while monatomic systems have $3N-3$ degrees of freedom since rotations are meaningless.
This last case properly means that for a monatomic system, we could place it anywhere (as long as it is not interacting with anything external) and had better get the same answer: that is, zero degrees of freedom.

\item
In catalysis, we often have a crystalline metal with a molecular fragment (an adsorbate) on the surface.
In these simulations, some lower layers of the metal are often fixed at the bulk crystalline conditions; this is effectively enforcing a boundary condition while reducing the dimensional space of the PES that must be explored.

\end{itemize}

\paragraph{Example of optimization in ASE.}
An optimization example is provided in Section~\ref{code:constrainedoptimization} that shows constrained optimization.
Try changing the optimizer to see how it behaves.

\paragraph{Optimization in practice.}
Optimization is probably the most common task in atomistics (and numerics in general), and as a result these methods are relatively robust.
That is, you can typically just give your structure to ASE's QuasiNewton algorithm and let it find a minimum and it will ``just work''.
If you occassionally hit problems, then dive in deeper, or just try switching methods to something simpler such as BFGS or FIRE.


\section{Script(s)}

\subsection{Methanol rotation\label{code:methanol-rotation}}
\pcode{f/scan-methanol/run.py}

\subsection{Constrained optimization\label{code:constrainedoptimization}}
\pcode{f/scripts/constrained_optimization.py}

\endgroup
