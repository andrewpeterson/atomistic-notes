\begingroup
\chapter{Interatomic potentials}


\section{Interatomic potentials}
{
\newcommand{\pot}{E_\mathrm{pot}}
%\newcommand{\pot}{V}

\paragraph{The electronic structure problem.} All methods we have discussed to date rely on information from the PES; that is, energy and forces. MD simulations tend to require long simulation times and/or large system sizes (as compared to stationary point searches like relaxation or saddle-point searches). Ideally, the energy and forces come from electronic structure calculations. Put simply, in the Born-Oppenheimer limit this means finding (estimating) the solution to Schr\"odinger's equation:

\[ \hat{H} \left| \psi \right\rangle
=        E \left| \psi \right\rangle
\]


\noindent given a set of nuclear positions.
(These are sometimes called ``ionic positions'' since we specify the position of the positive ions, and the negative electrons fill in around them.)

In practice, the Schr\"odinger equation has not been explicitly solved for energy, except for the simplest of systems, such as the hydrogen atom.
Much of the difficulty is due to the $N$-body problem: that is, each electron's ``orbital'' is a function of the orbital of every other electron in the system.
This is also what makes calculations of planetary orbitals difficult.

In simplistic terms, density functional theory is a way to estimate the electronic structure solution by converting the $N$-body problem into a set of $N$ 1-body problems.
The electrons are treated in bulk as an electron density; and the algorithm searches for an electron density as a function of position, $\rho(x, y, z)$.
Then, each of the $N$ electrons is allowed to interact with this background density.
Corrections are made on top of this due to exchange (Pauli) and correlation (between electrons); this is what is often called the \emph{``exchange--correlation functional''}, often abbreviated in codes as \verb'xc' or just called the functional.
The solution's accuracy is limited by the unknowable nature of the xc functional.
(The functionals are often crafted to reproduce experimental or higher-accuracy data, and so depending upon what system and properties you are trying to model you may choose a different functional. For this reason DFT is sometimes called a ``semi-empirical method''.)
Finding the optimal electronic structure is variational in nature---that is, the electronic configuration that leads to the lowest energy is the correct one---so it just needs to try different electronic configurations until it finds the lowest energy one.
This makes DFT iterative, and slow.
However, it's (one of) the fastest means of solving the electronic structure problem, and is probably the most popular method.
In my group's research typical calculations take 30 minutes on 8 cores per single-point calculation (often called a ``force call'' or an ``ionic step'').
Generally, the calculation gets more expensive with an increasing number of electrons as $\mathcal{O}(N_\mathrm{el}^3)$, where $N_\mathrm{el}$ is the number of electrons, or more propertly the number of electronic basis functions. 
(Although research is underway to improve this scaling.)
More accurate electronic structure calculators are available; however these generally scale with $\mathcal{O}(N_\mathrm{el}^5$ -- $N_\mathrm{el}^7)$, and so will typically only be used with small molecular systems.

\paragraph{Interatomic potentials.}
Molecular dynamics calculations in particular often require a large number of time steps, large system sizes, or both, and thus methods such as DFT can sometimes be too expensive to be feasible.
Since there is no closed-form, energy-explicit form of the Schr\"odinger equation, for expensive calculations such as MD researchers will often rely instead upon empirical interatomic potentials, which are fit to other data, such as higher quality calculations or experimental data.
Note that potential energy is often denoted as $V$ in literature; I have been sticking to $E_\mathrm{pot}$ in this class to avoid over-use of $V$ (which we also use for volume and electrical potential).

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/pair-potential/out.pdf}
\caption{ The qualitative form we would expect of the potential energy as we bring together two atoms. The atom--atom distance is $r$.  \label{pair-potential}}
\end{figure}


We can start by looking at ``pair potentials'', which describe how the potential energy changes as two atoms are brought together. We can qualitatively sketch out what this should look like; see Figure~\ref{pair-potential}.
A simple interatomic pair potentials is the \emph{Lennard-Jones} potential:

\[\pot = 4 \epsilon \left[ \left( \frac{\sigma}{r}\right)^{12} - \left( \frac{\sigma}{r}\right)^{6} \right] \]

\noindent where $r$ is the interatomic distance and $\sigma$ and $\epsilon$ are adjustable parameters.
This sometimes called the 6--12 potential after the exponents.
Note that as $r \rightarrow \infty$ $E_\mathrm{pot} \rightarrow 0$ and as $r \rightarrow 0$ $E_\mathrm{pot} \rightarrow \infty$ in this formulation, and the well-depth is found at the local minimum, which just gives $\epsilon$.

Another common pair potential is the \emph{Morse potential}, which has the form

\[ \pot = D_\mathrm{e} \left( e^{-2 a (r - r_\mathrm{e})} - 2 e^{-a(r - r_\mathrm{e})} \right)
\]

\noindent Both Lennard-Jones and Morse potentials exhibit the following qualitative features from Figure~\ref{pair-potential}:

\begin{itemize}
		  \item  As $r >> r_\mathrm{e}$, $d \pot / dr \rightarrow 0$ (flat line; no interaction)
		  \item  As $r << r_\mathrm{e}$, $\pot  \rightarrow \infty$ (repulsion)
		  \item At $r = r_\mathrm{e}$, $\frac{d \pot}{dr} = 0$ and $\frac{d^2 \pot}{dr^2} > 0$ (local minimum)
\end{itemize}

One can write a pair potential for every pair of atoms in your system, and use this to run a molecular dynamics simulation.
(Both Lennard Jones and Morse potentials are available as calculators in ASE, for example.)
That is, the sum of all the pair potential potential energies will give you the total potential energy.
 However, this is of limited value as it can't reproduce many chemical phenomena---for example, it tells you nothing about the angle that atoms form, which we know from chemistry to be important.

There is an art to building interatomic potentials, and there are families of terms that are added to this to parameterize all sorts of interactions, from bond breaking to van der Waals terms to electrostatic terms.
The number of parameters often stretches into the hundreds to try to make these potentials more accurately fit the parent data.

However, it should be cautioned that the models are only as good as the physical terms put into them.
And, since the Schr\"odinger equation has no energy-explicit form, it is impossible to put the ``correct'' physics in, so we should always expect some imperfection.

\section{Machine learning}

A recent direction is to employ machine learning techniques from computer science to develop representations of the PES.
Recall that the ground-state potential energy is a unique, but unknowble function of the atomic coordinates, $\vec{x}$:

\[ \pot = f(\vec{x}) \]

\noindent
That is, mathematics does not give us an energy-explicit form of the above equation, but quantum mechanics tells us that the above meets the definition of a function.
Machine learning excels at fitting an agnostic functional form to arbitrary data, and many machine-learning functional forms have the property that if the training data is noiseless and continuous, then the machine can reproduce the parent function exactly, given a large enough set of training data and enough parameters in the machine-learning form.

Therefore, if we can generate a large amount of training data with an electronic structure method of our choice (\textit{e.g.}, DFT), we can develop a machine-learning representation of this data set that is energy explicit.~\cite{Blank1995,Lorenz2004} 
In principle, it can $\sim$exactly replicate the DFT calculations at the points of the training set, and will interpolate at points in between.

\begin{figure}
\centering
\includegraphics[width=5.0in]{f/neural-network/nn.pdf}
\caption{An example of a (3, 3, 3) neural network for machine learning of the PES. The input is the atomic positions and the output is the potential energy. \label{nn}}
\end{figure}

Machine-learning functions can take many forms, but a common one is the artificial neural network, shown schematically for our problem in Figure~\ref{nn}.
The input ``layer'' is the vector of atomic positions, $\vec{x}$, and the output ``layer'' is the potential energy $\pot$. Each ``node'' is a mathematical function known as an activation function. A common activation function is tanh (Figure~\ref{tanh}), resulting in the following equation defining the output of node $(i,j)$:

\[ N_{i,j} = \tanh \left( \sum_k w_{i-1, k} N_{i-1, k} + b_{i,j} \right)
\]

Activation functions such as tanh have regions of linear response (which can be made negative by the signs of the weights/scalings) positive and negative curvature, and no response.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/neural-network/tanh.pdf}
\caption{ Activation functions such as tanh have regions of linear response (which can be made negative by the signs of the weights/scalings) positive and negative curvature, and no response.  \label{tanh}}
\end{figure}

\noindent where the $w$'s and $b$'s are referred to as the weights and scalings, respectively, and are the adjustable parameters of the model. Note that there are lots of adjustable parameters with this model, so we do have to worry about overfitting. (There are different ways to address this; one is to fit not just the energy but also the forces which assures a smooth departure from each training point.)

We refer to the above as the \emph{Cartesian neural} approach, because it directly inputs the raw Cartesian coordinates into the neural network.
This, in general, should be capable of exactly replicating any potential energy surface of a fixed-composition system.
However, there are two disadvantages of such an approach:


\begin{itemize}
		  \item The system is not size-extensible. For example, if we trained to a system of 8 water molecules and then wanted to study a system with 9 water molecules, we would be out of luck. The system would have been trained with an input layer with 24 elements ($8\times3$) and there is no straightforward way to put in an input layer with 27 elements.

		  \item The model treats atoms of the same species as distinguishable. One should be able to swap the positions of two identical atoms in the system and get out the same energy prediction. Although this is not a problem per se, it hints at inefficiencies in the algorithm.
\end{itemize}



Recently, it has been proposed\cite{Behler2007} to use atom-centered machine learning to circumvent these problems.
See Figure~\ref{behler}.
A ``fingerprint'' is constructed that describes the local environment about each atom in a rotationally invariant way.
These fingerprints account for the local environment about each atom, for example in the Behler--Parrinello approach there will be a family of radial functions of the form

\[ G_1^{(i)} = \sum_{j \ne i}^{N_\mathrm{neighbors}} \exp \left \{ -\eta \left( \frac{r_{ij}}{r_c} \right)^2 \right\} f_\mathrm{C} (r_{ij})
\]

\[ f_\mathrm{C}(r_{ij}) = 0.5 \left[ \cos \left(\pi \frac{r_{ij}}{r_\mathrm{C}} \right) + 1 \right] \]


\begin{figure}
\centering
\includegraphics[width=4.5in]{f/atom-centered-learning/drawing.pdf}
		  \caption{The atom-centered learning scheme (e.g., Behler-Parinello) for creating interatomic potentials. \label{behler}}
\end{figure}

\noindent
where the cutoff radius ($r_\mathrm{C}$) describes how far out the description goes and the $\eta$ parameter has several values making up the different members of the family. There are similar forms to take into account three-body interactions, and together this makes up a ``fingerprint'' about each atom.

The fingerprint is the input to each neural network, and there is one neural network per element (\textit{e.g.}, one NN for carbon, one NN for oxygen, ...) .
The $N$ neural networks are run simultaneously and added to give the total energy.
In building the model, the whole system of neural networks is trained together.

Our group has produced an open-source code, known as \textit{Amp}\footnote{See \url{https://bitbucket.org/andrewpeterson/amp}. Our previous version of this was referred to as \textit{Neural}, which may offer more stability / better convergence in the near term.}, to facilitate such calculations in either approach, or with user-defined fingerprinting or machine-learning schemes.

} % end potentials command

\endgroup
