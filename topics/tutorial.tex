\begingroup
\newcommand{\smallurl}[1]{\begingroup\footnotesize\url{#1}\endgroup}
\newcommand{\mib}[1]{\begingroup\footnotesize\mintinline{bash}{#1}\endgroup}
\newcommand{\mip}[1]{\begingroup\footnotesize\mintinline{python}{#1}\endgroup}

\chapter{Computational logistics and tutorial \label{ch:tutorial}}

Here we'll summarize some of the common things you need to know to run atomistic scripts with the tools in this class.
See also the example scripts at the end of each topic, as well as the Piazza discussion forum for additional help.

\section{Logging into Oscar}

The high-performance computing system we use is called Oscar, hosted by Brown's Center for Computation and Visualization (CCV).
There are two major ways to log in:

\begin{itemize}
\item The terminal method, via SSH (faster, more powerful):

\smallurl{https://docs.ccv.brown.edu/oscar/connecting-to-oscar/ssh}

\item The graphical method (easier to new users):

\smallurl{https://docs.ccv.brown.edu/oscar/connecting-to-oscar/open-ondemand}

\end{itemize}
  
You can verify that your connection is working by typing the below lines, followed by ``enter''.
Don't type the dollar signs---that is the prompt symbol.

\begingroup
\footnotesize
\begin{minted}{bash}
$ module load xeyes
$ xeyes
\end{minted}
\endgroup

\noindent
You should see creepy eyes.
You can log out by typing \mib{exit}.

\subsection{Initial setup}

The first time you log in, you will need to link your account to the ENGN 2770 course modules.
First, open the hidden file called ``.bashrc'' by typing \mib{gedit .bashrc} at the prompt.
Paste the following lines at the end of the resulting text file:

\begingroup
\footnotesize
\begin{minted}{bash}
if [[ -d /oscar/data ]]; then
    source /oscar/data/ap31/ap31/teach/2024-engn2770/software/bashrc
fi
\end{minted}
\endgroup

\noindent
Save and close the text file.
\mib{exit} Oscar, then log back in.
Verify that you can load the course software by typing:

\begingroup
\footnotesize
\begin{minted}{bash}
$ engn2770_loadnwchem
$ ase gui
\end{minted}
\endgroup

\noindent
A blank window should appear that says `ase.gui' on the top.

\subsection{Loading the software environment}

You'll need to type \mib{engn2770_loadnwchem} once per session---that is, every time you log in.
If you want to avoid this, you can optionally just add that line to the end of your \mib{.bashrc} file; everything in that file is run every time you log in.

\section{Basic linux commands}

Oscar runs linux, and you generally navigate through it at the command line.
Copying files, creating files, opening directories (folders), are all common operations.
Here we'll summarize a few of the most common commands you are likely to use.
Anywhere you see angle brackets (\mib{<like_this>}), this means you should replace those with some word, like a folder name.
Don't type the brackets.

\begin{itemize}
\item
\mib{pwd}: ``print working directory'' (What folder am I in?)

Note that directories are separated with a forward slash (\mib{/}).

\item
\mib{cd <pathname>}: ``change directory'' (Open the folder and enter it.)

\item
\mib{cd ..}: change back to the parent directory (\mib{..} means parent directory)

\item
\mib{mkdir <directoryname>}: ``make directory'' (Make a new folder.)

\item
\mib{cp <source_file> <destination>}: copy a source file to a destination

The destination can be either a directory or a file name, or even a combination like \mib{path/to/my/file.txt}.

\item
\mib{cp -r <directory> <destination>}: (recursively) copy a directory

\item
\mib{mv <origin> <destination>}: move a file or directory

\item
\mib{gedit <filename>}: open a text editor and edit a file

The file will be created if it doesn't exist.

\item
\mib{rm <path>}: remove (delete) a file or directory

Like \mib{cp}, you need to supply the \mib{-r} flag if you want to delete a directory.

\item
\mib{exit}: log out

\end{itemize}

\subsection{Good practices}

\paragraph{Tab is your friend.}
Use the tab key to help you type commands.
If you type the first few letters of a command followed by the tab key (\textit{e.g.}, \mib{engn[tab]}), it should automatically type the rest of the command for you (\textit{e.g.}, \mib{engn2770_loadnwchem}).
If there are multiple commands matching, hit tab twice, and you will see a list of matching commands.
This also works for paths and filenames.
It saves you time \emph{and} prevents typos.

\paragraph{Work in a clean directory.}
In this class, you'll generally be running a python script (a text file) for each atomistic simulation.
I strongly recommend putting each python script you creat in its own, empty directory.
That way, you know that any files within that directory were created by your script, and it also avoids conflicts between scripts.

\paragraph{Be careful with copy, move, and remove!}
There is no Trash Bin!\footnote{Technically I think there are some periodic snapshots made of the entire filesystem that you might be able to find.}
If you remove a file, or copy a file on top of another one, that file is gone permanently.
I recommend adding the following lines to the end of your \mib{.bashrc}, which makes all these commands interactive (asking you for confirmation before deleting anything):

\begingroup
\footnotesize
\begin{minted}{bash}
# Prevent stupid mistakes:
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
set -o noclobber
\end{minted}
\endgroup

\section{Software overview}

The main interface we will be using for atomistic simulations is called the Atomic Simulation Environment (ASE).
ASE handles the \emph{atomistics}; that is, the manipulation of the atoms in space.
This is cleanly divided from the \emph{electronic structure}, which tells us where the electrons are for a given configuration of atoms (that is, given coordinates of the nuclei).

ASE can interface with dozens of different codes that determine the electronic structure.
In this class, we will focus on three, which are used mainly as:

\begin{itemize}
\item
\textbf{EMT} (Effective Medium Theory): Used primarily for simulations of transition metals and materials properties. This is a crude, but very fast, calculator. We often use it just to explore methods, since it is so fast and is built in to ASE.
\item
\textbf{NWChem}: a density-functional theory calculator, which we'll use primarily for small molecules and non-periodic systems.
\item
\textbf{GPAW}: a density-functional theory calculator, which we'll use primarily for materials, surfaces, and periodic systems.
\end{itemize}

\section{Basic ASE introduction}

\smallurl{https://wiki.fysik.dtu.dk/ase}

ASE has excellent documentation and tutorials; you should refer to its website often.
ASE is based in python, although you don't need to be an expert in python coding in order to use it.
Here we'll go over some of the basics of ASE.

\subsection{Working with the Atoms object}

The central object of an ASE script is the \mip{Atoms} object.
Try the script below to illustrate its use.
(That is, save it to a file named \mib{build-h2.py}, and run it with \mib{python build-h2.py}.)

\pcode{f/tutorial/build-h2.py}

\noindent
Breaking down each line of this code:

\begin{itemize}
\item[1]
This line just tells the computer that this is a python script.
You don't always need it, but it's a good idea to have at the top of all your scripts.
\item[3--4]
In python, you need to import the objects you use.
This makes it obvious where each object comes from.
\item[7--8]
Here we show the most basic way of building an \mip{Atoms} object.
We use the \mip{Atoms} class to make this; it is literally a list of \mip{Atom} objects.
Each \mip{Atom} object needs to have the basic information of what element it is, and where in space it is, in units of \AA.
Here, we've created an \ce{H2} molecule where the bond length is 1.1~\AA, oriented on the $x$ axis.
\item[9]
We're also establishing a unit cell for this atoms; that is, the size of box the atoms are in.
\item[10]
This centers the atoms inside their cell.
\item[12--13]
We make a calculator object---here using EMT---and attach it to the atoms.
The calculator is the electronic structure calculator.
\item[15]
Here, we are asking the calculator (EMT) to return the potential energy for the specific position the atoms are in.
\item[16]
Now we ask the calculator to calculate the force on each atom.
\item[17--18]
Finally, we print the results to the screen.
\end{itemize}

\subsection{Interactive inspection}
It can be very useful to \emph{inspect} objects to understand what they do.
Try running the script above in interactive mode; type \mip{ipython -i build-h2.py}.
You should get an interactive python shell where you can interact with these objects.
Try each of the commands below to see what they do (don't type the \mip{>>>}):

\begingroup
\footnotesize
\begin{minted}{python}
>>> help(Atoms)
>>> help(EMT)
>>> dir(atoms)
>>> Atoms?
>>> Atoms??
>>> len(atoms)
>>> energy
>>> forces
>>> forces[1, 0]
\end{minted}
\endgroup

\subsection{Building atoms}

The earlier script showed the most basic way of building an atoms object.
However, there are lots of helper functions in ASE to allow you to make more complicated structures.
Try some of the below commands to see what they do (after starting an interactive python session by just typing \mib{ipython} or \mib{python}):

\begingroup
\footnotesize
\begin{minted}{python}
>>> from ase.visualize import view
>>> from ase.build import molecule, nanotube, fcc211
>>> atoms = molecule('H2O')
>>> view(atoms)
>>> print(atoms.positions)
>>> atoms = nanotube( ... )  # you finish!
>>> atoms = fcc211( ... )  # you finish!
>>> atoms.write('my-atoms.traj')
\end{minted}
\endgroup

\noindent
Here, the final line writes your atoms object out to a file.
We'll use that in the next section.

\subsection{Seeing and interacting with the atoms}

You can use the \mib{ase gui} to interact with atoms graphically.
From a bash prompt, you can rum \mib{ase gui my-atoms.traj} to open the file you created in the last section.
(This is equivalent to how you opened it from within python with the \mip{view} command.)
The ase gui can also be used to do many other tasks, including some rudimentary build commands as well as moving atoms around, and saving the results.

My personal recommendation, however, is to build atoms using scripts.
This makes the process both repeatable and self-documented.
If you want to do it again, tweaking something just a bit, you can do that much easier if you have a script you can modify.

\section{Submitting scripts to the high-performance computing system}

So far, we have just run python scripts at the command line, because we have only been doing simple tasks such as building atoms or using the EMT calculator.
Whenever you use a real electronic structure calculator (NWChem or GPAW), you must submit the script to the high-performance computing system.
This puts the job in a queue, and when resources become available lets it run on the high-performance nodes.
You can generally request that it run on many cores, which will (typically) speed up the calculation. 

You can submit a job, in this example named \mib{run.py}, with:

\begingroup
\footnotesize
\begin{minted}{bash}
$ sbatch run.py
\end{minted}
\endgroup

\noindent
You can check on your running jobs with:

\begingroup
\footnotesize
\begin{minted}{bash}
$ myq
\end{minted}
\endgroup

\noindent
When jobs run on the high-performance nodes, they will also make a file that starts with the word 'slurm' which has information about the job, such as the nodes it has been assigned and the job start time.
SLURM is the name of the queue management software.

\section{NWChem}

NWChem is an open-source density functional theory calculator developed at Pacific Northwest National Laboratory.
We will generally use this software, through the ASE interface, to calculate isolated molecules in non-periodic systems.
An example NWChem script follows.

\pcode{f/tutorial/run-nwchem.py}

\noindent
The new content:

\begin{itemize}
\item[2--6]
These lines tell the job management system what to do with your job.
\textit{E.g.}, how much memory, how many cores, etc.
For now, leave all these lines as-is.
\item[15--19]
The calculator is, obviously, now NWChem.
It has many more parameters to specify than EMT, but for now, don't touch these. 
\end{itemize}

\noindent
You submit this script with \mib{sbatch}, as described above.

\endgroup
