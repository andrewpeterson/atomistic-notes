\begingroup
\newcommand{\Epot}{\ensuremath{E_\mathrm{pot}}}

\chapter{The potential energy surface}

\reading{Truhlar, ``Potential Energy Surfaces''~\cite{Truhlar2003}}

The goal of atomistic modeling is to use computers to describe chemistry; that is, to describe reactions, materials properties, etc.
Therefore, we need a numerical framework to describe chemistry, and we must be capable of interpreting what the simulation can tell us.

This is done in the language of the ``potential energy surface'' (PES), which generally contains the information that we need to understand the chemistry of a system.
Many people will already be familiar with this, but here, we give a formal description of the PES and some of the information contained within it.

\section{Potential energy}

When we refer to the ``potential energy'', we just mean the energy of a stationary atomic system---that is, one in which we specify the coordinates of all the nuclei in the system.
We can think of the potential energy by a simple thought experiment, shown in Figure~\ref{fig:potential-energy}.
First, we start out with an empty box, and we bring in the nuclei to their assigned positions; for example, the position of the O, H, and H nuclei in \ce{H2O}.
Since nuclei are positively charged, they will repel eachother, and the net energy of the system so far will be positive (unstable).
In the final step, we bring the electrons to the system---where they equilibrate to their ``ground state''.
This lowers the net energy of the system.
This final, net energy is the ``potential energy''.

\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/potential-energy/drawing.pdf}
\caption{Thought experiment for constructing the potential energy, for a given configuration of nuclei.
\label{fig:potential-energy}
}
\end{figure}

The potential energy is so-named to distinguish it from kinetic energy.
That is, if the nuclei are moving\footnote{\textit{I.e.}, if the molecule is flying through space, vibrating, rotating, etc.} then they have momentum, resulting in the system having kinetic energy.
We assume that the total energy can be expressed as a sum of the kinetic and potential energy.
At this level of approximation, we take the potential energy to only be a function of the atomic positions, and the kinetic energy to only be a function of the atomic momenta.

\paragraph{Assumptions.}
The key assumption in working with a potential energy surface is that if the nuclear positions are fixed (specified), then there is a single value of the energy.
That is, the potential energy is \emph{only} a function of the position of the nuclei.
What assumptions does this rely on?

\begin{itemize}

\item
The number of electrons in the system is fixed.
For example, the system is charge neutral, where the number of electrons matches the total nuclear charge.
Clearly, if we are looking at an isolated \ce{H3O} or an isolated \ce{H3O+}, which have different numbers of electrons, then we should expect them to have different potential energies.

\item
The electrons are in their ground-state configuration.
From quantum mechanics (or even basic chemistry), we know that there is a most-stable configuration that the electrons can find, and we assume the electrons are in this state.
Of course, there are such things as ``excited states'', where electrons can be excited to higher-energy orbitals.
For the moment, we'll assume we are always referring to the ground-state electronic configuration.

\item
The electrons move very fast compared to the nuclei, such that at any nuclear configuration the electrons instantly equilibrate---that is, they immediately find their ground state configuration.
In reality, when the nuclear positions move, it does take some time for the electons to find their ground state, but this time is very short.
This assumption goes by the names of the \emph{Born--Openheimer approximation} or the \emph{adiabatic approximation}.

\end{itemize}

Note also that we should be cautious, because of the Heisenberg uncertainty principle.
This principle says there is always a trade-off between how well I can know an object's position and its momentum.
That is, if I now an atom's position very precisely, then I will have no idea about it's momentum.
This may seem problematic in atomistic simulations where we are specifying positions to many decimal places (expressed in \AA).
However, at this stage, don't worry about this.
We will deal with this in later sections, such as when we look at examining thermodynamics from the potential energy surface.

In this class, we'll only be dealing with the ``ground-state potential energy surface'', which we'll refer to just as the potential energy surface, which is uniquely defined by the atomic coordiantes.
Note that in some literature, such as the article by Truhlar, multiple potential energies are present, including both the ground-state and excited-state surfaces.


\section{Example}

To make this more concrete, let's look at an example of building an ethanol molecule; a script for this is contained in Section~\ref{code:buildingatoms}.
The molecule is shown in Figure~\ref{fig:ase-ethanol}.
The molecule, as we observe it in these simulations, can be described by the position of each atom in the system.
That is what we access in the script with \verb'atoms.positions':


\vspace{1em}

\begin{center}
\begin{tabular}{r|r|r|r}
\hline
Atom & $x$ & $y$ & $z$ \\
\hline
C & 1.168181 & -0.400382 &   0.000000 \\
C & 0.000000 & 0.559462 &    0.000000 \\
O &-1.190083 & -0.227669 &   0.000000 \\
H &-1.946623 & 0.381525 &    0.000000 \\
H & 0.042557 & 1.207508 &    0.886933 \\
H & 0.042557 & 1.207508 &   -0.886933 \\
H & 2.115891 & 0.144800  &     0.000000 \\
H & 1.128599 & -1.037234  &  0.885881 \\
H & 1.128599 & -1.037234  & -0.885881 \\
\hline
\end{tabular}
\end{center}

\vspace{1em}


\begin{figure}
\centering
\includegraphics[width=0.3 \textwidth]{f/scripts/ethanol.pdf}
\caption{An ethanol molecule, as rendered by ASE.
\label{fig:ase-ethanol}
}
\end{figure}

The above literally gives the position of each atom (in \AA), in terms of the location of its nucleus in Cartesian ($x,y,z$) space.
(Again, if you are worried about the Heisenberg-principle ramifications of exactly specifying the position...hold that thought for now!)
Here, we'll refer to the vector of all positions as $\vec{x}$.
Each of the 9 atoms has 3 Cartesian coordinates, so this is a vector of length 27.
In general, $\vec{x}$ is a 3$N$-length vector, where $N$ is the number of atoms in the system.

If we specify $\vec{x}$ and the number of electrons---and in the absence of any external disturbances, like magnetic or electrostatic fields---then quantum mechanics tells us the electrons will be most stable in their ground-state configuration.
Since the ground-state electronic configuration is that configuration with the lowest energy, this means the ground-state energy is a unique function of the nuclear positions, $\vec{x}$.
In atomistics, we generally use an electronic structure calculator (such as density function theory, or DFT) or a force-field model to calculate this energy.
An example script that calculates the potential energy of ethanol, as performed in density functional theory, is given in \ref{code:getpotentialenergy}.
In this example script, we find the potential energy to be -4218.9998 eV.
Like all energies, this is with regard to an arbitrary reference energy---in general, the number by itself is basically meaningless; we will always be concerned with \emph{changes} in the energy.

\paragraph{Perturbing.}
Let's do a thought experiment, where we perturb one of the atoms, such as the first atom in the list (a carbon).
Each time we perturb the atom, we can calculate a new potential energy.
Let's say we make several perturbations, and plot them as in Figure~\ref{fig:PES-ethanol}.
If we keep doing this, we'll get out a shape of energy versus position.
This is a one-dimensional potential energy surface.

\begin{figure}
\centering
\includegraphics[width=2.0in]{f/PES-ethanol/drawing.pdf}
\caption{The change in the potential energy for perturbing a single atom in an ethanol structure.\label{fig:PES-ethanol}}
\end{figure}

Basic physics tells us that we can calculate the force on this atom (in this dimension) as the first derivative of the potential energy,\footnote{More elaborately, we sometimes think of the potential energy as the Helmholtz free energy of the electrons; that is, in the $T$, $V$, $N$ ensemble. If you think along these lines, then we would express the force as $F = \left( \partial A / \partial x \right)_{T, V, N}$, where $A$ is the Helhmoltz energy, $T$and $V$ are the temperature and system volume, and $N$ is the number of electrons.} as:

\[ F = - \frac{d E}{d x} \]


\section{3$N$--dimensional space}

Of course, we can move any atom in the system in the $x$, $y$, or $z$ direction.
If we have $N$ atoms, this means that the potential energy is a function of $3N$ dimensions.
The physics term for the $3N$-dimensional space ($\mathbb{R}^{3N}$) that fully specifies the position of every atom is \defn{configuration space}.
The coordinates of a specific configuration in this space are referred to as the \defn{generalized coordinates}, which we often write as a single tuple $\vec{q}$ containing all $3N$ coordinates necessary to describe the system:

\[ \v{q} =
\brm{x_1 & y_1 & z_1 & x_2 & y_2 & z_2 & \cdots & x_N & y_N & z_N}\T
\] 

\noindent
The $6N$-dimensional space ($\mathbb{R}^{6n}$) that also specifies the momentum of every atom is called \defn{phase space} (or sometimes state space).
Note that if we have both the position and momentum of every atom specified, then it is possible to know both the system's potential energy (from the atomic positions) and the system's kinetic energy (from the atomic momenta).
This becomes important for tasks such as molecular dynamics, which we'll introduce in a future class.

Therefore, we can express the potential energy as

\[ \Epot = f(\v{q}) \]

\noindent
The above is the mathematical expression of the \defn{potential energy surface} (PES).
This is often described as ``a surface in 3$N$-dimensional space''.
However, it is simpler to state that energy is a \emph{function} of atomic positions; the surface analogy helps us to conceptualize local minima and other features.
Earlier, we saw a 1-dimensional PES.
Figure~\ref{fig:PES-example} shows a 2-dimensional PES, which is the limit to what we can visualize, but we can already see the usefulness of the surface analogy.
Clearly, we have no ability to visualize a surface in $3N$ dimensions.

The force on any particular atom (in a single direction) is then the partial derivative of the potential energy:

\[ F_i = - \left(\frac{\partial \Epot}{\partial q_i}\right)_{q_{j \ne i}} \]

In general we can calculate single points\footnote{These single-point calculations are often referred to as a ``force call'', or if the system is evolving an ``ionic step''.} on the potential energy surface with an electronic structure calculator, such as density functional theory (DFT).
However, we generally don't have an explicit form for the full PES---methods like DFT take some time to find the ground-state electronic structure---and there's no way we can really visualize the PES for normal systems.
But we can \underline{sample} from the PES and use these points to determine properties of the system, and that's what this course is about.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{f/PES-example/PES.png}
\caption{Example of an arbitrary 2-dimensional potential energy surface, showing key regions.  From http://www.chem.wayne.edu/~hbs/chm6440/PES.html\label{fig:PES-example}}
\end{figure}

\section{Conservation of energy}

Let's discuss how the conservation of energy---the first law---works at the atomic level.
We examine a simulation of a Au dimer vibrating; which you can produce from the script provided in Section~\ref{code:dimervibration}.
This script runs molecular dynamics, which means it propagates motion according to Newton's Laws.
If we look at the potential and kinetic energies (you can try plotting by putting ``i, epot'' and ``i, ekin'' in the plot box of ase-gui), we see that it constantly interchanges kinetic and potential energy over the course of the atomic motion.
If we add together the kinetic and potential energy we will get a constant, straight line (within roundoff error); that is, energy is conserved and is traded off between kinetic and potential terms as the system propagates through time.
Thus, we can consider the dimer's vibration to be analogous to a pendulum, or a spring, or a ball rolling in a valley.
A plot of the kinetic and potential energy during this simulation is shown in Figure~\ref{fig:dimervibration}.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/dimervibration/out.pdf}
\caption{The total energy is conserved, changing form between kinetic and potential energy as a dimer vibrates. \label{fig:dimervibration}}
\end{figure}


\section{Regions of the PES}

By examining Figure~\ref{fig:PES-example}, we can divide the PES into regions.


\begin{itemize}
\item
\textbf{Local minima}: The region about a local minimum describes a stable state, such as a molecule, a phase, or a stable reaction intermediate. A system samples a region about a local minimum, typically within a statistical distribution, such as the Boltzmann distribution according to its temperature. The system is never precisely at the minimum point, except for fleeting moments. (All positions on the PES have infinitesimal probabilities.)

\item
\textbf{Transitions.}
To transition between local minima, we need to go through a higher energy region.
This generally means a reaction or a phase transition, and there is a whole terminology we'll get to, like minimum energy pathways, saddle points, transition-state regions, second order saddle points, transition path sampling, etc.
But generally we need to figure out how to traverse the PES to go from one local minimum to the next for a reaction to occur.

\end{itemize}


\paragraph{Ethanol PES.}
Consider again the example of ethanol; one possible reaction is to form \ce{CH4} + \ce{CH2O}.
That is,

\[ \ce{CH3CH2OH -> CH4 + CH2O} \]

\noindent
Of course, we're not likely to move directly from the ethanol region of the PES directly to this one without crossing any other local minima, or stable configurations.
That is, while the above equation can be called a reaction, it's not an \emph{elementary reaction} or a mechanism.
As one possibility of a mechanism, consider going through an intermediate state consisting of

\cen{CH3 + CH2OH}

\noindent and then in the next step the H is passed to the \ce{CH3} to get the final product.
So we could refer to this as a reaction pathway, or a reaction mechanism:

\cen{CH3CH2OH -> CH3 + CH2OH -> CH4 + CH2O}


\begin{figure}
\centering
\includegraphics[width=3.0in]{f/PES-ethanol/drawing2.pdf}
\caption{A hypothetical reaction pathway through the PES to convert ethanol into methane plus formaldehyde. \label{fig:PES-ethanol2}}
\end{figure}

An example of this pathway through the PES is shown in Figure~\ref{fig:PES-ethanol2}.
We have all seen diagrams that look like this before in chemistry courses.
What I want you to realize is that this is not a simple path in a single dimension, but a very complicated trip through coordinate space, which we must have some way to deal with if we are going to use atomistics to attempt to describe this.


In atomistic calculations we are often trying to figure out how to move through the PES and understand something: how a reaction occurs, how a material property changes or behaves, how water solvates something, how an enzyme works, etc.
Recall each single-point calculation of the PES will be computationally intensive, and we are exploring  a spatially complex space of 3$N$ dimensions.
In general we can't ``know'' the entire PES---we have to figure out how and where to sample it, and that is the art of atomistic simulations.

\section{Script(s)}

\subsection{Building atoms\label{code:buildingatoms}}

\pcode{f/scripts/buildingatoms.py}

\subsection{Calculating the potential energy\label{code:getpotentialenergy}}

In the below script, NWChem is a density functional theory (DFT) calculator, that calculates the electronic structure and returns the energy of the configuration.
The first six lines of the script (starting with \mintinline{python}{#SBATCH}) are not python commands, but tell the CCV how much processor power, time, and memory to allocate to the job.

\pcode{f/scripts/getpotentialenergy.py}

\subsection{Dimer vibration (via molecular dynamics)\label{code:dimervibration}}

\pcode{f/dimervibration/run.py}

\endgroup
