\begingroup
\chapter{Basics of bonding}

\newcommand{\ket}[1]{\ensuremath{\left| #1 \right\rangle}}
\newcommand{\bra}[1]{\ensuremath{\left\langle #1 \right|}}
\newcommand{\op}[1]{\ensuremath{\hat{\mathrm{#1}}}} % operator
\newcommand{\ebra}{\bra{\phantom{X}}}
\newcommand{\eket}{\ket{\phantom{X}}}
\newcommand{\braket}[2]{\ensuremath{\left\langle #1 \middle| #2 \right \rangle}}
\newcommand{\sandwich}[3]{\left\langle #1 \middle| #2 \middle| #3 \right \rangle}
\newcommand{\expect}[1]{\ensuremath{\left\langle \op{#1} \right\rangle}}
\newcommand{\ebraket}{\braket{\phantom{X}}{\phantom{X}}}
\setlength{\fboxsep}{0pt}
\newcommand{\eop}{\hat{\fbox{\phantom{X}}}}

\newcommand{\psia}{\ensuremath{\psi_\mathrm{A}}}
\newcommand{\psib}{\ensuremath{\psi_\mathrm{B}}}
\newcommand{\ca}{\ensuremath{c_\mathrm{A}}}
\newcommand{\cb}{\ensuremath{c_\mathrm{B}}}
\newcommand{\ve}{\varepsilon}
\newcommand{\haa}{\ensuremath{H_\mathrm{AA}}}
\newcommand{\hab}{\ensuremath{H_\mathrm{AB}}}
\newcommand{\hba}{\ensuremath{H_\mathrm{BA}}}
\newcommand{\hbb}{\ensuremath{H_\mathrm{BB}}}
\newcommand{\saa}{\ensuremath{S_\mathrm{AA}}}
\newcommand{\sab}{\ensuremath{S_\mathrm{AB}}}
\newcommand{\sba}{\ensuremath{S_\mathrm{BA}}}
\newcommand{\sbb}{\ensuremath{S_\mathrm{BB}}}
%\newcommand{\ala}{\ensuremath{\alpha_\mathrm{A}}}
%\newcommand{\alb}{\ensuremath{\alpha_\mathrm{B}}}
\newcommand{\ala}{\ensuremath{\varepsilon_\mathrm{A}}}
\newcommand{\alb}{\ensuremath{\varepsilon_\mathrm{B}}}
\newcommand{\Va}{\ensuremath{\op{V}_\mathrm{A}}}
\newcommand{\Vb}{\ensuremath{\op{V}_\mathrm{B}}}
\newcommand{\Ha}{\ensuremath{\op{H}_\mathrm{A}}}
\newcommand{\Hb}{\ensuremath{\op{H}_\mathrm{B}}}

In this topic, we qualitatively discuss how bonding can be interpreted in simple molecules as well as within metals.

\section{Bra-ket (Dirac) notation\label{sxn:braket}}

We will use the (mathematical) language of quantum mechanics to introduce the basics of bonding,  using the Bra-ket (Dirac) notation.
For those who have not had formal classes in this area, we provide a brief overview of how to read this notation here.

The mathematics here can be thought equivalently in an integral form or a linear algebra form.
For the qualitative treatment we employ in the following, it's simplest to think of them in terms of their linear algebra analogs, as in that way the rules for manipulation will become clear.


\paragraph{Basic symbols.}
The state of the system is represented by a ``ket'' in Dirac notation; this will sometimes be referred to as the wavefunction and it is often said to contain all the information about the system.
The ket is represented by $\eket$, and is analogous to a column vector:

\[ \ket{x} \sim 
		  \left[
								\begin{array}{c}
										  \# \\
										  \# \\
										  \# \\
										  \# \\
										  \vdots \\
								\end{array}
		  \right]
\]

\noindent
Each ket has a compliment, the ``bra'': $\ebra$, which contains the same information as the ket.\footnote{A bra \bra{x}\ can be created by taking a ``Hermitian transpose'' (or conjugate transpose) of a ket \ket{x}. This means we transpose its geometry (from $n\times1$ to $1\times n$) and take the complex conjugate of each element in the matrix (\textit{i.e.}, change the sign on the imaginary component of each element). For our purposes, don't worry that the elements in these vectors and matrices are complex numbers; you can just think of the bra as being the transpose of the ket.}
It is analogous to a row vector:

\[ \bra{x} \sim 
		  \left[ \#\, \#\, \#\, \#\,  \cdots \right]
\]

\noindent
An ``operator'' is used to extract information (observables) from the kets and bras.
An operator is represented with a hat, and is analogous to a matrix:

\[ \op{X} \sim
		  \left[ \begin{array}{ccccc}
								\#\, \#\, \#\, \#\,  \cdots \\
								\#\, \#\, \#\, \#\,  \cdots \\
								\#\, \#\, \#\, \#\,  \cdots \\
								\#\, \#\, \#\, \#\,  \cdots \\
								\vdots
		  \end{array} \right]
\]

\paragraph{Basic properties.}
Given the analogs to linear algebra, we can quickly see how these are manipulated.
The product of a ket and a scalar $a$ commutes:

\[ a \ket{x} = \ket{x} a \]

\noindent
The same can be said for the product of a scalar with either a bra or operator.

The product of a bra and a ket is referred to as a bra--ket, and is written as

\[ \braket{x}{y} \equiv \bra{x} \cdot \ket{y} \]

\noindent
Then, we can see how they interrelate by the rules of matrix algebra:

\[ \ebraket \sim 
		  \left[ \#\, \#\, \#\, \#\,  \cdots \right]
		  \left[
								\begin{array}{c}
										  \# \\
										  \# \\
										  \# \\
										  \# \\
										  \vdots \\
\end{array}
\right]
= \#
\]

\noindent
That is, a bra--ket is just a number (a scalar).
Note that this operation does \emph{not} commute: $\braket{x}{y} \ne \ket{y}\bra{x}$.
In fact, \braket{x}{y} gives a scalar, while $\ket{y} \bra{x}$ gives us an $n \times n$ matrix.\footnote{So $\ket{y}\bra{x}$ is actually an operator!}

If we apply an operator to a ket we get another ket:

\[ \eop \eket = \eket \]

\noindent
(Note in principle it gives a new ket, as in $\op{A} \ket{x} = \ket{y}$.)

Finally, if we sandwich an operator inside of a bra and ket we also get a number:

\[ \ebra \eop \eket = \# \]

\noindent and the expectation value of an operator (e.g., the expectation value of the Hamiltonian is the average energy) can be found by:


\[ \left\langle \op{O} \right\rangle =
		  \frac{ \sandwich{\psi}{\op{O}}{\psi} } {\braket{\psi}{\psi}} 
\]


\paragraph{The Schr{\"o}dinger equation.}
The most famous use of bra--ket notation is in the (time-independent) Schr{\"o}dinger equation, which we can write as

\begin{equation}\label{eq:schroedinger} \op{H} \ket{\psi} = \varepsilon \ket{\psi} \end{equation}

\noindent
If we examine the pieces of this, we see that \op{H} is a matrix, which multiplies a vector \ket{\psi}.
In this case, we see that upon this operation we get back the same vector \ket{\psi}, times a constant.
Thus, the Schr{\"o}dinger equation is an eigenvalue problem, where the energies $\varepsilon$ and states \ket{\psi} are the allowed energy levels and associated states of the system, respectively.
The way to think about working with this is we first ``construct'' the Hamiltonian \op{H}, which is comprised of the kinetic and potential energy operators specific to the system.
Then the eigenvalues and eigenvectors give us the energy levels and system states.

Note that we can left-multiply the above equation by \bra{\psi} to get

\[ \sandwich{\psi}{\hat{H}}{\psi} = \varepsilon \braket{\psi}{\psi} \]

\noindent
where we have ``commmuted'' $\varepsilon$, a scalar, to the front since scalar multiplication is commutative.
Note that despite all the funny symbols, both sides of the above equation are just scalars.
We can re-arrange as

\[ \frac{\sandwich{\psi}{\hat{H}}{\psi}}{\braket{\psi}{\psi}} = \varepsilon \]

\noindent
and we see that the energy $\varepsilon$ is the expectation value of the Hamiltonian operator \expect{H}.
Note that this is the energy associated with the particular wavefunction \ket{\psi}; that is, in a particular orbital or system state.\footnote{Statistical mechanics can give you the average over a population of molecules, where molecules are in various states of excitement.}
Note also that in equation~\eqref{eq:schroedinger}, we could have replaced our ket \ket{\psi} with any multiple of itself---\textit{i.e.}, a ket like $a \ket{\psi}$ would also satisfy this equation, by the commutative property of scalar multiplication.
Thus, we are free to choose \ket{\psi} to be of any magnitude we like, and by convention \ket{\psi} is often ``normalized'' such that $\braket{\psi}{\psi} = 1$, a property we will use in the coming notes.

\section{Basics of bonding\label{sxn:basics-of-bonding}}

We will examine a simple model of bonding between two atoms in the following, following closely the logic of Chorkendorff and Niemantsverdriet~\cite{Chorkendorff2005}.
The key points you should appreciate from this treatment are:

\begin{itemize}
\item The more overlap in the initial orbitals, the more interaction.
\item The closer together the two original energy levels, the more splitting.
\end{itemize}

\paragraph{Bonding between two atoms.}
The Schr{\"o}dinger equation describes the electronic configuration:

\[ \op{H} \ket{\psi} = \varepsilon \ket{\psi} \]

\noindent
For a qualitative understanding, consider a H atom with one proton and one electron.
The Hamiltonian ($\op{H}$) contains terms describing the kinetic and potential energy; the eigenvalues $\varepsilon$ are the energy levels of the orbitals, and the wavefunctions ($\ket{\psi}$) describe the orbitals themselves.
(The square of the wavefunction gives the electron's probability density.)

\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{f/bonding/drawing.pdf}
\caption{Systems A and B are the cases of two individual atoms (nucleus and an electron). The final system combines them, and our goal is to understand any bonding that may occur. \label{fig:bonding}}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=4.5in]{f/coulomb-wells/out.pdf}
\caption{Potential energy wells around nucleus of Atom A, Atom B, and the combined system, which is assumed to add in this case. \label{fig:coulomb-wells}}
\end{figure}

We will consider writing the Schr{\"o}dinger equation to describe forming a bond between two atoms denoted A and B, as in Figure~\ref{fig:bonding}.
Starting with the isolated atom A, the electron is attracted to the nucleus and thus feels a potential well as shown qualitatively in Figure~\ref{fig:coulomb-wells}.
The Schr{\"o}dinger equation for the isolated systems around Atom A and Atom B are

\[ \left( \frac{-\hbar^2}{2m} \hat{\nabla}^2 + \op{V}_\mathrm{A} \right) \ket{\psi_\mathrm{A}} = \varepsilon_\mathrm{A} \ket{\psi_\mathrm{A}} \]

\[ \left( \frac{-\hbar^2}{2m} \hat{\nabla}^2 + \op{V}_\mathrm{B} \right) \ket{\psi_\mathrm{B}} = \varepsilon_\mathrm{B} \ket{\psi_\mathrm{B}} \]

\noindent
or just 

\[ \Ha \ket{\psia} = \ala \ket{\psia} \]

\[ \Hb \ket{\psib} = \alb \ket{\psib} \]

When the Schr{\"o}dinger equation is solved in the one-electron case, for either A or B, the resulting lowest-energy electronic orbital predicts an electron density (or probability) that is roughly Gaussian shaped, centered around the nucleus of the respective atom.
This corresponds to the s orbital, and in 3 dimensions it is spherically symmetric, as shown in Figure~\ref{fig:bonding}.
Higher-energy solutions correspond to p orbitals, d orbitals, etc.

When the systems are brought near each other, we will assume that the potentials add, as in figure~\ref{fig:coulomb-wells}.
Then, for a single electron (that is, neglecting electron--electron interactions) we can write the new Schr{\"o}dinger equation as:

\[ \left( \frac{-\hbar^2}{2m} \hat{\nabla}^2 + \op{V}_\mathrm{A} + \op{V}_\mathrm{B} \right) \ket{\phi} = \varepsilon \ket{\phi} \]

\noindent
or just

\[ \op{H} \ket{\phi} = \varepsilon \ket{\phi} \]

So that we can easily compare the interacting system to the isolated systems, we'll assume that the wavefunction solution $\ket{\phi}$ can be approximated as a linear combination of the atomic orbitals for Atoms A and B:

\[ \ket{\phi} \approx \ca \ket{\psia} + \cb \ket{\psib} \]

\noindent
where we only need determine the coefficients \ca\ and \cb\ to determine the shape of the new orbital(s).
Such a linear combination of atomic orbitals, or LCAO, is a relatively common approximation for the wavefunctions of chemical systems.
You can even find it as a basis set in GPAW, for example.
Plugging this into the combined-system Schr{\"o}dinger equation:

\[ \op{H} \left(\ca \ket{\psia} + \cb \ket{\psib} \right)
= \varepsilon \left(\ca \ket{\psia} + \cb \ket{\psib} \right) \]

\noindent
Using what we learned in \secref{sxn:braket}, we can convert this expression to scalars by pre-multiplying with a bra.
Let's start by pre-multiplying with $\bra{\psia}$ and manipulating the expression:

\[ \bra{\psia} \op{H} \left(\ca \ket{\psia} + \cb \ket{\psib} \right)
= \bra{\psia} \varepsilon \left(\ca \ket{\psia} + \cb \ket{\psib} \right) \]

\noindent
Separating the terms that multiply \ca\ and \cb\ gives

\[ \left( \underbrace{\sandwich{\psia}{\op{H}}{\psia}}_{\equiv \haa} - \ve \underbrace{\braket{\psia}{\psia}}_{\equiv \saa} \right) \ca
+ \left( \underbrace{\sandwich{\psia}{\op{H}}{\psib}}_{\equiv \hab} - \ve \underbrace{\braket{\psia}{\psib}}_{\equiv \sab} \right) \cb
=0 \]

\noindent
\haa\ and \hab\ are referred to as matrix elements of the Hamiltonian whereas \saa\ and \sab\ are referred to as matrix elements of the overlap.
The overlap refers to the overlap of the original atomic orbitals, and can range from 0 (no overlap) to 1 (complete overlap), assuming the original wavefunctions are normalized.
Note from \secref{sxn:braket} that we can think of $\braket{\psi_i}{\psi_j}$ as the dot product of two vectors, so this matches our intuitive understanding of dot products.
Since \saa\ is the overlap of $\ket{\psia}$ with itself, it will be equal to 1, but we will keep it in our equations for the moment just be clear where all the terms come from.
Also, note that if the wavefunctions are normalized, then $\haa$ is equal to $\varepsilon_A$, but we'll also just keep this as \haa\ in our equation for now.

We can condense this equation down to the next equation shown.
If we repeat the procedure but pre-multiply by $\bra{\psib}$ instead, we end up with a second, parallel equation, which is the second equation shown below.
\[ (\haa - \ve \saa) \, \ca + ( \hab - \ve \sab ) \, \cb = 0 \]
\[ (\hba - \ve \sba) \, \ca + ( \hbb - \ve \sbb ) \, \cb = 0 \]
\noindent
We can re-write this in linear-algebra form as:
\[ \begin{bmatrix}
 \haa - \ve \saa & \hab - \ve \sab \\
 \hba - \ve \sba & \hbb - \ve \sbb
\end{bmatrix}
\begin{bmatrix} \ca \\ \cb \end{bmatrix} = 
\begin{bmatrix} 0 \\ 0 \end{bmatrix}
\]

\noindent
You might remember from a linear algebra course that by Cramer's rule, this has a non-trivial solution when the determinant of the matrix is zero, which leads to
\begin{equation}\label{eqn:bonding-two-levels}
\ve = 
\frac{\haa + \hbb - 2 S \hab \pm \sqrt{(\haa - \hbb)^2 - 4 (\haa + \hbb) S \hab + 4 \hab^2 - 4 \haa \hbb S^2}}
{2 \left(1 - S^2\right)}
\end{equation}

\noindent
This long equation has several simplifications: we have substituted $\saa = \sbb = 1$ (mentioned above) plus $\hab = \hba$ and $\sab = \sba \equiv S$ (both by symmetry).

From the two roots of equation~\eqref{eqn:bonding-two-levels}, we can already see that we have the qualitatively correct result that two energy levels result from the two electronic orbitals ``merging''. 
If the overlap between the two original orbitals is small, then $S$ is small and we can estimate $\haa = \ala$ and $\hbb = \alb$, where \ala\ and \alb\ are the energy levels of the original two electrons.
Note the nature of this approximation: the Hamiltonian in $\haa = \sandwich{\psia}{\op{H}}{\psia}$ contains $\Va + \Vb$ (the merged potential wells), while \ala\ would come from a Hamiltonian with only \Va.
However, since \psia\ exists primarily in the spatial region of \Va\ when the overlap is low, this is a reasonable simplification.
When these approximations are made we have the simpler equation that we will use for our qualitative descriptions:
%
\begin{equation} \label{eqn:diatomic-bonding}
\ve = \frac{\ala + \alb - 2 S \hab \pm \sqrt{ (\ala - \alb)^2 + 4 \hab^2}}{2}
\end{equation}

\noindent
Note $S>0$ and $\hab<0$.

\subsection{The homonuclear diatomic case}

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/diatomic-bonding/homonuclear.pdf}
\hspace{3em}
\includegraphics[width=0.2 \textwidth]{f/diatomic-bonding/filling.pdf}
\caption{Simplified bonding picture for homonuclear diatomic bonding, such as \ce{H2}.\label{diatomic-bonding}}
\end{figure}

When the two atoms A and B are the same (\textit{e.g.}, \ce{H2}), then $\ala = \alb$, and equation~\eqref{eqn:diatomic-bonding} becomes:
%
\[
\ve = \ala  - S \hab \pm \hab
\]

\noindent
A molecular orbital picture of this is shown in Figure~\ref{diatomic-bonding}.
Starting from the two equal isolated-atom electronic energies $\ala$, the $-S \hab$ term is an energy penalty (recall that $\hab$ is negative).
Qualitatively, this is often thought of as an orthogonalization penalty: as the two original electronic orbitals encroach on one another, by the Pauli exclusion principle they must ``orthogonalize'' (that is, get out of each other's way).
Since the two original orbitals were in their lowest energy configuration, there must be an energy penalty associated with this.
Second, we have the splitting due to the hybridization of the wavefunctions, which is a $\pm \hab$ correction from this new level, leading to bonding and antibonding orbitals.

A schematic of this view of forming bonds is shown in the right-hand side of the figure.
If there are two bonding electrons (such as \ce{2 H -> H2}), the final state has both electrons in a lower energy state than that of the energy levels of the isolated atoms.
However, if there are four bonding electrons (such as \ce{2 He -> He2}) then both the bonding and antibonding orbitals are filled.
Because of the upshift by $-S \hab$, the split orbitals are not equidistant from the energy level $\ala$, but in net have a higher energy.
Thus, the filling with four electrons is unstable, leading to a repulsive state, which we can use to understand why \ce{H2} forms but \ce{He2} does not.

Note that both $\hab$ (indirectly) and $S$ (directly) are functions of the amount of overlap, so the short picture is that \emph{the greater the overlap, the greater the splitting}.

\subsection{The heteronuclear diatomic case}

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/diatomic-bonding/heteronuclear.pdf}
\hspace{3em}
\includegraphics[width=0.3 \textwidth]{f/diatomic-bonding/splitting.pdf}
\caption{Simplified bonding picture for heteronuclear diatomic bonding.\label{fig:diatomic-bonding-heteronuclear}}
\end{figure}

In the case when the two atoms are not the same, the picture changes somewhat, and if we define $\delta \equiv \alb - \ala$ as the distance between the two energy levels (where B has the higher level) we can approximate equation~\eqref{eqn:diatomic-bonding} as
%
\begin{align*}
\ve_- &= \ala - S \hab - \frac{\hab^2}{\delta} \\
\ve_+ &= \alb - S \hab + \frac{\hab^2}{\delta}
\end{align*}

\noindent
under the assumption that $|\hab | \ll \delta$ and making a Taylor series approximation.
The schematic of how these orbitals split is shown in Figure~\ref{fig:diatomic-bonding-heteronuclear}.
Again, we start from each of the atomic orbitals \ala\ and \alb\ and apply the penalty $-S \hab$.
The splitting then occurs from these higher and lower states, and is approximated by $\hab^2/\delta$.

Qualitatively, this is similar to the homonuclear case, where there is an orthogonalization term and a hybridization (splitting) term.
However, here, one important point arises: the degree of splitting is a strong function of the difference in the energy levels.
As this distance ($\delta$) grows, the amount of splitting decreases, and it is at a maximum when the two energy levels are equivalent and approaches zero when they are far apart.
This is shown qualitatively on the right in Figure~\ref{fig:diatomic-bonding-heteronuclear}.
The conclusion from this is that \emph{the original energy levels have to be similar in energy for significant hybridization to occur.}

\endgroup
