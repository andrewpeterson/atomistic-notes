\begingroup
\chapter{Adsorption: bonding to surfaces \label{ch:adsorption}}

\newcommand{\ket}[1]{\left| #1 \right\rangle}
\newcommand{\bra}[1]{\left\langle #1 \right|}
\newcommand{\op}[1]{\hat{\mathrm{#1}}} % operator
\newcommand{\ebra}{\ensuremath{\bra{\phantom{X}}}}
\newcommand{\eket}{\ket{\phantom{X}}}
\newcommand{\braket}[2]{\left\langle #1 \middle| #2 \right \rangle}
\newcommand{\sandwich}[3]{\left\langle #1 \middle| #2 \middle| #3 \right \rangle}
\newcommand{\ebraket}{\braket{\phantom{X}}{\phantom{X}}}
\setlength{\fboxsep}{0pt}
\newcommand{\eop}{\hat{\fbox{\phantom{X}}}}

\newcommand{\ve}{\varepsilon}
\newcommand{\ka}{\ensuremath{\ket{\mathrm{a}}}}
\newcommand{\ea}{\ensuremath{\varepsilon_{\mathrm{a}}}}
\newcommand{\Va}[1]{\ensuremath{V_{\mathrm{a}#1}}}

The bonding of small molecular fragments to metal surfaces is the basis of heterogeneous catalysis, which is responsible for the manufacture of the vast majority of chemicals and fuels.
Here, we look at some basic models that help to understand bonding at surfaces.

\section{Newns--Anderson model}

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/newns/drawing.pdf}
\caption{An adsorbate approaching a surface, with labeled energies and eigenstates before adsorption.\label{newns}
}
\end{figure}

Mathematically, the Newns--Anderson model takes a similar approach to the basic bonding situation presented for a diatomic molecule in Section~\ref{sxn:basics-of-bonding}.
Here, we give a brief description of this model and it's application to some limiting cases of bonding at a surface.
For a more detailed paper, see Newns' original 1969 paper~\cite{Newns1969} or more recent textbooks.~\cite{Chorkendorff2005,Norskov2014}

Consider an atom approaching a surface of metal atoms as in Figure~\ref{newns}, which has eigenstate $\ka$ and energy \ea.
The metal that it is interacting with has eigenstates $\ket{k}$ and energy eigenvalues $\ve_k$, where here $k$ is an index running effectively to infinity (giving the continuum of states within the metal).
After adsorption, the Schr{\"o}dinger equation can be written as the eigenvalue problem

\begin{equation}\label{eq:newns-schrodinger}
		  \op{H} \ket{\Psi_i} = \ve_i \ket{\Psi_i}
\end{equation}

\noindent where the subscript $i$ is just used to emphasize that there are many pairs of eigenvalues $\ve_i$ and eigenstates $\ket{\Psi_i}$.

Like in the diatomic case, we assume that the combined wavefunction can be assembled as a linear combination of the parent wavefunctions.
That is,

\begin{equation}\label{eq:newns-lcao}
		  \ket{\Psi_i} = c_{\mathrm{a},i} \ket{\mathrm{a}} + \sum_k c_{k, i} \ket{k}
\end{equation}

\noindent Again, emphasizing that the italicized ``$k$'' is a counting index over the original states in the metal, while the roman ``a'' represents a single state.
We will follow the same procedure as in the diatomic case, where we build a system of linear equations to find a solution for the coefficients of equation~\eqref{eq:newns-lcao} and energy eigenvalues $\ve_i$ of equation~\eqref{eq:newns-schrodinger} by pre-multiplying by each of $\bra{\mathrm{a}}$, $\bra{1}$, $\bra{2}$, $\cdots$, where the integers inside \ebra's represent values of $k$.
As we do so we make the following assumptions:
\begin{enumerate}
\item The basis set is orthogonal:

		  \[ \braket{\mathrm{a}}{k} = 0 \]
					 \[ \braket{k}{l} = 0, \, \text{when} \, k \ne l \]

where $\ket{l}$ is also an eigenstate of the metal before hybridization; that is, $l$ is just an index in the series shown in Figure~\ref{newns}.

\item The matrix elements can be approximated as interacting with their own local potential energy:

		  \[ \sandwich{\mathrm{a}}{\op{H}}{\mathrm{a}} = H_\mathrm{aa} = \ve_a \]
		  \[ \sandwich{k}{\op{H}}{k} = H_\mathrm{kk} = \ve_k \]

\item The interaction terms can be written as:

		  \[ \sandwich{k}{\op{H}}{l} = 0, \, \text{when} \, k \ne l \]
		 \[ \sandwich{\mathrm{a}}{\op{H}}{k} = V_{\mathrm{a}k} \]
		 \[ \sandwich{k}{\op{H}}{\mathrm{a}} = V_{k\mathrm{a}} = V_{\mathrm{a}k} \]
\end{enumerate}

\noindent
Following the same procedure as in Section~\ref{sxn:basics-of-bonding}, we get an infinitely sized matrix similar to what we had for the diatomic case.
The solution of this determinant gives the eigenvalues, $\ve_i$.

\[
		  \begin{vmatrix}
					 H_{\mathrm{aa}} - \ve & \Va{1} & \Va{2} & \Va{3} & \Va{4} & \cdots \\
					 \Va{1} & H_{11} - \ve & 0 & 0 & 0 & \cdots \\
					 \Va{2} & 0 & H_{22} - \ve & 0 & 0 & \cdots \\
					 \Va{3} & 0 & 0 & H_{33} - \ve & 0 & \cdots \\
					 \Va{4} & 0 & 0 & 0 & H_{44} - \ve & \cdots \\
					 \vdots & \vdots & \vdots & \vdots & \vdots & \ddots \\
		  \end{vmatrix}
		  = 0
\]


\noindent
A detailed mathematical treatment (see~\cite{Newns1969} or \cite{Chorkendorff2005}) can lead to the adsorbate-projected density of states:

\[ n_\mathrm{a} (\ve) = \frac{1}{\pi} \frac{\Delta(\ve)}{ \left[ \ve - \ve_a - \Lambda(\ve) \right]^2 + \Delta(\ve)^2} \]

\noindent where $\Delta (\ve)$ and $\Lambda(\ve)$ are defined in terms of the matrix elements $V_{\mathrm{a}k}$:

\[ \Delta (\ve) = \pi \sum_k \left| \Va{k} \right|^2 \delta(\ve - \ve_k) \]

\[ \Lambda (\ve) = \frac{P}{\pi} \int_{-\infty}^{+\infty} \frac{\Delta (\ve') d \ve'}{\ve - \ve'} \]

\noindent (where $P$ is the Cauchy principle value).
Note that when the energy level $\ve_k$ is close to that of \ea, the coupling $\Va{k}$ will be strong, and for energies ``near'' the adsorbate's pre-hybridized level \ea, $\Delta (\ve)$ will look essentially like the metal's (pre-hybridized) density of states.
As the energy level $\ve_k$ gets further from \ea, this will dampen and diminish towards zero as the coupling term \Va{k} diminishes.
Thus, we can intuitively think of $\Delta(\ve)$ as the metal's density of states near the adsorbate's energy level, for further discussion see Chorkendorff and Niemantsverdriet's textbook~\cite{Chorkendorff2005}.
This adsorbate-projected density of states can provide physical insight of how we should expect bonding at surfaces to behave under some idealized conditions, like the jellium model or tight-binding.


\paragraph{Case 0: Uniform density of states.}
We will start with the most idealized case, in which we take the metal's density of states to be constant with respect to energy.
If the we look at the interaction with a metal of constant DOS, $\Delta = \Delta_0$ and $\Lambda = 0$ and 

\[ n_\mathrm{a} (\ve) = \frac{1}{\pi} \frac{\Delta_0}{ \left[ \ve - \ve_a \right]^2 + \Delta_0^2} \]

\noindent
We get something peaked near $\ve_a$ from this distribution, as is shown in Figure~\ref{newns-constant-dos}.
Can we understand this qualitatively?
To do so, consider the band as made up of a large number of discrete energy levels with uniform spacing, as shown in the right-hand side of the same figure.
This makes binding of an atom at a metal fundamentally different than binding to another molecular system: the states immediately are seen to broaden upon hybridization.


\begin{figure}
\centering
\includegraphics[width=0.3 \textwidth]{f/newns/constant-dos.pdf}
\includegraphics[width=0.69 \textwidth]{f/newns/constant-dos-discrete.pdf}
\caption{\label{newns-constant-dos}
(Left) The adsorbate-projected density of states in the Newns--Anderson model as an adsorbate with a single energy level interacts with a metal with a uniform density of states.
		  (Right) A qualitative interpretation of why this distribution arises, using discrete energy levels uniformly spaced in the metal's band as a proxy for a uniform density of states.
}
\end{figure}


\paragraph{Case 1: Jellium.}
Jellium is qualitatively similar to the uniform case considered above, but the density of states scales with $\sqrt{\ve}$.
Qualitatively, we get similar broadening as the uniform density-of-states case, but is accompanied by a slight downshift.
(You should be able to work out with a qualitative discrete picture like that shown in Figure~\ref{newns-constant-dos} why this is the case.)
Since the new energy has downshifted from the adsorbate's original energy, this always results in an attractive situation; in other words, a bond.



\paragraph{Case 2: Tight-binding (d-band).}
The tight-binding approximation is a realistic approximation for electrons in the d-band of a metal.
Roughly, we can consider the density of states in the tight-binding model to be given by a semicircular segment of a cosine function:

\[ \ve_0 +  2 V \cos (ka) \]

\noindent
In this case, the tighter density of states leads to a tighter distribution in $\Delta(\ve)$ function.
We won't step through the math in this case, leaving that to the references given above, but this leads to both a broadening and a splitting of energy levels.
It's again simple to rationalize how this can arise if we instead consider the case of discrete energy levels as shown in Figure~\ref{newns-tight-binding}.


\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/newns/tight-binding.pdf}
\caption{\label{newns-tight-binding}
A qualitative picture of how splitting emerges when an adsorbate hybridizes with a metal with a relatively localized density of states, such as in the tight-binding model.
}
\end{figure}


\section{The d-band model}

The d-band model~\cite{Hammer1995,Hammer1995a} relates concepts developed in the Newns--Anderson model to the strength of binding at surfaces, and uses these concepts to rationalize trends in adsorbate binding strengths across transition-metal surfaces.

\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/Norskov_2014_8-1.png}
\caption{\label{d-band-model}
Conceptual schematic of the d-band theory of adsorption.
Image is from reference~\cite{Norskov2014}.
}
\end{figure}

Figure~\ref{d-band-model} shows the qualitative interpretation of the d-band model, which is readily understood from the concepts of the Newns--Anderson model presented in the previous section.
The left-hand side (marked ``Vacuum level'') shows the energy level of the adsorbate, for example an H atom, when it is in the vacuum and not interacting with the metal surface.
Here, it is still a discrete atomic/molecular orbital.

The far right-hand side of the figure shows the two major components of transition-metal band structures: an sp band and a d band.
The sp band is broad and increases slowly with energy as a jellium-type model would suggest.
The d band is relatively narrow and localized and is idealized as a tight-binding cosine segment.
Note that in this picture they both intersect the Fermi level and are only partially filled; the degree of filling is determined by the number of electrons in the system.
Note also that the lone atom's energy level is lower than the Fermi level of the metal; if we consider this to be in an electronic structure calculation this had better be the case, otherwise that state would be initially unoccupied.

As the adsorbate is brought to the metal surface, the hybridization between these electronic orbitals is considered to occur in two steps.
In the first step, the adsorbate's energy level hybridizes with the broad sp band of the metal; as predicted by Newns--Anderson, this leads to a broadening and a downshift.
Note that because of the downshift this is always an attractive interaction.
The sp bands across transition metals are considered to be approximately the same, so although this interaction is strong (and contributes the bulk of the binding strength) there is little variation.
This new state is shown as the second density of states in Figure~\ref{d-band-model}.

In the second conceptual step, this pre-hybridized state is considered to interact with the metal's d-band, resulting in the splitting discussed in the Newns--Anderson section; however, this splitting is now considered between the sp-band--adsorbate pre-hybridized state rather than just the adsorbate.
This splitting leads to low-lying bonding states and higher-lying antibonding states.
These are showed in the third density of states plot in Figure~\ref{d-band-model}.

The adsorbate-projected density of states are important to consider in relation to the metal's Fermi energy.
The Fermi energy of the metal will be unchanged throughout this process---recall that the metal is effectively infinite in size compared to the adsorbate.
So these states will be filled if they are below the Fermi level and empty if they are above it.
The relative strength of the bond is controlled by the energy of the bound state (3rd plot) relative to the unbound state (1st plot).
Thus, the antibonding states are those that are above the original adsorbate's energy level as they will lead to a net destabilization, and the degree to which these antibonding states are filled is ultimately controlled by the height of the d-band (roughly, the central moment of the d-band or the ``d-band center'') compared to the Fermi level.

Thus, the qualitative interpretation of the d-band model is that a higher d-band center leads to a more reactive surface.

\section{Adsorbate scaling relations}

\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/adsorbate-scaling/drawing.pdf}
\caption{\label{adsorbate-scaling}
		  The original scaling relations in catalysis, from reference~\cite{Abild-Pedersen2007a}.
		  The different colored curves refer to different surface facets, while the different sloped curves refer to different adsorbates; e.g., CH, \ce{CH2}, or \ce{CH3}.
		  See the original reference for full descriptions.
}
\end{figure}

A key insight in catalysis theory is that the binding strength of adsorbed species on transition metal surfaces is highly correlated across surfaces~\cite{Abild-Pedersen2007a}.
Figure~\ref{adsorbate-scaling} shows these ``scaling relations'' as first presented for adsorbed species such as CH$_x$, NH$_x$, OH$_x$, and SH$_x$.
We can immediately see that there are strong, linear correlations between similar adsorbates across several electron--volts of variation.

Interestingly, this was shown to be highly related to the valency of the adsorbate---that is, the number of ``dangling bonds'' that species would have were it not adsorbed.
For example, the slope of \ce{CH2}, with two dangling bonds, when plotted versus \ce{C}, with four dangling bonds, is approximately $\frac{1}{2}$, while that of \ce{CH3} vs \ce{C} is $\frac{1}{4}$.
This pattern was observed to hold for the adsorbates shown in Figure~\ref{adsorbate-scaling}.

Other forms of these scaling relations have been reported for transition-states versus adsorbed species, as the transition-state itself can just be considered as a special adsorbate.

These scaling relations have been used to rationalize many catalytic reactions and to identify new promising materials.
Interestingly, they also are sometimes considered to limit what can be done with catalysts; e.g., catalysts cannot be tuned arbitrarily due to these strong correlations, and these relations have been used to explain why improved catalysts have been hard to find for several reactions.~\cite{Norskov2004,Peterson2012}

\endgroup
