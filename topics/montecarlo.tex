\begingroup
\chapter{Monte Carlo methods}

\newcommand{\kb}{\ensuremath{k_\mathrm{B}}}
\newcommand{\etal}{\textit{et al.}}

Generally, a Monte Carlo (MC) method is one that relies on random number generation, and they can take many forms.
Here we'll discuss two forms.
The Metropolis algorithm (a Markov-chain Monte Carlo approach) is a technique which comes from atomistics and now sees application outside of atomistics, notably in Bayesian statistics.
The second method, kinetic Monte Carlo, is a manner of performing kinetic simulations in an alternate way to the commonly employed microkinetic model, which is an atomistic approach instead of a mean-field (continuum) approach.

\section{Simple Monte Carlo integration}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{f/monte-carlo-area-shapes/drawing2.pdf}
\caption{We would like to know the area of the shape on the left.
A possible solution is to draw a rectangle of known area around the shape, then throw darts at the board and record the fraction inside the inner shape; this will approach its normalized area as the number of darts thrown gets large. \label{fig:monte-carlo-area-shapes}}
\end{figure}

We start with a very basic introduction to Monte Carlo integration.
Consider the shape shown in the left of Figure~\ref{fig:monte-carlo-area-shapes}, and suppose we want to know its area.
Properly, we can find this by integrating a function that defines the shape,

\[ A_\mathrm{shape} =  \iint f(x,y) \, dx \, dy \]

\noindent
where

\[ f(x,y) = 
 \left \{ \begin{array}{ll}
1, \, \, \text{if the point $(x,y$) is inside the shape} \\
0, \, \, \text{else}
\end{array} \right.  \]

\noindent
and the domain of the integral must be large enough to include the shape.
Although this is a valid mathematical approach, it tells us nothing about how to solve this problem in practice; for example, if we could even write $f(x,y)$, it would likely be so complex that our hopes of analytically solving the integral would be quite unrealistic.

We thus turn to numerical approaches to solve this integral.
One obvious way to do this is to divide the inside into a grid, and count the number of squares fully inside the shape.
As the grid mesh gets finer, this will approach the correct answer, but constantly underestimating the true area by a smaller and smaller amount.
If we are clever, we might simultaneously deploy a method that counts the number of squares at least partially inside the shape, which would consistently over-estimate the true area, thus we would have a constant bracket on ou uncertainty.

A different method is to rely on random numbers.
Let's consider the right-hand side of Figure~\ref{fig:monte-carlo-area-shapes}.
Here, we will draw a box of known area around our original shape, and examine the ratio of the area of our shape to the area of the known box:

\begin{equation}\label{area-ratio}
\frac{A_\mathrm{shape}}{A_\mathrm{total}}
= \frac{ \iint f(x,y) \, dx \, dy} { \iint dx \, dy}
= \frac{ \iint f(x,y) \, dx \, dy} { L \times W}
\end{equation}

\noindent where the bounds of integration are the bounds of the outer rectangle, and $f$ is the definition of the shape from above.

In simple Monte Carlo integration, we ``throw darts'' at the board, and count the number of darts that land inside our shape.
(More properly, we randomly sample points in the $x,y$ domain.)
In this case our integral just becomes

\[ \frac{A_\mathrm{shape}}{A_\mathrm{total}} \approx \frac{N_\text{inside}}{N_\text{total}} \]

\noindent where the two $N$'s are the number of random points inside the shape (that is, where $f=1$), and the total number of random points generated, respectively.
Since it's trivial to find the total area, we can then easily estimate the area in our shape, given enough random points.
That is, if we multiply our Monte Carlo estimate of the ratio by the total area, we have the estimate of the area of our shape.
Thus, if we can throw enough darts in a truly random fashion, we can avoid doing a messy integral (or counting grid squares).

In practice, we often use this when we could not write a simple analytical form for $f(x,y)$.
Imagine if to evaluate $f$ we had to perform an electronic structure calculation; \textit{e.g.}, we feed our electronic structure calculations the atomic coordinates $x$ and $y$, and $f$ tells us if the system is magnetic.
In this case, we would have no choice but to use some sampling method, as we would not be able to write out an analytical integral to solve.
In this case, MC provides a more efficient means of \defn{unbiased sampling} than just making a grid of $x,y$ data points.
In the next section, we'll instead discuss how to efficiently perform \defn{biased sampling}.


\section{Markov-Chain Monte Carlo}

We can think of Markov Chain Monte Carlo (MCMC) as an alternative to performing molecular dynamics calculations---for example if we are interested in finding the average value of some property of a material or substance.
Instead of running a simulation for long times and taking an average, as we would do in molecular dynamics, we instead take random samples within an ensemble and average over them.
(The equivalence of these two techniques comes from the \emph{ergodic hypothesis}.)
MCMC is a family of techniques, the most well-known is the \emph{Metropolis algorithm}.


\subsection{The Metropolis algorithm}

In the basic scheme, we are looking to compute an average value of some property $F$; that is, we would like $\langle F \rangle$.
We will follow closely the original logic of Metropolis~\etal~\cite{Metropolis1953}.
From statistical mechanics, the average value of some property we are trying to predict can come from a Boltzmann-weighted integral, as in:

\[
\left\langle F \right \rangle = 
\frac{ \int F(\vec{x}) \, e^{-E(\vec{x})/\kb T} d^{3N} \vec{x} }
{ \int e^{-E(\vec{x})/\kb T} d^{3N} \vec{x} }
\]

\noindent here $\vec{x}$ is the vector of all atomic positions.
Note I have taken momentum ($\vec{p}$) out of this integral for simplicity; this is discussed in the original Metropolis paper but can normally be safely ignored.
(Roughly, this is due to the assumption that $F$ is only a function of position $\vec{x}$, and not momentum, and thus the momentum terms factor out equivalently from the numerator and denominator.)

For perhaps the simplest concrete example, consider a simulation with just two H atoms where we want to know the average bond length between the two atoms.
This thought experiment illustrates a rather important point.
Let's consider what would happen if we sampled in an unbiased manner.
We'd create a large box, perhaps 100~\AA\ $\times$ 100~\AA\ $\times$ 100~\AA, and for each data point we'd randomly place each hydrogen atom at a position in $(x,y,z)$ space.
The vast majority of samples would have the H's far apart, where they are not forming a bond whatsoever.
Whereas, we know from chemical intuition that the H's should spend most of their time reasonably close together, at around their optimal bondlength.
We could be much more efficient if we could weight the sampling in order to bias it towards low-energy configurations where there is a bond.

So, a na{\"i}ve approach to solving the integral above would put the particles in all possible positions $\vec{x}$, calculate their energy, and weight each sample with an appropriate Boltzmann term; however, even a reasonably course grid would be very expensive (``the curse of dimensionality'') and may miss some important points.
We could also think of populating our integrals by drawing randomly from a uniform distribution as in Monte Carlo Integration example above.
In principle this would work: we would multiply each point we sampled by its Boltzmann term (proportional to probability) to build the fraction.
However, we would waste a lot of time sampling improbable configurations that don't contribute much to the property; \textit{i.e.}, high energy configurations have a small Boltzmann term, and thus every high-energy configuration adds $\sim$0 to \textit{both} the numerator and denominator.

The insight of the Metropolis algorithm is that instead of sampling from a uniform distribution and multiplying by Boltzmann weights before summing, we could instead sample directly from the Boltzmann distribution and add together \textit{unweighted} terms.
This is mathematically equivalent, and intuitively this means we will preferentially sample the high-probability terms (low energy), which are the most important ones.
The Metropolis algorithm allows us to do this in the NVT (canonical) ensemble, and the more general \emph{Metropolis--Hastings} algorithm extends this to other ensembles.
Here, we will only focus on the Metropolis algorithm itself.


The general algorithm is:
{
\newcommand{\q}[1]{\ensuremath{\v{q}^{(#1)}}}

\begin{enumerate}
\item
Choose an initial configuration of atoms, \q{0}.
Calculate its energy, $E(\q{0})$, such as from an electronic structure or interatomic potential method.
The probability of the existence of this state is then \textit{proportional to} the Boltzmann factor

\[ p(\q{0}) \propto \exp \left\{ \frac{-E(\q{0})}{\kb T} \right\} \]

(The exact probability is this factor divided by the sum of the Boltzmann factor of \textit{all} configurations---that is, the partition function---which is a very difficult thing to calculate.)

Add this initial configuration to the \emph{ensemble}, or collection, of states.

\item
Propose a random-walk step; such a random walk is known as a \emph{Markov chain}.
That is, for each degree of freedom (coordinate) $i$ in $\v{q}$, move it according to

\[ q^{(j+1)*}_i = q^{(j)}_i + \Delta q_i^\mathrm{max} \cdot \xi_i \]

where $\xi_i$ is a randomly-drawn number, one per coordinate, from a uniform distribution between -1 and +1.
$\Delta q_i^\mathrm{max}$  is the maximum allowed step size for coordinate $i$.
The star ($*$) here just indicates the step has been proposed, and not yet added to the ensemble.
Again, calculate this configuration's energy, and note that its probability is proportional to the Boltzmann factor:

\[ p(\vec{x}^{(j+1)*}) \propto \exp \left\{ \frac{-E(\vec{x}^{(j+1)*})}{\kb T} \right\} \]

\item
The ratio of the probability of the proposed configuration to that of the previous configuration is 
					 
\[ r = \frac{ p(\q{(j+1)*})}{ p(\q{j})} =  \exp \left\{ \frac{-(E(\q{(j+1)*}) - E(\q{j}))}{\kb T} \right\} \]

If $r>1$, accept the step.
Otherwise, accept the proposed step with a probability $r$.
(That is, if the new step is lower in energy than the previous step, always accept it.
If not, draw another random number between 0 and 1, and if this random number is less than $r$ accept the proposed step.)
					 
If the proposed step is accepted, add it to the ensemble and take the next random-walk step from this point; \textit{i.e.}, return to step \#2.
Otherwise, take the next random-walk step from the previous point, ignoring the newly proposed point.

								
\end{enumerate}

At the end of the simulation, we have a Boltzmann-weighted \emph{ensemble} of configurations, and the average of any property value can be calculated as the average of the individual values in this ensemble.
Note that in order to assess the true probability of any individual configuration, we would have to calculate the denominator (that is, the partition function), which would require a knowledge of all points in space, and is extraordinarily expensive.
This method avoids that by working with only ratios of probability (in step \#3), such that the partition function never needs to be calculated.
Thus, anything that is proportional to probability will do.

\subsection{Aside: Bayesian statistics}

The random-walk steps are known as a \emph{Markov chain}, and the random draws are known as \emph{Monte Carlo}.
This algorithm is an example of a {Markov chain Monte Carlo} (MCMC) method.
These are seeing increased prominence in Bayesian statistics, for example.
In Bayesian statistics, if one were to fit a line ($y = mx + b$, with slope $m$ and intercept $b$) to a data set, you would not come up with a single estimate of $m$ and $b$, but an ensemble of estimates, and this ensemble is generated with an MCMC method as above.
With this ensemble, you would have a mean value of both $m$ and $b$, but you would also have distributions about each of them (\textit{e.g.}, a standard deviation about each estimate).
Importantly, each estimate is paired; that is, a specific entry in the ensemble is a value of the slope and intercept, which should be used together to make a specific prediction of the ensemble.
Then any estimate you make with your model; that is, given $x$, you would not have a single value for $y^\mathrm{[pred]}$ but an ensemble.
Thus, you have some degree of confidence built into every prediction.

\subsection{Proof that this leads to a Boltzmann distribution}

We will follow Metropolis \etal's logic.
Consider two states of our system, $u$ and $s$, which can be connected by a step on the Markov chain; $u$ and $s$ are unique atomic configurations.
Assume that $s$ is the lower energy configuration, $E_\mathrm{u} > E_\mathrm{s}$.
(u and s are relatively unstable and stable states.)
The probability of \emph{proposing} a step from $u$ to $s$ or vice versa is equal and is given by $P_{us} = P_{su}$.
Here we assume a discrete set of possible states (a lattice of possible positions) for simplicity, but note that this proof holds also in continuous space.
Suppose we have run our simulation for some time and we have an ensemble of states, where $N_u$ and $N_s$ are the number of ensemble elements in states $u$ and $s$, respectively.

Then the number of systems that would be expected to move from $u$ to $s$ is 

\[ u \rightarrow s: \hspace{2em} N_u P_{us} \cdot 1 \]

\noindent
because $s$ is the lower energy state, these steps are accepted with 100\%\ probability, which we have indicated by explicitly multiplying by one.
The number of steps that would be expected to move the other way is

\[s \rightarrow u: \hspace{2em} N_s P_{us} \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\}
\]

\noindent
Thus, the \textit{net} steps from $u$ to $s$ is

\[ \mathrm{net}(u \rightarrow s): \hspace{2em} N_u P_{us} - N_s P_{us} \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\} \]

\noindent
So, the condition in which we would expect more states to move from $u$ to $s$---that is, when the term above is positive---is given by:

\[  N_u  > N_s \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\} \]

\[  \frac{N_u}{N_s}  > \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\} \]

\noindent That is, if the number of states in the relatively unstable state $u$ relative to the number of states in $s$ exceeds the Boltzmann ratio, then we would expect a net conversion of $u$ to $s$, and vice versa.
Thus, this sampling pattern will tend toward the Boltzmann distribution.


}

\section{Kinetic Monte Carlo\label{section:kMC}}

A completely different MC methodology and purpose is kinetic Monte Carlo, abbreviated kMC.
Recall that we deal with the rare-event problem in describing reactions---we could in principle run molecular dynamics until we observed our reaction, but these calculations might take billions of years.
kMC speeds that up, while remaining atomistic in nature, by ``fast-forwarding'' through the non-reactive parts of the simulation, and only worrying about reactive events.
This is as opposed to microkinetic modeling (Section~\ref{section:microkinetic}), which speeds up the process by taking a ``mean-field'' (continuum) approach.

\begin{figure}
\centering
\includegraphics[width=4.0in]{f/kmc/schematic.pdf}
\caption{A basic schematic of the approach with kinetic Monte Carlo. \label{kmc-schematic}}
\end{figure}

The basic approach of kMC is shown in Figure~\ref{kmc-schematic}.
We attempt to simulate what would happen in a long-time molecular dynamics calculation by ``fast-forwarding'' through all the boring parts (which in atomistic simulations are essentially molecules vibrating and translating around.
At a specified time, an ``event'' happens, such as an elementary bond-breaking or -making event.
More time is fast forwarded through, until another ``event'' occurs.
This procedure is repeated until statistics are accrued about reaction rates and other events of interest.

In order to implement a procedure like in Figure~\ref{kmc-schematic}, we need to determine two factors:


\begin{enumerate}
\item What event happens next?
\item How long do I wait until that event happens?
\end{enumerate}


\paragraph{Illustrative example.}
Detailed discussions of kMC can be found in the literature~\cite{Lukkien1998,Hansen2000,Voter2005}.
Here, we illustrate this with a simple example, relating to reactions of \ce{H2S} and \ce{H2} on a catalyst surface (which we can consider as relating to sulfur poisoning and removal from a surface).
Let's pretend our catalyst is just one-dimensional and has only nine possible sites. 
The only possible reactions are the dissociative adsorptions of \ce{H2S} and \ce{H2}:

\cen{H2S + $**$ ->[k=5] H$**$SH \label{rxn:kmc1}}
\cen{H2 + $**$ ->[k=2] H$**$H \label{rxn:kmc2}}

\noindent
Here we have used $**$ to emphasize that two sites are necessary and that the sites must be adjacent; similarly nomenclature like \ce{A$**$B} means that A and B are bound to adjacent sites.
The rate constants of the two (forward) reactions are indicated and have been chosen arbitrarily for this example; in practice these would be calculated from methods such as transition-state theory or even measured experimentally.

The first step in kMC is to make a table of the possible reactions and their rates, which will be used to predict their probability.
Reaction~\eqref{rxn:kmc1} can occur by 16 means on these 9 sites; due to indistinguishability reaction~\eqref{rxn:kmc2} can occur by 8 means.
These possible events are summarized in Figure~\ref{kmc-event-table}.
The probability of any one of these individual events occurring next is proportional to its elementary rate constant, this is reflected in the bottom bar chart of Figure~\ref{kmc-event-table}, in which the \ce{H2S} dissociation events have width 5 and the \ce{H2} events have width 2.

\begin{figure}
\centering
\includegraphics[width=6.5in]{f/kmc/lattices/drawing.pdf}
\caption{The event table for the first step of our hypothetical reaction to be modeled in kMC. \label{kmc-event-table}}
\end{figure}

Therefore, to choose the next event, we just normalize the bar shown in the bottom of Figure~\ref{kmc-event-table} to be on a [0, 1] interval, and choose a random number in this interval.
For example, if we choose $u = 0.543$, then we have selected event 10, which corresponds to \ce{H2S} dissociating into H and SH on sites 2 and 3 of our lattice, respectively.
This defines the next event and answers the first question.

To decide how much time we ``skip'' before our event occurs, we start by recognizing that first-order reactions are equivalent to an exponential decay in survival probability.
That is if we observe the state at time $t'$ later than our initial time, the probability that it is destroyed is

\[ P_\mathrm{destroyed}(t') = 1 - e^{-k_\mathrm{tot} t'} \]

\noindent
Note that it is $k_\mathrm{tot} = \sum_i k_i$, the sum of all possible event rates, that enters this exponential decay, and not $k_{10}$, the rate of the actual single event that occurred.
(Intuitively, our time steps should be shorter if there are more possible reactions.)

To convert this to a probability density function in $t$; that is, to convert from the probability of being destroyed \emph{by} time $t'$ to the (differential) probability of being destroyed \emph{at} time $t'$, we can note that the former must be the integral of the latter; that is,

\[ P_\mathrm{destroyed}(t') = \int_0^{t'} p(t) dt \]

\noindent where $p(t)$ is the instantaneous probability of getting destroyed at time $t$.
Thus,

\[ p(t) = k_\mathrm{tot} e^{-k_\mathrm{tot} t} \]

\noindent
$p(t)$ is the ``exponential distribution function'', which is well-known across many stochastic processes, from earthquake modeling to radioactive decay to the frequency of customers entering a store.
A plot of this probability distribution is shown in Figure~\ref{fig:exponential}.
Note that $p(t)$ is a probability density function and has dimensions of inverse time; it is only when we integrate it over some finite time range that we get a true dimensionless probability of the event occurring in that time range.
This is shown with the shaded region in the figure, shown between times of 0.4 and 0.6 seconds; the shaded area would give the probability that the event occurs in the time range 0.4 s $< t <$ 0.6 s.
The probability at any exact time approaches zero; this is a property of probability density functions.


\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/kmc/exponential.pdf}
\caption{An exponential probability distribution drawn with $k_\mathrm{tot} = 0.5$ s$^{-1}$, from which the step time is drawn.
The green highlighted area shows an area from 0.4 to 0.6; if you integrated this area you'd get the probability that the time is between 0.4 and 0.6 seconds.
\label{fig:exponential}}
\end{figure}

With this, we can manipulate this equation to randomly sample an increment of time to skip in the simulation.

\[ \Delta t_\text{next event} = \frac{-1}{k_\mathrm{tot}} \ln u' \]

\noindent where $u'$ is a second random number.

For example, in our systems if we draw $u' = 0.963$ we get a time step of 0.000397 s (with our $k_\mathrm{tot}$ of 96 s$^{-1}$).
Thus, after the first step of the kMC algorithm we find ourselves at time 0.000397 s and with a configuration having H and SH on sites 2 and 3, respectively.

From here, we just repeat this procedure.
It's important to note that the event table is different at each step; for example, now that we have a H and SH on the surface, we have new reaction possibilities such as
\cen{H$**$SH -> H2S + $**$ \label{rxn:kmc3}}
\cen{H$**$ ->  $**$H \label{rxn:kmc4}}
\cen{HS$**$ -> H$**$S \label{rxn:kmc5}}

\noindent
\textit{etc.}
Reaction~\eqref{rxn:kmc3} is just the reverse of the reaction that occurred in the previous step.
Reaction~\eqref{rxn:kmc4} is surface diffusion of the hydrogen and Reaction~\eqref{rxn:kmc5} is dissociation of the HS group on the surface.
Of course, now that some of the surface sites are blocked some of the reactions we had listed in our previous event table are no longer possible.
Thus, we need to re-build the event table, and this is the most challenging part of the kMC procedure.

\paragraph{Steps.} In summary, the steps of the kMC procedure are:

\begin{enumerate}
		  \item Prepare the initial state (typically a lattice).
		  \item Generate an event table, which is a list of all possible events and their elementary rates $k_i$. The total rate is

					 \[ k_\mathrm{tot} = \sum_i k_i \]
		  \item Pick an event with a random number $u$ in (0, 1] and choose the event by $(u \times k_\mathrm{tot})$.
		  \item Pick a time step with a second random number $u'$ in (0, 1] with
\[ \Delta t_\text{next event} = \frac{-1}{k_\mathrm{tot}} \ln u' \]
\item Update the lattice state and simulation time with this information.
\item Repeat steps 2--5 \textit{ad nauseum}.
\end{enumerate}

\paragraph{Thermodynamics.}
Note that the kinetics are specified with an elementary rate constant $k_i$ for each step, but the relationship between forward and reverse rates of each step also dictate the thermodynamics.
That is, with the Eyring equation we can express forward and reverse rates as

\[ k_\mathrm{f} = \frac{\kb T}{h} e^{-\Delta G_\mathrm{f}^\ddag/\kb T} \]

\[ k_\mathrm{r} = \frac{\kb T}{h} e^{-\Delta G_\mathrm{r}^\ddag/\kb T} \]

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/dG-thermo-kinetics/drawing.pdf}
\caption{The relationship between kinetics and thermodynamics. \label{dG-thermo-kinetics}}
\end{figure}

\noindent
The ratio is thus:

\[ \frac{k_\mathrm{f}}{k_\mathrm{r}} = \frac{\kb T}{h} e^{-\left(\Delta G_\mathrm{f}^\ddag -\Delta G_\mathrm{r}^\ddag\right)/\kb T} = \frac{\kb T}{h} e^{-\Delta G_\mathrm{rxn}/\kb T} \]

\noindent where the second equality in the line above can be deduced from Figure~\ref{dG-thermo-kinetics}.
This well-known result links the forward and reverse rate constants to the reaction equilibrium, and thus over long times the simulations will tend toward equilibrium configurations if the rate constants are specified consistently with the thermodynamic relations above.

\paragraph{Coverage-dependent kinetics.}
Perhaps the most powerful aspect of kMC, as compared to a mean-field approach such as microkinetic modeling, is the ability to include non-mean-field effects such as coverage effects.
Consider the system shown in Figure~\ref{kmc-coverage}.
In both cases, a CO diffuses from site 0 to site 1.
However, CO's are known to repel each other on surfaces, so in the second case where a CO sits on a neighboring site the forward reaction is diminished and the reverse reaction is enhanced; these sorts of effects can be directly included within kMC models, which would be challenging to account for in microkinetic models.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/kmc/coverage/drawing.pdf}
\caption{Coverage effects can be built into kMC as shown above for diffusion of CO with and without a neighboring CO adsorbate. \label{kmc-coverage}}
\end{figure}

\endgroup
