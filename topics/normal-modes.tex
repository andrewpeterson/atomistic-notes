\begingroup
\chapter{Vibrations: normal-mode and phonon analyses}

\newcommand{\kb}{\ensuremath{k_\mathrm{B}}}
\newcommand{\pd}[3]{\left(\frac{\partial #1}{\partial #2}\right)_{#3}}

Recall that quantum mechanics states that energy levels are quantized, and that they start above the local minimum on the potential energy surface by a spacing called the zero-point energy.
Statistical mechanics states that for a population of particles the lower energy states will be exponentially more likely to be occupied, and gives the temperature dependence of this occupation.
In this lecture, we'll introduce how to find the energy levels for a harmonic system via a normal mode analysis.
This assumes that each degree of freedom can be described harmonically (that is, like a harmonic oscillator).

\section{Normal mode analysis (or molecular vibrational analysis) \label{section:nma}}
\begingroup
\newcommand{\naught}{^{(0)}}
\newcommand{\kt}[1]{\tilde{k}_{#1}}
\newcommand{\qt}[1]{\tilde{q}_{#1}}
\newcommand{\qtv}[1]{\ensuremath{\tilde{\underbar{q}}_{#1}}}

To find the energy levels, we often invoke the harmonic approximation, that all modes can be treated as harmonic oscillators.
(We will re-examine this approximation later.)
This analysis is very useful for molecular vibrations, protein vibrations, and in its periodic form, for phonons.

For a different take on this, I will recommmend the Colby College handout, which can be found on the web,\footnote{Currently available at \url{http://www.colby.edu/chemistry/PChem/notes/NormalModesText.pdf}}, or Chapter 12.6 of Br{\'a}zdov{\'a} and Bowler~\cite{Brazdova2013}.
We will derive this by a slightly different procedure in class.

Note that normal modes are resonant frequencies.
Each is centered about the minimum; that is, the mode always passes through its minimum.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/hookes-law/drawing.pdf}
\caption{A model of a single particle vibrating in a single dimension harmonically. $x^{(0)}$ is the equilibrium position of the system, $m$ is the mass of the object, and $k$ is the spring constant. \label{fig:hookes-law}}
\end{figure}

\subsection{One particle in one dimension\label{section:nma-1d}}

We'll start out with a one-dimensional example: you should think of a single bond vibrating, such as that in \ce{O2}.
However, we'll draw the system like in introductory physics, as in Figure~\ref{fig:hookes-law}.
This is essentially a mass on a spring attached to a wall, ignoring gravity and friction.
Recall Hooke's law states

\[ F = - k \cdot (x - x^{(0)}) \]

\noindent
where $x\naught$ is the position of the mass when there is zero force; that is, the resting state.
To simplify life, we'll define $q \equiv x - x\naught$, that is, the displacement from the equilibrium position, so that we can drop $x\naught$ from future math:

\[ F = - k q \]

Following the procedure we all learned in introductory physics, we write Newton's second law:

\[ F = m \frac{d^2 q}{dt^2} = - kq \]

\noindent
A solution to this differential equation is a simple wave equation:\footnote{This is, of course, a ``particular solution'' to this second-order differential equation. Properly there can be a phase-shift term in the wave equation for generality, but here we're just asserting the position is at a maximum at time zero.}

\[ q(t) = A \sin  \omega t \]

\noindent
We can verify that this is a solution by plugging in:

\[ - \omega^2 m A \sin  \omega t = -k A \sin  \omega t \]

\noindent
So we get $\boxed{\omega = \sqrt{k/m}}$, a well-known result in classical physics.
This is the characteristic frequency for this simple vibrating system, whose equation of motion is given by

\[ q(t) = A \sin \sqrt{\frac{k}{m}} t \]

\paragraph{Relation to energy.}
How generic is this solution?
Before talking through the meaning of this equation, let's look at the generality.
By our relationship between potential energy $E$ and force,

\[ F = - \frac{dE}{dq} \]

\[ \int dE = k \int q \, dq \]

\begin{equation}\label{eq:hookes-energy}
E = \frac{1}{2} k q^2 + \text{constant}
\end{equation}

\noindent
In other words, energy is a parabola centered around $q=0$, or $x=x\naught$.
There is an additive constant, since here we're deriving it from the force.

Note the assumption of harmonicity is often called the \underline{harmonic approximation}. This is equivalent to a second order Taylor series expansion about a local minimum.

\[ E = E_0 + \left. \frac{dE}{dq} \right|_{q=0} q + \frac{1}{2} \left. \frac{d^2 E}{dq^2} \right|_{q=0} q^2 + \text{H.O.T.} \]

\noindent
The first derivative is zero at the local minimum, so we are just left with the first and third terms:

\[ E \approx E_0 + \frac{1}{2} \left. \frac{d^2 E}{dq^2} \right|_{q=0} q^2 \]

\noindent
and therefore by comparison with equation~(\ref{eq:hookes-energy}) we can see that

\[ k = \frac{d^2 E}{dq^2} \]

\noindent
The harmonic approximation (Hooke's law) is equivalent to a second-order Taylor series expansion, and is often a reasonable approximation near the bottom of any potential energy well.

\paragraph{Thought experiment.}
Our equation of motion was:

\[ q(t) = A \sin \sqrt{\frac{k}{m}} t \]

\noindent
Note that this is an interesting result: let's do a thought experiment to explore it.
If we imagine pulling the mass away from its $q=0$ position, and release it, the system will start to oscillate.
The potential energy of the system is given by $\frac{1}{2} k q^2$, at any point $q$.
At any time, the total energy of the system will be conserved (in the frictionless case), and will just vary between how much of the energy is potential versus kinetic.
No matter how far we initially displace the mass, we'll find that the \emph{frequency} of the vibration is the same, although the amplitude will be different.

In the thought experiment where we move the mass to a new position $q'$, before releasing it from a static position, this means that we can choose the system to have any energy that we like, based on how far we displace.
(Of course, this is within certain physical limits, like not distorting the spring away from Hooke's law behavior.)
That is, we can continuously set the energy to any value we want, but we'll always get the same frequency of vibration.

\subsection{Energy levels, zero-point energy, and partition function\label{sxn:energy-levels}}

When we analyzed the system classically---that is, with Newton's laws of motion, as above---we found that the system could oscillate at any fixed energy level, but always with the same characteristic frequency.
However, in the quantum-mechanical limit, only discrete energy levels are allowed: in quantum mechanics, this system is referred to as the ``quantum harmonic oscillator'' and is one of the classic model systems of the field.
The allowed energy levels of the quantum harmonic oscillator with frequency $\omega$ are given by

\[ \varepsilon_n = \left(n + \frac{1}{2}\right) \hbar \omega, \hspace{2em} n = 0, 1, 2, 3, \cdots, \infty \]



It can be treated classically if $\kb T \gg \hbar \omega$, which means that the quantum mechanical energy spacings are very small compared to the thermal excitation of the system. In that case, we would effectively have a continuum of allowed states, which is the definition of the classical limit.
See Figure~\ref{fig:energy-levels-kT}.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{f/energy-levels-kT/drawing.pdf}
\caption{If the thermal energy is similar to (or less than) the energy spacing, then we are in the quantum-mechanical limit as shown in (a). If the thermal energy is much greater than the energy spacing, then we are in the classical limit (b). \label{fig:energy-levels-kT}}
\end{figure}


The lowest energy mode is given by plugging in $n = 0$,  which is the \emph{zero-point energy} for this system:

\[ E_\mathrm{ZPE} = \varepsilon_0 = \frac{1}{2} \hbar \omega \]

Recall that the partition function is given as the sum of the Boltzmann terms of each energy level.
Here, we reference all energies to the zero-point energy (removing the $\frac{1}{2} \hbar \omega$ offset); for a discussion of this convention see Cramer~\cite{Cramer2004}.
Note that the splitting off of the zero-point energy is only done for the vibrational degrees of freedom; by convention this is not done for the translational and rotational degrees of freedom.
This is why the zero-point energy term of equations~\eqref{eq:enthalpy-from-stat-mech} and \eqref{eq:zpve} only contains vibrational terms; this is purely a convention and not a fundamental, scientific difference.
The single-particle partition function is written as\footnote{This uses the identity $\sum_{n=0}^\infty x^n = \frac{1}{1-x}$, valid when $\left|x\right| < 1$.}

\[ q_\mathrm{vib} = \sum_{n=0}^\infty e^{-n \hbar \omega / \kb T} =
\frac{1}{1 - e^{-\hbar \omega / \kb T}} \]

\noindent
This can then be used as the basis for the derivation of the vibrational heat capacity and entropy presented in \secref{section:thermo-idealgas}.


\subsection{Multi-component, multi-mode \label{sxn:multi-component-nma}}

In a system of atoms, we don't know \textit{a priori} where the springs are or what the spring constants are, thus we will work out a system to assign them systematically; this is the normal-mode analysis.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/hookes-law/many-body.pdf}
\caption{Coordinate system for a many-body normal-mode analysis. \label{hookes-law-many-body}}
\end{figure}

We extend this to a many component system; \textit{e.g.}, the example system shown in Figure~\ref{hookes-law-many-body}.
Each particle of mass $m_i$ has displacements like $x_i - x_i\naught$, $y_i - y_i\naught$, and $z_i - z_i\naught$.
For simplicity, we'll just refer to all displacements in terms of $q_j$'s, which are equivalent to the displacements listed above (as shown in the figure).


We'll use our earlier ``discovery'' that the harmonic approximation is equivalent to a second-order Taylor series expansion, and write the TSE out in many dimensions:

\[ E \approx E_0 + \sum_{i=1}^{N} \left. \left(\frac{\partial E}{\partial q_i}  \right) \right|_{\vec{q}=0} q_i + \sum_{i,j} \frac{1}{2} \left. \left(\frac{\partial^2 E}{\partial q_i \partial q_j}  \right) \right|_{\vec{q}=0} q_i q_j \]

\noindent
Again, the first-order derivatives go to zero since we are expanding about a local minimum, and the third term looks like our spring terms from the one-dimensional example.
By analogy we define

\[ k_{i,j} \equiv \left. \left(\frac{\partial^2 E}{\partial q_i \partial q_j}  \right) \right|_{\vec{q}=0} \]

\noindent
where the subscript is shorthand to note that the derivative is evaluated about the local minimum.
Note that these terms make up the elements of the Hessian matrix, as discussed in \secref{sxn:optimization-methods}.

These terms can also be understood to be variations in forces, by

\[ k_{i,j} \equiv \left(\frac{\partial^2 E}{\partial q_i \partial q_j}  \right) = \left(\frac{\partial}{\partial q_i} \left( \frac{\partial E}{\partial q_j} \right)  \right) = - \left(\frac{\partial}{\partial q_i} F_j \right) \]


\noindent
That is, $k_{i,j}$ is the change in force component $j$ when atomic coordinate $i$ is varied.
Note that $k_{i,j} = k_{j,i}$.
Using this to simplify our Taylor series:

\[ E \approx E_0 + \sum_{i,j} \frac{1}{2} k_{i,j} q_i q_j \]

We'll again analyze this with Newton's second law, which we can write on any atom (in a single dimension) as

\[ F_i = m_i a_i \]

\noindent
Note one subtlety in indexing: the mass corresponds to the mass of the atom, generally the first three values of $i$ will all have the same mass (corresponding to the mass of the atom they are describing motions of), etc.

The force is just the derivative of the energy.
\textit{E.g.}, the force on component 1 (which might be the $x$ direction for atom 1) is expressed in this harmonic approximation as

\[ F_i = - \pd{E}{q_i}{\{q_{j \ne i}\}} \approx - \sum_j k_{i,j} q_j \]

\noindent
Plugging into Newton's second law,

\[ m_i \frac{d^2 q_i}{dt^2} = - \sum_j k_{i,j} q_j \]

\noindent
This is often written in \emph{reduced-mass} form to make the subsequent eigenvalue--eigenvector problem more straightforward, defining new terms (denoted with tildes):

\[ \qt{i} \equiv q_{i} \sqrt{m_i}, \,\,\,
\kt{i,j} \equiv \frac{k_{i,j}}{\sqrt{m_i} \sqrt{m_j}}
\]

\[ \frac{d^2 \qt{i}}{dt^2} = - \sum_j \kt{i,j} \qt{j} \]

\noindent
We can guess\footnote{Note that the magnitude $A_j$ has a subscript, but the frequency $\omega$ does not.  If we had guessed a solution with $\omega_j$, that is, a per-atom frequency, we would find it does not solve our differential equation. Therefore, this means that the frequency will be for the system as a whole; this will have the added effect of enforcing the motions all move through the local minimum.} a solution of the form:

\[ \qt{j} =  A_j \sin \omega t \] 

\noindent
We can plug this into our differential equation to see that it works:

\[ - \omega^2  A_i \sin \omega t = - \sum_j \kt{i, j} A_j \sin \omega t \]

\[ - \omega^2  A_i \sin \omega t = - \sin \omega t \sum_j \kt{i, j} A_j  \]

\noindent
To satisfy the differential equation, the time dependent term must cancel out, and we see it does.
Note that $\sin \omega t$ only cancels out because it has no subscript: that is, there is a single frequency which applies to every term in the sum.
This leaves us with an algebraic relationship between the unknown parameters of the equation of motion:

\[ \boxed{ \omega^2  A_i  = \sum_j \kt{i, j} A_j} \]

\noindent
Note the above is one of a system of coupled equations, which need to be solved simultaneously.
Let's look at an example, for 3 degrees of freedom.
(Properly, the example in the figure had $3N=9$ degrees of freedom, but we can understand the procedure with a simpler system.)

\[ \omega^2  A_1  =
  \kt{1, 1} A_1
+ \kt{1, 2} A_2
+ \kt{1, 3} A_3
\]

\[ \omega^2  A_2  =
  \kt{2, 1} A_1
+ \kt{2, 2} A_2
+ \kt{2, 3} A_3
\]

\[ \omega^2  A_3  =
  \kt{3, 1} A_1
+ \kt{3, 2} A_2
+ \kt{3, 3} A_3
\]

\noindent
Combining terms, we can re-write these three equations as:

\[ 0 = 
( \kt{1, 1} - \omega^2) A_1
+ \kt{1, 2} A_2
+ \kt{1, 3} A_3
\]

\[ 0 = 
\kt{2, 1} A_1
+ (\kt{2, 2} - \omega^2) A_2
+ \kt{2, 3} A_3
\]

\[ 0 = 
\kt{3, 1}  A_1
+ \kt{3, 2} A_2
+ (\kt{3, 3} - \omega^2) A_3
\]

\noindent
Now we'll write in matrix form:

\[ \brm{
\kt{1,1} - \omega^2 & \kt{1,2} & \kt{1,3} \\
\kt{2,1} &  \kt{2,2} - \omega^2  & \kt{2,3} \\
\kt{3,1} & \kt{3,2} & \kt{3,3} - \omega^2  \\
}
\brm{ A_1 \\ A_2 \\ A_3 }
=
\brm{ 0 \\ 0 \\ 0 }
\]

Note that this is an eigenvalue--eigenvector problem such where our matrix \m{M}\ can be written as $\m{M} = \m{\tilde{H}} - \omega^2 \m{I}$ (where $\omega^2$ is an eigenvalue, $\mathbf{I}$ is the identity matrix, and \m{\tilde{H}} is the Hessian matrix in reduced-mass form).
We thus have:

\[ \left( \m{\tilde{H}} - \omega^2 \m{I} \right) \v{a} = \v{0} \]

\noindent
where $\v{a} = \brm{A_1 & A_2 & A_3}\T$, and we can re-write this in standard eigenvalue-problem form:

\[ \m{\tilde{H}} \, \v{a} = \omega^2 \v{a} \]

\noindent
This can be solved by a standard diagonalization procedure.

The number of eigenvalue ($\omega^2$) / eigenvector (\v{a}) pairs is equivalent to the number of degrees of freedom of the system---that is, the number of rows in the matrix.
Each eigenvalue corresponds to the frequency squared ($\omega_l^2$) of the mode $l$, which in turn can be related to the energy levels of that mode by $\varepsilon_l = \hbar \omega_l$.
The associated eigenvector is the mode of that vibrational frequency, describing the relative motion of each of the degrees of freedom of the system.
These are called the \underline{normal modes}, and make up a complete, orthogonal basis set to describe any motion.

Put together, the equation of motion for any one vibrational mode $l$ will be:

\[ \boxed{\qtv{l} = \underbar{A}_l \sin \omega_l t} \]

\noindent
where I'm using the underbar to indicate a vector---that is \qtv{l}\ is a vector containing each of the atom's movements; it is typically of length $3N$, where $N$ is the number of atoms in the system.
Therefore, these vectors contain one element for the $x$, $y$, and $z$ direction of motion for each atom.
The subscript $l$ means the $l$th vibrational mode; there are up to $3N$ vibrational modes for the system (with fewer when translations and rotations are removed, as discussed elsewhere).
In practice, you'll notice that the lowest 6 (5 for linear) vibrational frequencies actually correspond to translations and rotations---that is, this procedure separates out the translations and rotations.
It is also possible to manually project out these modes before doing a normal-mode analysis.

\subsection{Computational approach\label{sxn:computational-nma}}

In practice, to solve this problem all we need to do is to construct the reduced-mass Hessian, denoted here as \m{\tilde{H}}.
We then diagonalize it to find the eigenvector/eigenvalue pairs, which are the normal modes and their associated frequencies.
The elements of \m{\tilde{H}} are just the reduced-mass spring constants,


\[
\kt{i,j} \equiv \frac{k_{i,j}}{\sqrt{m_i} \sqrt{m_j}}
\]

\noindent
where

\[
k_{i,j} = - \pd{F_j}{q_i}{q_{\{k \ne i\}}}
\approx
\frac{F_j\Big|_{q_i=-\Delta q}
- F_j\Big|_{q_i=+\Delta q}
}
{2 \Delta q}
\]

The equation above gives the recipe for constructing the (reduced-mass) Hessian computationally.\footnote{As noted in \secref{sxn:optimization-methods}, some codes can produce the Hessian directly from electronic structure calculations.}
The routines just displace each atom in the $\pm x$, $\pm y$, and $\pm z$ directions and examine the forces induced on each other atom in the system, for each displacement.
This is used in the above equation to estimate $k_{i,j}$, and ultimately construct \m{\tilde{H}}, which is then diagonalized with a standard eigenvalue solver.

Performing a normal-mode vibrational analysis in ASE is automated and an example script is in Section~\ref{code:normalmodeanalysis}.

\subsubsection{Energy levels}

The quantized energy levels and zero-point energy are treated just like the one-dimensional example in \secref{sxn:energy-levels}, but for each normal mode independently.


\endgroup % end NMA comands

\subsection{Normal mode comparison}

\begin{figure}
\centering
\includegraphics[width=0.7 \textwidth]{f/normal-mode-comparison/drawing.pdf}
\caption{Qualitative comparison of a narrow and wide mode about a local minimum.\label{normal-mode-comparison}
}
\end{figure}


The homework problem on calculating free energies ``by hand'' is intended to give you some intuition by working through the numbers on a few cases, even though you can now quickly do these sorts of calculations in an automated fashion by using the thermochemistry module in ASE (or similar).
But it is useful to work through examples to develop intuition.
Qualitatively, let's examine narrow versus wide vibrational modes; see Figure~\ref{normal-mode-comparison}.
We can draw some qualitative conclusions:
 
\vspace{1em}

\begin{tabular}{|l|l|}
		  \hline
		  Narrow mode & Wide mode \\
		  \hline
		  High frequency & Low frequency \\
		  Higher ZPE & Lower ZPE \\
		  Lower $S$ & Higher $S$ \\
		  \hline
\end{tabular}

\vspace{1em}

\noindent
Note also that 

\[ G \equiv H - TS \approx E_\mathrm{pot} + \underbrace{E_\mathrm{ZPE}}_{\text{narrow}} + \int_0^T C_P dT - T \underbrace{S}_{\text{wide}} \]

\noindent
That is, the narrow mode will be a large term in the zero-point energy (while the wide will contribute relatively little), whereas the wide term will be dominant in the entropy (and the narrow will contribute to a lesser extent).

\section{Phonon analysis}

The vibrations inside of solids, in particular periodic crystals, are known as \defn{phonons}.
Phonons, as the name implies are responsible for the propagation of sound throughout a solid.
They also dictate the heat capacity, and therefore the thermodynamics of the solid.
In some materials, phonons (called optical phonons) can influence optical properties by inducing changes in internal dipole moments of the material.

Phonons are treated in depth and rigor in most solid-state physics courses; here, we'll go over only the most basic textbook example of a phonon, as well as how they are calculated in practice in a code like ASE.

\subsection{The classic phonon example problem}


\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{f/phonon/drawing.pdf}
\caption{Model system for a simple phonon analysis.
\label{fig:phonon}}
\end{figure}

Here we'll examine the classic textbook example of phonon behavior.
Note that in practice, this is not how phonons are calculated in atomistic programs like ASE; they are treated with Hessian matrices and eigenvalue problems, similar to normal modes.
However, this simple example gives us a working knowledge of how phonons behave in a solid.

We'll consider how vibrations travel through a one-dimensional chain of $N$ identical, identically spaced atoms, as shown in Figure~\ref{fig:phonon}.
Each atom has mass $m$ and has separation $a$ form its neighbors.
Each atom is linked to its neighbors by a spring with spring constant\footnote{We're going to have to use the variable name $k$ for the wave number in a bit; this is so standard that we'll have to change the Hooke's Law constant variable name from $k$ to $c$. Everbody loves $k$!} $c$ and Atom $n$'s equilibrium position is denoted as $q_n$.
To treat Atoms 1 and $N$ identical to all internal atoms, we will impose periodic boundary conditions; this means that we put another spring between Atoms 1 and $N$.
In this 1-d example, you can think of the atoms as arranged in a ring, as if they are wrapped around a coffee can.
In practice, this means that Atoms $N + n$ and Atom $N$ are identical, as is Atom $2N +n$, $\cdots$.

The force on Atom $n$ is due to the springs to its nearest neighbors, and will be:

\[
F_n = c \, (q_{n+1} - q_n) - c \, (q_n - q_{n-1})
\]

\noindent
and Newton's second law applied to this atom is thus

\[
\eqlabel{eq:phonon-ode}
m \frac{d^2q_n}{dt^2} = c \, (q_{n+1} - q_n) - c \, (q_n - q_{n-1})
\]

Similar to our previous examples, we can propose a solution to this differential equation and see if it works.
In this case, we can propose:

\[ \eqlabel{eq:qn} q_n = A e^{i \omega t + i k n a} \]

\noindent
If you haven't worked with complex exponential functions before, this is probably counterintuitive.
But recall Euler's formula ($e^{ix} = \cos x + i \sin x \equiv \mathrm{cis} \, x$), so this represents a periodic function.
So if you compare the argument of this function to a sinusoidal function, and realize that $na$ has dimensions of length, this is similar to $\cos (\omega t + k x)$, which is the equation of a traveling wave.
In this case, $\omega$ is the (temporal) frequency, and $k$ is the wave number, or spatial frequency, of the wave.
Although it's certainly more conceptually challenging to think of a wave as a complex exponential function, it makes the mathematical manipulations a lot easier as we don't have to remember all our trigonometric identities.
Using the exponential form, it's easy to decompose this function into:

\[ \eqlabel{eq:qn-split} q_n = A e^{i \omega t} e^{i k n a} \]

\noindent
which would be harder to do by remembering trigonometric identities.
This will come in handy in the subsequent manipulations.

We need to see what happens when we plug this in to our differential equation.
We'll examine the left-hand side (LHS) and right-hand side (RHS) of equation~\eqref{eq:phonon-ode} individually.

\[ \mathrm{LHS} =
- A \omega^2 m e^{i \omega t} e^{ikna}
\]

\[ \mathrm{RHS} =
c \, \left(q_{n-1} - 2 q_n + q_{n+1} \right)
\]


\[
= c \left(
A e^{i \omega t} e^{i k (n-1) a}
-2 A e^{i \omega t} e^{i k n a}
+ A e^{i \omega t} e^{i k (n+1) a}
\right)
\]

\[
= A c e^{i \omega t} \left(
e^{i k n a} e^{- i k a}
- 2 e^{i k n a}
+ e^{i k n a} e^{+ i k a}
\right)
\]

\[
= A c e^{i \omega t}
e^{i k n a}
\left(
e^{- i k a}
- 2
+ e^{+ i k a}
\right)
\]

\[
\mathrm{LHS} = \mathrm{RHS}
\]

\[ - \omega^2 m
= c \left(
- 2
+ e^{- i k a}
+ e^{+ i k a}
\right)
\]

\noindent
See how easy that was with complex exponentials!
We can now substitute back in the trigonometric functions, via Euler's formula: $e^{+ix} + e^{-ix} = \cos x + i \sin x + \cos (-x) + i \sin (-x) = \cos x + i \sin x + \cos x - i \sin x = 2 \cos x $:

\[ - \omega^2 m = c \left( - 2 + 2 \cos ka \right) \]

\noindent
And now we can finally use a trigonometric identity to simplify, the double-angle identity ($- 2 + 2 \cos 2x = - 4 \sin^2 x$):

\[  \omega^2 m =  4c \sin^2 ka \]

\[ \eqlabel{eq:omega-from-k}
\boxed{ \omega = \pm 2 \sqrt{\frac{c}{m}} \, \left| \sin \frac{ka}{2} \right| } \]

\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{f/phonon/out.pdf}
\caption{Relationship between phonon frequency ($\omega$) and $ka$.
\label{fig:phonon-omega}}
\end{figure}

However, we don't really know what is allowed to be put in for $k$ in the above equation.
First, recall that we should have exactly $N$ frequencies for this one-dimensional $N$-atom problem\footnote{If it were a proper 3-dimensional problem, we would expect $3N$ frequencies, corresponding to the degrees of freedom of our system.}.
We will find the missing relationship by applying our boundary conditions, which in this case were periodic.
That is, $q_{N+n} = q_n$.
Using our form for $q$ from equation~\eqref{eq:qn-split},

\[
A e^{i \omega t} e^{ik(N+n)a} 
= A e^{i \omega t} e^{ikna} 
\]

\[
e^{ikNa} 
e^{ikna} 
= e^{ikna} 
\]

\[
e^{ikNa} 
= 1
\]

Let's recall that $e^{ikNa} = \cos kNa + i \sin kNa$.
This will equal exactly 1 (with no imaginary component) when $kNa$ is equal to $0, 2 \pi, 4 \pi, \cdots$, so we can write this out formally as

\[
kNa = 2 \pi j, \,\, j = 1, 2, \cdots, N
\]

\[ \eqlabel{eq:k-from-j}
k = \frac{2 \pi j}{Na}, \,\, j = 1, 2, \cdots, N
\]

Note that having the series go past $N$ gives redundant terms, and we could equivalently start at any integer as long as we have exactly $N$ terms, such as $j = 0, 1, \cdots, N-1$.
With this, the equation of motion for any atom $n$ is

\[ \eqlabel{eq:qn-with-j}
q_{n,j}
= A e^{i 2 \pi j n / N } e^{i \omega_j t}
= A \, \mathrm{cis} \, \left( 2 \pi j \frac{n}{N} + \omega_j t \right)
\]

Where we've added the subscript $j$ to $\omega$ to make it clear that there is one value of the frequency $\omega$ for each $j$ index; these can be linked from equations~\eqref{eq:k-from-j} and \eqref{eq:omega-from-k}.
This subscript also appears on $q$ to indicate this is the equation of motion for atom $n$ associated with phonon frequency $\omega_j$.


For our purposes, think of $\mathrm{cis}$ as a sinusoidal trigometric function like $\sin$ or $\cos$.
Let's take an example system of a chain of $N=10$ atoms, and let's arbitrarily examine atoms $n=3$ and $n=5$ along this chain.
Equation~\eqref{eq:qn-with-j} for these two atoms is:

\[
q_{3,j}
= A \, \mathrm{cis} \, \left( 2 \pi j (0.3) + \omega_j t \right)
\]

\[
q_{5,j}
= A \, \mathrm{cis} \, \left( 2 \pi j (0.5) + \omega_j t \right)
\]

Recall this is just an equation of a traveling wave\footnote{In complex exponential form, but ignore that!}, so those two equations tell us that the atoms move with identical frequency $\omega_j$ but with a relative offset---that is, the peaks and troughs of the waves hit each atom at a different time, although the frequency is constant.
This will be true for any value of $j$, except for when $j=N=10$: then this factor becomes $2 \pi 3$ and $2 \pi 5$, and since both are integer values of $2 \pi$, then the atoms are in sync.

In practice, in real materials $N$ is very large---think $10^{23}$ atoms, so we generally do think of this in the continuum limit, and can make plots of $\omega$ versus $k$, as in Figure~\ref{fig:phonon-omega}.

\subsection{Phonon analysis in practice}

In practice, phonon analysis in atomistic codes takes place by a similar procedure to what we went over for normal-modes in molecules in \secref{sxn:multi-component-nma} and \secref{sxn:computational-nma}.
In broad strokes: a force constant matrix is constructed by displacement, often taking advantage of symmetries, and this is diagonalized to find the frequencies.
A concise summary of the detailed mathematics appears in Alf{\`e}~\cite{Alfe2009}.
Tools to convert the phonon analysis output into thermodynamic data appears, along with good documentation, in ASE's thermochemistry module.\footnote{This module and its documentation were both created by a Brown undergraduate who took this course!}

\section{Script(s)}

\subsection{Normal mode analysis\label{code:normalmodeanalysis}}

File attached here.  \attachfile{f/scripts/normalmodeanalysis.py}

Note: The \ce{H2} molecule has $3N-5 = 1$ vibrational degrees of freedom, but the script below will give you a list of $3N$ vibrations.
Therefore, you should discard the lowest five modes as only the highest-frequency mode is a true vibration.
You can see this by opening the modes in ase gui.
\pythoncode{f/scripts/normalmodeanalysis.py}


\endgroup
