\begingroup
\chapter{Bonding in metals}

\newcommand{\ket}[1]{\left| #1 \right\rangle}
\newcommand{\bra}[1]{\left\langle #1 \right|}
\newcommand{\op}[1]{\hat{\mathrm{#1}}} % operator
\newcommand{\ebra}{\ensuremath{\bra{\phantom{X}}}}
\newcommand{\eket}{\ket{\phantom{X}}}
\newcommand{\braket}[2]{\left\langle #1 \middle| #2 \right \rangle}
\newcommand{\sandwich}[3]{\left\langle #1 \middle| #2 \middle| #3 \right \rangle}
\newcommand{\ebraket}{\braket{\phantom{X}}{\phantom{X}}}
\setlength{\fboxsep}{0pt}
\newcommand{\eop}{\hat{\fbox{\phantom{X}}}}
\newcommand{\ve}{\varepsilon}
\newcommand{\kb}{\ensuremath{k_\mathrm{B}}}


Metals tend to form dense structures where each atom has many nearest neighbors.
This situation leads to lots of orbital overlap, and ultimately a band structure for the electrons.
We give a brief overview of how band structures form and examine two limiting models of how bonding works in metals: jellium (completely delocalized) and tight-binding (highly localized).
For a more thorough description, see Chorkendorff and Niemantsverdriet~\cite{Chorkendorff2005}.

\section{Density of states and the Fermi level}

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/crystal/Au4000.png}
\caption{The dense packing of metals leads to rampant overlap in their outer orbitals. \label{crystal-dense}}
\end{figure}

Metals form dense crystals (or even amorphous structures), in which each atom has many nearest neighbors, as shown in Figure~\ref{crystal-dense} for a Au fcc crystal.
The dense packing leads to strong overlap in the outer orbitals, and thus rampant orbital hybridization.
This is depicted in Figure~\ref{Chorkendorff_2005_6-9}.
We see that eventually the energy levels become so numerous that they effectively form a continuum of states so close to one another that they cannot be distinguished; this is the well-known band structure of metals.

\begin{figure}
\centering
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[width=3.0in]{f/Chorkendorff_2005_6-9.png}
\caption{
The large amount of overlap in electronic orbitals eventually leads to a continuum of electronic energy levels, as depicted by Chorkendorff and Niemantsverdriet~\cite{Chorkendorff2005}.
\label{Chorkendorff_2005_6-9}}
\end{minipage}\hfill
\begin{minipage}{0.45\textwidth}
\centering
\includegraphics[width=3.0in]{f/dos-from-states/drawing.pdf}
\caption{
A density of states emerges from the discrete levels, showing the relative density of available states at each energy level.
\label{dos-from-states}}
\end{minipage}
\end{figure}

However, the energy levels are not all evenly spaced, but have structure to them as is shown in Figure~\ref{dos-from-states}.
We typically think of the energy levels in a distribution, known as the \emph{density of states}, that indicates at each energy level the relative number of states.
This density of states is a probability density function that is conventionally plotted sideways; that is, with probability density on the abscissa and energy on the ordinate, to match how energy levels are typically plotted vertically.
As a continuous distribution function, the number of sites given between energy levels $\varepsilon_\mathrm{A}$ and $\ve_\mathrm{B}$ is given by integrating between the two points, that is $\int_{\ve_\mathrm{A}}^{\ve_\mathrm{B}} \mathrm{DOS}(\ve) d \ve$, where $\mathrm{DOS}$ is the density of states.
However, DOS is typically given in arbitrary units, where only the relative proportions matter.
Example density of states plots for the late transition metals are shown in Figure~\ref{Peterson2012a-Fig2}.

While the density of states gives the allowable energy levels, not all energy levels will be occupied with electrons.
Naturally, the lowest energy levels fill first, and this continues until all electrons are accounted for; the level of the highest electron is called the \defn{Fermi level} or the Fermi energy.
More formally, the Fermi level is the energy required to add or subtract an incremental electron from the system, and as such is \emph{equivalent to the chemical potential of the electrons}.
At 0~K, all states below the Fermi level are occupied while all states above the Fermi level are empty.
At finite temperatures, some of the electrons are thermally excited, leading to some partially filled states above the Fermi level and some incompletely filled states below the Fermi level.
This distribution is described by Fermi--Dirac statistics, where the filling (referred to as occupation) at energy level $\ve$ is given by

\[ \frac{1}{1 + e^{(\ve - \mu)/\kb T}} \]

\noindent
where $\mu$ indicates the Fermi level, \kb\ is the Boltzmann constant, and $T$ is the temperature.
We won't derive this here; but you can find it derived in most statistical mechanics and solid-state physics textbooks, as well as from three different frameworks on wikipedia.
An example of this ditribution at different temperatures is shown in Figure~\ref{fig:fermi-dirac}.
At 0~K, all of the states below the Fermi level are filled, and all of the states below ther Fermi level are empty.
At finite temperatures, some electrons are excited above the Fermi level.
An equal number of electrons are missing from the area below the Fermi level as are present above the Fermi level.

\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{f/fermi-dirac/out.pdf}
\caption{
Fermi--Dirac distribution at different temperatures.
The left  plots show occupations (fractional, between 0 and 1).
The right plots show the filling of an arbitrarily shaped density of states at these temperatures; in this case the gray area is proportional the number of electrons at each energy level.
\label{fig:fermi-dirac}}
\end{figure}

As a physical analog, you can think of the density of states as a strangely-shaped bucket, and the electrons as water that is poured into the bucket.
The amount of water is fixed (by the stoichiometry of the system), and thus it fills the bucket to a level called the Fermi level, marked when the bucket and water are perfectly still.
The temperature of the system is analogous to kinetic energy, so we can consider this as vibrating or sloshing the bucket.
When this occurs, some of the water will splash up, preferentially occupying the lowest energy parts of the unfilled region of the bucket, and splashing higher and higher as it is vibrated a higher temperature.
Simultaneously, if some water is splashed up, a bit of water must be missing from the region near the surface.
Therefore, the states just below the Fermi level are not quite filled, and the states just above the Fermi level are not quite empty.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{f/Peterson2012a-Fig2.pdf}
\caption{Example density of states plots. These particular densities of states are for the late transition metals and are the projection of the indicated d orbitals on a surface atom of an fcc (111) surface. Note that the blue indicates filled regions, and that the energies are plotted with respect to the Fermi level. From reference~\cite{Peterson2012a}. \label{Peterson2012a-Fig2}}
\end{figure}

With these definitions of density of states and Fermi level, we now look at two idealized models: jellium and the tight-binding model.

\section{Jellium, or the free-electron gas}

The valence s and p electrons tend to be highly delocalized in metals; a classic example is aluminum, which has no valence d electrons.
These electrons are smeared out in space, and the model that describes this is appropriately called \defn{jellium} or the \defn{free-electron gas}.

To model this idealized system, the positive charges are smeared out over space; that is, rather than existing as point charges at the positions of the nuclei they are a homogeneous background charge---a positive charge that exists to compensate the negative charges of the electrons.
The term ``jellium'' actually refers to this smeared-out positive charge, and was coined in the pejorative sense\footnote{Jellium properly refers to a smeared-out positive charge that stabilizes the free-electron gas. It was originally introduced as a somewhat derogatory term for the concept. A real metal, like sodium or lithium or aluminum, has discrete points of positive charge. In this model, they are smeared out like jelly. Hence, this element is jellium.} as a criticism of the unphysical nature of this model~\cite{Ewald1953}.
This defines the potential energy operator in the Hamiltonian to be featureless, and if we use a particle-in-a-box solution in three dimensions~\cite{Chorkendorff2005}, we get energy levels described by

\[
\ve_{n_x,n_y,n_z}
= \frac{\hbar^2}{2m}\left(\frac{\pi}{L}\right)^2 \left( n_x^2 + n_y^2 + n_z^2 \right); \,\,\,
n_x = 1, 2, \cdots; \,
n_y = 1, 2, \cdots; \, 
n_z = 1, 2, \cdots
\]

\noindent
where $m$ is the electronic mass and $L$ is a characteristic length, and $n_x$, $n_y$, and $n_z$ are the quantum numbers in the three dimensions.
Each of the quantum numbers starts at 1, and continues unbounded in integer increments.
These quantum numbers are often collapsed into a vector $\vec{k} \equiv \frac{\pi}{L} [n_x, n_y, n_z]$,

\[ \eqlabel{eq:jellium-energy-levels}
\ve_k
= \frac{\hbar^2}{2 m} \lVert \vec{k} \rVert_2^2
\]

\noindent
(The operator $\lVert \hspace{1em} \rVert_2$ refers to the Euclidean ($\ell^2$) norm; this is just the magnitude of the vector so the $\ell^2$-norm of $\vec{k}$ is $\lVert \vec{k} \rVert_2 = \sqrt{n_x^2 + n_y^2 + n_z^2}$.)


\begin{table}
\centering
\caption{Tabulated energy levels for jellium, from equation~\eqref{eq:jellium-energy-levels}.
\label{table:jellium-energy-levels}}
\vspace{0.5em}
\begin{tabular}{rrrr}
\hline \hline
$n_x$ & $n_y$ & $n_z$ & $\ve_k \, / \left(\frac{\hbar^2 \pi^2}{2mL^2}\right)   $ \\
\hline
1 & 1 & 1 & 3 \\
2 & 1 & 1 & 6 \\
1 & 2 & 1 & 6 \\
1 & 1 & 2 & 6 \\
2 & 2 & 1 & 9 \\
$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
\hline \hline
\end{tabular}
\end{table}

Let's try to build the density of states for this model.
We can start tabulating the quantum numbers to find the allowable energy levels, as shown in Table~\ref{table:jellium-energy-levels}.
The pattern may not yet be apparent for some time, but what we'll find is that there are more states available as the energy increases.
However, it's not obvious how to spot any pattern.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/2-d-quantum-numbers/drawing.pdf}
\caption{Accounting for quantum number density in 2-dimensional space. \label{two-d-quantum-numbers}}
\end{figure}

To better understand the pattern, we will look at a case that is simpler to draw, with only $n_x$ and $n_y$, then extrapolate our two-dimensional results to the three-dimensional case that also contains $n_z$.
Figure~\ref{two-d-quantum-numbers} shows the position of the $n_x$ and $n_y$ points on the two-dimensional (positive) integer lattice.
This grid layout is essentially like graph paper: each grid point occupies a square that is $1\times1$.
Therefore, the number of points in an area is approximately equal to the area itself; this approximation becomes more and more exact as the area gets large.
(And since the number of electrons in a real system is something like 10$^{23}$, this is an excellent approximation.)

So, let's draw a circle centered on the origin; a small circle will capture points with the smallest quantum numbers, and larger circles will capture points with higher quantum numbers.
The number of energy values inside our circle will just be the circle's area divided by four, since only the positive--positive quadrant is occupied.
Thus, the number of states less than the vector with magnitude $r$ is $N = \pi r^2 / 4$.
In three-dimensional space, the analogous equation is $N = (4/3) \pi r^3 / 8 = \pi r^3 /6$
A vector $r$ corresponds to an energy from equation~\eqref{eq:jellium-energy-levels} of $\varepsilon = \frac{\hbar^2 \pi^2}{2 m L^2} r^2$, and the density of states can be found as:

%\[ \mathrm{DOS}(\ve) = \frac{dN}{d\ve} = \frac{\sqrt{2}}{2} \frac{ m^{3/2} L^3}{\pi \hbar^3} \sqrt{\ve} \propto \sqrt{\ve} 
%\]

\[ \mathrm{DOS}(\ve) \equiv \frac{dN}{d\ve}
= \frac{dN}{dr} \frac{dr}{d \ve}
= \left(\frac{1}{2} \pi r^2 \right)
\left( \frac{\hbar^2 \pi^2}{m L^2} r \right)^{-1}
\propto r \propto \sqrt{\ve}
\]


\noindent
Thus, this predicts a broad band of energy states whose availability increases slowly, with $\sqrt{\ve}$, as the energy level increases.
This is a reasonable approximation for the free-electron metals such as aluminum; Figure~\ref{Waldecker2016-Fig7} shows a comparison of density of states calculated for aluminum with density functional theory as compared to the $\sqrt{\ve}$ model; we see very good agreement.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/Waldecker2016-Fig7.png}
\caption{Density of states for Al calculated with DFT compared to $\sqrt{\ve}$. From reference~\cite{Waldecker2016}. (Image under Creative Commons license.) \label{Waldecker2016-Fig7}}
\end{figure}

The jellium model gives other qualitative predictions, such as the presence of a surface dipole where electrons overlap the boundary of the homogeneous background charge, and emission of electrons from a filament.
For a discussion, see textbooks such as Chorkendorff and Niemantsverdriet~\cite{Chorkendorff2005}.


\section{Tight-binding model}

The other limiting model is known as the tight-binding model.
As the name implies, in this model the electrons are much more localized around individual nuclei---that is, they stay in individual orbitals for long periods of time and occasionally ``leak'' to the next ion.

The mathematics below lead to the conclusion that the energy in the density of states is much more localized than in the sp band, and ultimately forms a shape more like a semicircle.

To derive this, we consider a crystal in one dimension.
That is, we have a lattice of atoms on a line with a spacing of $a$ between atoms; therefore, atom $j$ is at a position of $R_j = ja$.
Considering only the local field, the potential about a single nucleus is

\[ V_j = \frac{n_\mathrm{nucleus} e^2}{4 \pi \ve_0 \left| x - R_j \right| } \]

\noindent and under the assumption that the potential fields are additive, that of the crystal field is just:

\[ V(x) = \sum_{j=1}^N V_j = \sum_{j=1}^N \frac{n_\mathrm{nucleus} e^2}{4 \pi \ve_0 \left| x - R_j \right| } \]

\noindent
The Schr{\"o}dinger equation with this periodic potential is

\[ \op{H} \ket{\Psi(x)} = E \ket{\Psi(x)} \]

\[ \left[ \frac{-\hbar^2}{2 m_\mathrm{e}} \frac{d^2}{dx^2} + \sum_{i=1}^{N} \op{V}_i \right] \ket{\Psi(x)} = E \ket{\Psi(x)} \]

Just like in the molecular orbital picture theory (binding in diatomics discussed in Section~\ref{sxn:basics-of-bonding}) , we take the system's wavefunction to be a linear combination of atomic orbitals:

\[ \ket{\Psi(x)} = \sum_{j=1}^N c_j \ket{ \phi(x - ja)} \]

\noindent
Note the indexing in the above; each of the wavefunctions $j$ is identical to all other wavefunctions but spaced a distance $a$ from its neighbors.
We need to find the values of $c_j$ in this expansion taking what we know of the system.

In our combined system, we can only say that the electron density (an ``observable'')  is expected to be identically distributed about each nucleus; that is,

\[ n(x) = n(x + ja), \hspace{2em} \text{for all $j$} \]

\noindent
From quantum mechanics, we know the electron probability density can be found from the square of the wavefunction:

\begin{equation}\label{eq:tight-binding-nx}
n(x) = \left|\Psi(x)\right|^2 = \sum_{\text{all}\, j} c_j^2 \left| \phi(x - ja) \right|^2
\end{equation}

\begin{equation}\label{eq:tight-binding-nx+a}
n(x + a) = \left|\Psi(x + a)\right|^2 = \sum_{\text{all}\, j} c_j^2 \left| \phi(x + a - ja) \right|^2
\end{equation}


\noindent
The above two equations must be equal to one another (and must equal $n(x+2a)$, etc.).
If we pull out the $j{=}1$ term of equation~\eqref{eq:tight-binding-nx} and the $j{=}2$ term of equation~\eqref{eq:tight-binding-nx+a}, note that we have the same wavefunctions: $c_1^2 \left| \phi(x - a) \right|^2 $ and $c_2^2 \left| \phi(x - a) \right|^2$.
Therefore, $c_j^2 = c_{j+1}^2$ for any value of $j$, and we can enforce this with a periodic function such as

\[ c_j = \frac{1}{N} e^{i k j a} \]

(Note that according to \emph{Bloch's theorem}, the eigenfunctions are the product of a plane wave and a periodic function with the same periodicity as the potential:

\[ \Psi_{n \mathbf{k}} (\mathbf{r}) = e^{i \mathbf{k} \cdot \mathbf{r}} u_{n \mathbf{k}}(\mathbf{r}) \]

\noindent
The above is a generic 3-dimensional form, where the periodicity is built into the $k$ vector.)

In our one-dimensional system, the wavefunction becomes

\[ \ket{\Psi(x)} = \frac{1}{N} \sum_{j=1}^{N} e^{ikja} \ket{\phi(x-ja)}
= \sum_{j=1}^N c_j \ket{\phi_j} \]

\noindent We can now expand our Schr{\"o}dinger equation in this basis:

\[ \op{H} \sum_{j=1}^N c_j \ket{\phi_j}  = E\sum_{j=1}^N c_j \ket{\phi_j} \]

\noindent
The left-hand side of the above can be expressed as

\begin{align*}
		  \sum_{j=1}^N c_j \op{H} \ket{\phi_j} &= c_1 \op{H} \ket{\phi_1} + \cdots
= c_1 \left( \op{H}_1 + \sum_{j \ne 1} \op{V} \right) \ket{\phi_1} + \cdots
= c_1 \left( E_0 + \sum_{j \ne 1} \op{V_j} \right) \ket{\phi_1} + \cdots \\
		  &= \
		  \sum_{j=1}^N c_j \left( E_0 + \sum_{j \ne i} \op{V_i} \right) \ket{\phi_j}
\end{align*}

In the above, we have made the substitution that $\op{H} = \op{H}_j + \sum_{i\ne j} \op{V}_j$, since $\op{H} = -\frac{\hbar^2}{2 m_\mathrm{e}} \frac{d^2}{dx^2} + \sum_{i=1}^N \op{V}_i$ and $\op{H}_j = -\frac{\hbar^2}{2 m_\mathrm{e}} \frac{d^2}{dx^2} + \op{V}_j$, where $\op{H}_j$ is the individual atomic orbital Hamiltonian about ion $j$.
Further, $E_0$ is the solution to the individual atomic orbital Schr{\"o}dinger equation $\op{H}_j \ket{\phi_j} = E_0 \ket{\phi{j}}$, which by symmetry is the same for all isolated orbitals.

As in Section~\ref{sxn:basics-of-bonding}, we pre-multiply this Schr{\"o}dinger on an atomic-orbital basis with bras of each of our atomic wavefunctions.
We'll show this for $\bra{\phi_1}$ below:

\[ \sum_{j=1}^N c_j \left( E_0 + \sum_{j \ne i} \op{V_i} \right) \ket{\phi_j} = E\sum_{j=1}^N c_j \ket{\phi_j} \]

\begin{equation}\label{eq:tight-binding-schrodinger}
		  \sandwich{\phi_1}{c_1 \left(E_0 + \sum_{j \ne 1} \op{V}_j \right) }{\phi_1}
        + \cdots +
		  \sandwich{\phi_1}{c_N \left(E_0 + \sum_{j \ne N} \op{V}_j \right) }{\phi_N}
		  =
		  \bra{\phi_1} E \sum_{j=1}^N c_j \ket{\phi_j}
\end{equation}

We make a number of simplifications to the above equation:

\begin{itemize}
		  \item The right-hand side can be reduced to $c_1 E$ under the assumption that the atomic wavefunctions are orthonormal; that is, $\braket{\phi_i}{\phi_j} = \delta_{ij}$, where $\delta_{ij}$ is the Kronecker delta function\footnote{ $\delta_{ij} = 0$ if $j \ne i$ and $\delta_{ij} = 1$ if $j = i$}. 
		  \item Terms like $\sandwich{\phi_i}{c_i E_0}{\phi_i} = c_i E_0 \braket{\phi_i}{\phi_i}$ become $c_i E_0$ under the same assumption.
		  \item Terms like $\sandwich{\phi_i}{c_i \op{V}_{j}}{\phi_j}$ are approximated as 0 (when $j \ne i$). Note that when $j = i + 1$ this corresponds to shifts in electronic energies due to the neighboring potentials, which tend to cancel out. When $j$ is farther from $i$, there is very little direct interaction in this term.
		  \item The probability of the electron ``hopping'' sites is given by $V \equiv \sandwich{\phi_j}{V_j}{\phi_i} = \sandwich{\phi_j}{V_i}{\phi_i}$, where $j= i \pm 1$.
					 If $j$ and $i$ are farther apart, the probability of hopping $>1$ site away is negligible, so only nearest-neighbor interactions are considered.
\end{itemize}

\noindent
Taken all together, we can re-express equation~\eqref{eq:tight-binding-schrodinger} as


\[ c_j E_0 + \left(c_{j-1} + c_{j+1} \right) V = c_j E \]


\noindent
So we can solve for $E$ (no need for a determinant this time!) as

\[ E = E_0 + \frac{c_{j-1} + c_{j+1}}{c_j} V = E_0 + \left( e^{ika} + e^{-ika} \right) V \]

\noindent
where we have made use of the fact that $c_j = \frac{1}{N} e^{ikja}$ from earlier.
From Euler's rule\footnote{Euler gives $e^{ix} = \cos x + i \sin x$, where $i$ is the imaginary number.} this can be simplified to a cosine form:

\[  E_k = E_0 + 2 V \cos ka \]

\noindent
where, by periodic arguments, $k$ is given by

\[ k = \frac{2 \pi}{Na} l, \hspace{2em} l = 0, \pm 1, \pm2, \cdots \text{.} \]

\begin{figure}
\centering
\includegraphics[width=1.5in]{f/tight-binding/drawing.pdf}
\caption{Density of states as predicted with the tight binding model of $E_k = E_0 + 2 V \cos ka$. \label{tight-binding-dos}}
\end{figure}

The resulting form of the density of states in the tight binding model is shown in Figure~\ref{tight-binding-dos}.
This creates a roughly semicircular band of states with width of $4 V$ centered about the single orbital energy level of $E_0$. 
As will be discussed in Topic~\ref{ch:adsorption}, this is often taken as a model of the d-band and is used to rationalize trends in surface binding in transition metals.
This also can be used to give predictions of other factors, such as the cohesive energy in transition metals, as discussed in the textbook of Chorkendorff and Niemantsverdriet~\cite{Chorkendorff2005}.
The model shown here is a very simplified version of tight binding, which can take on much more sophisticated forms in the solid-state physics community.

\endgroup
