\begingroup
\chapter{Thermodynamics from the potential energy surface}

\newcommand{\q}[1]{q_\mathrm{#1}}
\newcommand{\cv}[1]{C_{V, \mathrm{#1}}}
\newcommand{\kb}{k_\mathrm{B}}
\newcommand{\pd}[3]{\left(\frac{\partial #1}{\partial #2}\right)_{#3}}


\section{Qualitative thermodynamics\label{sxn:qualitative-thermo}}

Consider the potential energy surface shown in Figure~\ref{PES-entropy}, with two local minima.
Which is more stable?
(Let's adopt a time-based definition of stability: if we watch the system for a very long time, which of the two wells is occupied longer?)
The left well is lower in energy; however, the right well is wider and thus has more entropy.

A good qualitative mental picture is to think of a ball rolling around on this surface.\footnote{Meaning it can only exist literally at the surface, it can't bounce or otherwise leave the surface. The ball also has no friction: kinetic and potential energy are conserved.}
In this picture, the kinetic energy of the ball---that is, $\frac{1}{2} m v^2$---is analogous to the temperature of an atomic system.\footnote{We'll go over the relationship between temperature and atomic motion more rigorously when we discuss molecular dynamics, \secref{sxn:molecular-dynamics}.}
If the ball has lots of kinetic energy, it will spend quite a bit of time in the right, wide well---that is, at the high-entropy state.
If the ball has a very low velocity, after it finds the left, narrow well, it gets stuck there and will spend most of its time in the left well.


\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/PES-entropy/drawing.pdf}
\caption{\label{PES-entropy}
A potential energy surface providing a qualitative understanding of energy versus entropy.
}
\end{figure}

This is a classic example of an energy--entropy trade-off.
In thermodynamics, speaking roughly, the depth of the well gives the energy (expressed as internal energy or enthalpy), while the width of the well gives the entropy.
If we use the well-known expression for free energy:

\[ \text{(free energy)} = \text{(energy)} - \text{(temperature)} \times \text{(entropy)} \]

\noindent
we can recover the qualitative interpretation of the ball's behavior above  in thermodynamic terms: at low temperatures, the low-energy state is favored, at high temperatures, the high-entropy state is favored.
Recall from thermodynamics the free energy can be expressed in either Gibbs or Helmholtz form as $G \equiv H - TS$ or $A \equiv U - TS$.
The conceptual understanding is identical whether you prefer $G$ or $A$.



\section{Important quantum and statistical mechanics principles\label{sxn:qm-sm-principles}}

Even if you have never taken a class in quantum or statistical mechanics, there are a few basic principles that every scientist and engineer should be familiar with.
We'll go over these and how they can be reconciled with atomic simulations.


\paragraph{Quantum mechanics.}
Quantum mechanics has a couple of key principles we need to keep in mind:

\begin{itemize}
\item
\textbf{Energy is quantized.}
Hence the name, quantum mechanics.
This is the key revelation of the field.
Just like matter cannot be subdivided indefinitely (eventually atoms or molecules are encountered, giving discrete building blocks), energy comes in packets or quanta.
Although at the macroscopic scale (where we live and have intuition) we think of energy as a continuous variable, quantum mechanics suggests that down in the quantum world it can occupy only discrete levels.
We often draw these allowed, discrete energy levels with energy on the vertical axis, as in Figure~\ref{fig:quantized-ZPE-Boltzmann}(a).

\item
\textbf{Heisenberg uncertainty.}
Heisenberg's famous uncertainty principle tells us we can't specify both the position ($x$) and the momentum ($p$) of a system exactly (and simultaneously); there is a limit to this knowledge.
This is commonly expressed as:

\[ \Delta x \, \Delta p \ge \frac{\hbar}{2} \]

where $\Delta x$ and $\Delta p$ are uncertainties in position and momentum.
That is, the more precisely we know position the less precisely we know momentum.
Note that we can relate these two variables, $x$ and $p$, to potential energy ($E_\mathrm{pot}=f(x)$) and kinetic energy ($E_\mathrm{kin}=p^2/2m$), respectively.
So we can also say we have limited certainty in the potential and kinetic energy of a system.

Although we can \emph{calculate} the energy of an atomic configuration at any point, we can't specify that our system occupy it.
That is, if we want our system to occupy the bottom of the potential energy well, we would be specifying $x$ exactly, so there must be a finite momentum, or kinetic energy.
This will act to propagate it away from the local minimum.
This results in a minimum energy level above the bottom of the PES well, and this first allowable energy state is known as the \underline{zero-point energy}.
See Figure~\ref{fig:quantized-ZPE-Boltzmann}(b).

\end{itemize}

\begin{figure}
\centering
\includegraphics[width=6.5in]{f/quantized-ZPE-Boltzmann/drawing.pdf}
\caption{(a) QM says energy is quantized, meaning it has discrete levels. (b) The Heisenberg uncertainty principle says the system's position can't be specified to be at the bottom of the well, hence the zero-point energy (ZPE). (c) Reconciliation of (a) and (b) with the picture of a local  minimum. (d) Statistical mechanics gives us the Boltzmann distribution, indicating the lowest energy states are exponentially most likely to be occupied. \label{fig:quantized-ZPE-Boltzmann}}
\end{figure}


We can combine these two facts with the local minimum on the potential energy surface; Figure~\ref{fig:quantized-ZPE-Boltzmann}(c).
First, we can state that the allowable occupations must be at some discrete levels.
The energy levels start above the minimum by a gap caused by the ZPE.
(We will discuss how to determine the spacing between the levels as well as the height of the ZPE later, but for now, we have a qualitative picture that there are discrete levels starting at ZPE.)
Thus, QM tells us that the energy comes in discrete levels starting at a level offset by a certain amount above the bottom of the potential energy well.

\paragraph{Statistical mechanics and the partition function.}
I'll present one principle from statistical mechanics, which provides the bridge for how we can link what happens at the scale of the PES with traditional thermodynamics.
The key principle is the \underline{Boltzmann factor}, which states that: For a large population of particles that exchange energy (through collisions, etc.), the probability of observing an individual particle at an energy level $\varepsilon_i$ is:

\[ \underbrace{\text{prob}[\varepsilon_i]}_{\equiv p_i} \propto \underbrace{\exp \left\{ \frac{-\varepsilon_i}{\kb T} \right\}}_\text{``Boltzmann factor''}
\]

\noindent
The derivation of this is quite straightforward, and comes from reconciling the conservation of energy with the idea of allowable energy levels that start at some minimum value.

If we know all the possible states (that is, all the possible energy levels), then we can write this exactly by normalizing the Boltzmann factor:

\begin{equation}\label{eq:prob_partition}
p_i = \frac{\exp \left\{ \frac{-\varepsilon_i}{\kb T} \right\}}{\sum_j \exp \left\{ \frac{-\varepsilon_j}{\kb T} \right\}}
\end{equation}

This gives us the occupation of each energy level.
If we make a plot of this function, it's just an exponential decay as shown in Figure~\ref{fig:quantized-ZPE-Boltzmann}(d), and we get different plots at different temperatures.
So the Boltzmann distribution tells us that \emph{the lowest energy levels are exponentially more likely to be occupied, but the relative occupation of the higher-energy levels increases with temperature}.

Summarizing:


\begin{itemize}
\item  Quantum mechanics: allowable energy levels
\item Statistical mechanics: population of these energy levels
\end{itemize}

The denominator of the equation above is called the \emph{``partition function''}:

\[ Q \equiv {\sum_j \exp \left\{ \frac{-\varepsilon_j}{\kb T} \right\}}
\]

\noindent which is the Boltzmann sum of all the energy level probabilities in the system.
It is often said that the partition function contains all the thermodynamic information of the system.
By inspection, we see that it contains all the energy levels $\varepsilon_j$, which come from quantum mechanics, and the population of them, which comes from statistical mechanics.

\section{Thermodynamic quantities from the partition function}

Since the partition function contains all the thermodynamic information of the system, then we should be able to derive any thermodynamic quantity from $Q$.
You may recall these relations from a stat mech course; for example,

\[ U = kT^2 \left( \frac{\partial \ln Q}{\partial T} \right)_{V,N} \]

\[ S = k \ln Q + \frac{U}{T} \]

\[ \mu = -kT \left( \frac{\partial Q}{\partial N} \right)_{T, V} \]

\noindent
(Derivations of $U$ and $S$ are shown in Section~\ref{sxn:thermoderivation}.)
Of course, we need to find $Q$.
This partition function represents the discrete energy levels rising above a local minimum.
With complete knowledge of the PES about the local minimum, one could (in principle) find these energy levels.
For some limited systems this is the approach, and we'll examine this approach for a one-dimensional system later in this class.
But in practice, one generally makes approximate forms depending on the situation encountered.

\subsection{Derivation of thermodynamic quantities\label{sxn:thermoderivation}}

Here we show the derivation of the internal energy and entropy from the partition function.
You can find this for other thermodynamic quantities in statistical mechanics textbooks.

\paragraph{Internal energy, $U$.}
The internal energy of a system of particles is the sum of each particle's energy, which is easier to count as a sum of the allowable energy levels $\varepsilon_j$ times their occupation\footnote{Here, we are assuming that for a large system the occupation of each level is exactly proportional to its probability; that is, the system is in its most probable state. We often say the system is ``thermalized'' to mean the same thing. So we can think of $p_j$ as being equivalently the probability of being in state $j$ or the fraction of particles in state $j$.} $p_j$:

\[ U = \sum_j p_j \varepsilon_j \]

\noindent
We can substitute in the expression for the probability based on the Boltzmann factor, equation~(\ref{eq:prob_partition}), letting $\beta \equiv 1/(k_\mathrm{B} T)$:

\[ p_j = \frac{e^{-\beta \varepsilon_j }}{\sum_k e^{-\beta \varepsilon_k }}
 = \frac{e^{-\beta \varepsilon_j }}{Q} \]

\[ U = \frac{1}{Q} \sum_j \varepsilon_j e^{-\beta \varepsilon_j }  \]

\noindent
Note that

\[ \pd{Q}{\beta}{V,N} = \pd{\sum_j e^{-\beta\varepsilon_j}}{\beta}{V,N}
= - \sum_j \varepsilon_j e^{-\beta \varepsilon_j } \]

\noindent
Therefore

\[ U = - \frac{1}{Q} \pd{Q}{\beta}{V,N} = \boxed{- \pd{\ln Q}{\beta}{V,N}} \]

\paragraph{Entropy, $S$.} The entropy can be expressed in terms of the probability as

\[ S = - k \sum_j p_j \ln p_j \]

\begin{align*}
\frac{S}{k}
&= - \sum_j \frac{e^{-\beta \varepsilon_j}}{Q} \ln \left[ \frac{e^{-\beta \varepsilon_j}}{Q} \right] \\
&= -  \sum_j \frac{e^{-\beta \varepsilon_j}}{Q} \Big[ -\beta \varepsilon_j - \ln{Q} \Big] \\
&= \beta \underbrace{\frac{\sum_j \varepsilon_j e^{-\beta \varepsilon_j}}{Q}}_{=U}   + \underbrace{\frac{\sum_j e^{-\beta \varepsilon_j}}{Q}}_{=1} \ln Q \\
&= \boxed{\beta U + \ln Q}
\end{align*}

\section{The ideal-gas limit \label{section:thermo-idealgas}}


A common limit is the ideal-gas assumption, which is actually more powerful than you may initially assume.\footnote{This approach can work excellently for describing liquid water or even a solvated species, with the right thermodynamics. For example, liquid water is certainly not an ideal gas, but it is in equilibrium with water vapor, which is represented very well as an ideal gas at room temperature. Therefore, we can calculate the chemical potential of gas-phase water at the vapor pressure of our liquid water, and by definition this is equal to the chemical potential of liquid water at the same temperature. You can do similar tricks with solvated species and Henry's law, for example.}
The ideal-gas approximation assumes that the partition function is separable among particles, first:

\begin{equation}\label{eq:q-indistinguishable} Q = \frac{q^{N_\mathrm{m}}}{N_\mathrm{m}!} \end{equation}

\noindent
where $N_\mathrm{m}$ is the number of molecules, and $q$ is the partition function of an individual molecule.
($Q$, the partition function we introduced earlier, is for the whole \emph{system} of $N_\mathrm{m}$ particles. $q$ is what we actually get from the PES when simulating a single molecule; we extrapolate this to large systems with the above equation.)
Then, it assumes $q$ (the per-particle partition function) is separable into  different components (types of motion), such as translational, rotational, vibrational, and electronic:

\begin{equation}\label{eq:q-ideal-gas} q = \underbrace{\q{trans} \,\,\, \q{rot} \,\,\, \q{vib}}_\text{nuclear} \underbrace{\q{elec}}_\text{electronic} \end{equation}

The first three terms have to do with nuclear motion, while the last has to do with the possibility of electronic excitations.
For many stable systems, the first electronic excitation is at a high energy (\textit{i.e.}, $\Delta E_{\mathrm{elec}} \gg k_\mathrm{B} T$) and is inaccessible, so $\q{elec}$ can be ignored, and we will in general ignore that one in this class (unless you encounter it in a system you have chosen for your project).

This leaves us with the nuclear terms; for a molecule made up of $N$ atoms, there are $3N$ nuclear degrees of freedom; that is, it takes $3N$ numbers to describe the position of all the atoms in the system.

In the absence of an external field, the molecule could translate as a whole in the $x$, $y$, or $z$ dimension and it would not change in energy; these are the 3 translational degrees of freedom and are treated specially: as a quantum mechanical ``particle in a box''.
Similarly, the molecule could rotate as a whole in three dimensions.
However, there are a couple of exceptions: a linear molecule (like CO or \ce{CO2}) can only rotate in two directions, which you can work out if you try to rotate a pencil in the air.
In the most extreme case, a monotonic gas has no rotations at all.

The remaining degrees of freedom are considered vibrational; thus the number of vibrational degrees of freedom is
\begin{itemize}
\item $3N-6$ (general)
\item $3N-5$ (linear molecule)
\item $3N-6 = 0$ (monatomic species)
\end{itemize}

\noindent
This is summarized in Table~\ref{tab:degrees-of-freedom}.

\begin{table}
\centering
\caption{Nuclear degrees of freedom for ideal gases. \label{tab:degrees-of-freedom}}
\begin{tabular}{lrrrr}
\hline \hline
Geometry & Translational & Rotational & Vibrational & Total \\
\hline
Monatomic (1 atom) & 3 & 0 & 0 & $3N$ \\
Linear & 3 & 2 & $3N-5$ & $3N$ \\
General & 3 & 3 & $3N-6$ & $3N$ \\
\hline
\end{tabular}
\end{table}

From statistical mechanics, the thermodynamic quantities of interest can be derived from the partition functions.
See, for example, Cramer Chapter 10~\cite{Cramer2004}.
This leads to relationships such as, for enthalpy,

\[ \eqlabel{eq:enthalpy-from-stat-mech} H(T) = E_\mathrm{pot} + E_\mathrm{ZPVE} + \int_0^T C_P \, dT \]

\noindent
where $E_\mathrm{pot}$ is the local minimum on the potential energy surface corresponding to this molecule.
The remaining terms are:

\[ \eqlabel{eq:zpve} E_\mathrm{ZPVE} = \frac{1}{2} \sum_i^\text{vib DOF} \hbar \omega_i \]

\[ C_P = \kb + \cv{trans} + \cv{rot} + \cv{vib} + \cv{elec} \]

\[ \cv{trans} = \frac{3}{2} \kb \]

\[ \cv{rot} = 
 \left \{ \begin{array}{ll}
\frac{3}{2} \kb\, & \text{\ (for nonlinear molecule)} \\
\kb \, & \text{\ (for linear molecule)} \\
0 \, & \text{\ (for monatomic gas)} \\
 \end{array} \right. \]

\[ \int_0^T \cv{vib} \, dT = \sum_i^\mathrm{vib DOF} \frac{\hbar \omega_i}{e^{\hbar \omega_i/ \kb T} - 1} \]

\noindent In the above, $\hbar \omega_i$ is the \textit{energy level} of vibrational mode $i$.
These energy levels are typically found from a normal-mode analysis, as discussed in \secref{section:nma}, where we will give more information on these derivations.
The zero-point (vibrational) energy $E_\mathrm{ZPVE}$ also comes from this term; we'll discuss this in more depth in \secref{sxn:energy-levels}.
Note that the zero-point energy, as discussed in \secref{sxn:qm-sm-principles}, exists for \emph{all} degrees of freedom, not just the vibrational.
By convention, we tend to separate out only the vibrational contribution to the zero-point energy in equation~\eqref{eq:enthalpy-from-stat-mech} since this term has no temperature dependence and thus would not enter the heat capacity.
Note that some textbooks and manuscripts adopt different conventions, so just be aware that this may look different as you read the literature.


If there are no accessible electronically excited states, then $\cv{elec}=0$, which is the assumption we'll typically make in this class.

There are a couple of things to note in the above relations which you should remember from your undergraduate thermodynamics course, that pertain specifically to ideal gases.
First, for and ideal gas only, $C_P = C_V + \kb$ (or on a molar basis, $C_P = C_V + R$); you can see this was used in the $C_P$ equation above.
Second, note that $H$ is only a function of $T$, not $P$, for an ideal gas.
(You should recall this: first, the internal energy $U$ is only a function of $T$, and $H \equiv U(T) + PV = U(T) + RT$ for an ideal gas.)
All of the pressure dependence will enter through the entropy term.


\paragraph{Ideal-gas entropy.}
The ideal-gas entropy, $S$, can also be derived (see Cramer~\cite{Cramer2004}) from the partition function, and is given by

\[ S(T, P) = S_\mathrm{trans}(T, P^\circ) + S_\mathrm{rot}(T) + S_\mathrm{vib}(T) + S_\mathrm{elec} - \kb \ln \frac{P}{P^\circ} \]


\noindent where the individual terms are given by

\[ S_\mathrm{trans} = \kb \left\{ \ln \left[ \left( \frac{2 \pi M \kb T}{h^2} \right)^{3/2} \frac{\kb T}{P^\circ} \right] + \frac{5}{2} \right\} \]

\[ S_\mathrm{rot} =  \left\{ \begin{array}{ll}
\frac{3}{2} \kb + \kb  \ln \left[ \frac{ \sqrt{ \pi I_\mathrm{A} I_\mathrm{B} I_\mathrm{C}} }{\sigma} \left( \frac{8 \pi^2 \kb T}{h^2} \right)^{3/2} \right] & \text{, general} \\
\kb + \kb \ln \left( \frac{8 \pi^2 I \kb T}{\sigma h^2} \right)  & \text{, for linear} \\
0 & \text{, for monatomic}
\end{array} \right.  \]

\[ S_\mathrm{vib} = \kb \sum_i^\text{vib dof} \left[ \frac{\hbar \omega_i}{\kb T \left( e^{\hbar \omega_i/\kb T} - 1 \right) } - \ln \left( 1 - e^{-\hbar \omega_i / \kb T} \right) \right] \]

\[ S_\mathrm{elec} = \kb \ln \left[ 2 \times \left( \text{spin multiplicity} \right) + 1 \right] \]

In the above equations the principle moments of inertia of the molecule appear as $I$ (or $I_\mathrm{A}$, ...).
These are the eigenvalues that result when diagonalizing the inertial tensor.
In practice, these are routine to calculate and are automated by software; see an example script in Section~\ref{code:momentsofinertia}.
(You should notice when you run this example script that you get three unique moments for a general molecule but two degenerate moments for a linear molecule.)
$\sigma$ is the symmetry number of the molecule; see for example Appendix B in Cramer~\cite{Cramer2004} for an example of how to calculate this for generic cases.

Combining the above, we can get the system free energy ($G$), via $G \equiv H - TS$.
Generally, these functions are embedded into ASE; and the full equations are given on the ASE website.


\paragraph{Other limiting cases.}
A couple of other common limiting cases are below; both are standardized in ASE:


\begin{itemize}

\item
The harmonic limit.
Like the ideal gas limit, where all modes are treated harmonically, except there is no translation or vibration term.
This is often used for an adsorbate on a surface and includes only the vibrational terms.
Note there is some question as to the accuracy for low-frequency modes, such as hindered translations and rotations.

\item
Phonons in solids.
The vibrational modes in solids, are known as phonons.
Standard expressions exist for converting phonon densities of states into thermodynamic quantities.

\end{itemize}

\section{Free energy diagrams}
With this, we now have the capability to produce one of the standard tools we need to assess reactions: the free energy diagram. An example is shown in Figure~\ref{free-energy-diagram}. We start with the potential energy (at the minimum) calculated from our electronic structure calculator. In principle this correction can add to or subtract from the base potential energy.
If we consider the ethanol example; there are two molecules created when ethanol decomposes; thus the term $-TS$ is larger for the intermediate and final states than the initial states; this is indicated on this figure by the $G$ being lower than $E_\mathrm{pot}$ for these two states, while higher for the initial state.

\cen{CH3CH2OH -> CH3 + CH2OH} 
\cen{CH3 + CH2OH -> CH4 + CH2O} 

This free energy already gives us a qualitative understanding of the relative difficulties of various reaction pathways, and we can make quantitative predictions about the equilibrium ratios of the initial and final state.
Note this picture is still not complete until we add in barriers.



\begin{figure}
\centering
\includegraphics[width=3.0in]{f/free-energy-diagram/drawing.pdf}
\caption{An example free energy diagram, showing the $G$ values as offsets from the potential energies, via statistical mechanics corrections. \label{free-energy-diagram}}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=6.5in]{f/free-energy-diagram/catalyst.pdf}
\caption{An example of a reaction with and without a catalyst. The unstable molecular fragments bind can bind to the surface (often a transition metal), which stabilizes the reaction intermediates. A more reactive metal will bind the fragments more strongly, giving a first-order descriptor of the fit of various catalysts for a particular reaction. \label{free-energy-diagram-catalyst}}
\end{figure}

\paragraph{Catalysis.} Note if a catalyst is present it will change the energy of the intermediates, by allowing them to bind to the catalyst. In this case the catalyst is shown as a metal surface: a ``heterogeneous catalyst''. We conventionally write the reaction in the presence of a catalyst with $*$ representing a binding ``site'' on the surface as:

\cen{CH3CH2OH + 2 $*$ -> $*$CH3 + $*$CH2OH} 
\cen{$*$CH3 + $*$CH2OH -> CH4 + CH2O + 2$*$} 


\cen{CH3CH2OH -> CH4 + CH2O \text{  [net]}}

\noindent In this way, $*$ is treated stoichiometrically: that is, the number on the left and right sides of the arrow have to match. An example free energy diagram for our hypothetical reaction is shown in Figure~\ref{free-energy-diagram-catalyst}.

The binding strength of the adsorbates will change with catalyst material; as a first-order descriptor this can be seen to control the ruggedness of the free energy landscape. Typically, the less rugged the better kinetics.



\section{Background: Why do partition functions multiply?}

If the partition function is a sum of Boltzmann factors, it's a very common question of why is it that we commonly see partition functions multiplied by one another, as in equations~ \eqref{eq:q-indistinguishable} and \eqref{eq:q-ideal-gas}.
Here, we'll address that concern.

Let's start with the picture of Figure~\ref{fig:quantized-ZPE-Boltzmann}.
If we have only a single molecule, its single-molecule partition function is

\[ q = \sum_i e^{-\varepsilon_i / (\kb T)} \]

\noindent
Recall that for a single particle, each term $e^{-\varepsilon_i / (\kb T)}$ gives the relative probability of the molecule being in energy level $\varepsilon_i$.
(Whereas for a collection of molecules, each term in $Q$ is the relative occupation of that energy level---the fraction of particles carrying each level of energy.)

However, the example of Figure~\ref{fig:quantized-ZPE-Boltzmann} is for a one-dimensional potential energy surface; a real molecule lives on a $3N$-dimensional PES.
We can't draw in $3N$-dimensional space, but we can draw in two-dimensional space, and a 2-d example will give us all the intuition we need.

\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{f/partition-functions/drawing.pdf}
\caption{A potential energy surface with two vibrational degrees of freedom, which we are assuming are independent of one another.
\label{fig:partition-functions}}
\end{figure}

See Figure~\ref{fig:partition-functions}, where we have drawn a 2-d potential energy surface as a contour plot.
Here, we can see that our molecule can move in two dimensions, and thus there are two vibrational modes, which we labeled A and B.\footnote{We're assuming they are independent; we'll say more about this when we study vibrations in a subsequent part of the course.}
We have drawn the potential energy surfaces for these two modes on the right side of the figure.
In this example, mode A is relatively ``tight'' and has large spacing between energy levels, whereas mode B is relatively ``loose'' and has many dense energy levels.
For the sake of our example, let's assume the allowable energy levels of mode A are $\{ 5, 10, 15, 20, 25, \cdots \}$ and those of mode B are $\{ 1, 2, 3, 4, 5, \cdots \}$.
Thus, the allowable \emph{total} energy levels of the molecule are

\begin{center}
\begin{tabular}{c}
\hline \hline
$\varepsilon_{\mathrm{A},i} + \varepsilon_{\mathrm{B},j} = \varepsilon_{\mathrm{tot},k}$ \\
\hline
5 + 1 = 6 \\
5 + 2 = 7 \\
5 + 3 = 8 \\
5 + 4 = 9 \\
5 + 5 = 10 \\
5 + 6 = 11 \\
10 + 1 = 11 \\
5 + 7 = 12 \\
10 + 2 = 12 \\
5 + 8 = 13 \\
10 + 3 = 13 \\
$\vdots$ \\
\hline
\end{tabular}
\end{center}

Note there are two combinations that add up to 11; we call this energy level \emph{degenerate}---or alternatively we say energy 11 has a degeneracy of two.
The partition function for this molecule, accounting for both degrees of freedom, is thus

\[ q = \sum_i \sum_j e^{-\left( \varepsilon_{\mathrm{A},i} + \varepsilon_{\mathrm{B},j}\right) / (\kb T)} \]

Using properties of exponents and summations, we can manipulate this:

\[ q = \sum_i \sum_j e^{- \varepsilon_{\mathrm{A},i} / (\kb T)}  e^{-\varepsilon_{\mathrm{B},j} / (\kb T)}  = \underbrace{\sum_i e^{- \varepsilon_{\mathrm{A},i} / (\kb T)}}_{\equiv q_\mathrm{A}} \underbrace{\sum_j  e^{-\varepsilon_{\mathrm{B},j} / (\kb T)}}_{\equiv q_\mathrm{B}} \]

\[ q = q_\mathrm{A} \, q_\mathrm{B} \]

\noindent
Thus, we see that the molecule's partition function is the \emph{product} of its two vibrational parition functions, for this simple example.
This will generalize to a molecule with $3N$ degrees of freedom\footnote{So long as they are independent.}: the total partition function will be a product of its component partition functions, as in equation~\eqref{eq:q-ideal-gas} for the ideal gas.
You can readily work out that the same concept holds for a collection of $N_\mathrm{m}$ molecules: the partition function will be a product of that for each molecule.
We see this in equation~\eqref{eq:q-indistinguishable}, where $Q = q^{N_\mathrm{m}} / N_{\mathrm{m}}!$.
In this case, the denominator is present to account for the fact that the molecules are indistinguishable; we leave discussion of this aspect to statistical mechanics courses.

\section{Script(s)}

\subsection{Finding moments of inertia\label{code:momentsofinertia}}
\pcode{f/scripts/momentsofinertia.py}

\endgroup
