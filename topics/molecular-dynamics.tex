\begingroup
\chapter{Molecular dynamics\label{sxn:molecular-dynamics}}

\newcommand{\kb}{\ensuremath{k_\mathrm{B}}}

\section{Molecular dynamics}

Molecular dynamics simulations are often used to simulate materials properties, biological processes, or to sample the potential energy surface.
They are used in many advanced algorithms such as global structural optimization (e.g., ``minima hopping'') or searches for reaction pathways (``transition-path sampling'').
We also examined an example of an aquaporin (water-specific channel in a cell membrane) in lecture.

\paragraph{Nomenclature.}
I will use the term ``molecular dynamics'' to indicate the propagation of atomic positions and velocities via Newton's laws of motion.
Note that the term molecular dynamics is sometimes used in conversation---incorrectly, in my book---to describe when non-\textit{ab initio} calculations such as force fields (or interatomic potentials) are used instead of electronic structure codes.
(Someone might ask you: ``Did you do DFT or MD?'')
This is because MD is often used for long time-scale and large system-size problems where electronic structure codes are not feasible, and thus interatomic potentials are often employed to make the problem tractable.
We will talk about these potentials more in a future topic.
If you state that you performed an MD simulation, most people will assume you used an interatomic-potential-based method, and we often use the term  ``ab initio molecular dynamics'' (AIMD)---or sometimes ``Born-Oppenheimer molecular dynamics (BOMD)---to indicate if we did the more expensive electronic structure-based method.

There are a few other varieties to be aware of, such as Carr-Parrinello molecular dynamics (CPMD), which is intermediate between \textit{ab initio} and potential-based methods in terms of cost and accuracy, and path-integral molecular dynamics which is more sophisticated and also treats the atoms quantum mechanically.

\subsection{Mechanics}

Molecular dynamics is essentially moving atoms according to Newton's laws of motion.
Generally, all particles are treated classically---that is, Heisenberg's uncertainty principle is ignored.
\footnote{Generally speaking, the classical approach can be a problem if tunneling is expected, which happens to light atoms at low temperatures. Roughly, reactions involving H atoms at temperatures at or below room temperature is when this kicks in, and there are methods to deal with this such as path-integral molecular dynamics. We looked at some videos from Angelos Michaelides in this regard at the break. \url{http://www.chem.ucl.ac.uk/ice/}}

The basic Newton's second law ($F = ma$) expression, with $i$ indexing the atom number and $\vec{x}_i \equiv (x_i, y_i, z_i)$ gives 

\[ m_i \frac{d^2 \vec{x}_i}{dt^2} = \vec{F}_i \, \left(= - \vec{\nabla} E_\mathrm{pot} \right)
\]

\noindent from which we can just write that the acceleration is $\vec{a}_i = \vec{F}_i / m$.
The trick is to numerically integrate this.
In molecular dynamics we keep track of both the position and velocity (or more typically, the momentum) of each particle, for a total of $6N$ coordinates that we track.
(The force, and thus the acceleration, on each particle comes from the electronic structure or force-field calculator.)
This $6N$-dimensional space that tracks position and momentum of each particle is known as \textit{phase space}, whereas the $3N$-dimensional space that tracks only the position of each atom---which we discussed earlier in the context of potential energy surfaces and vibrations---is referred to as \emph{configuration space}.

If we integrate Newton's Laws directly, we conserve energy---that is, although the atoms exchange potential and kinetic with one another, the system's total energy is fixed.
If we do this in a simulation box of constant size and with a constant number of atoms, we refer to this as the ``NVE'' (constant \textbf{N}umber, \textbf{V}olume, and total \textbf{E}nergy) ensemble.
This is also referred to as the ``microcannonical'' ensemble.

\subsection{Thermalization}

The system needs to start from some initial state.
Unless the user has a reason to specify something else, a good starting point is to set the initial simulation temperature by sampling velocities from the \underline{Maxwell-Boltzmann} velocity distribution:

\[ f(v) = \left( \frac{m}{2 \pi \kb T} \right)^{3/2} 4 \pi v^2 \exp \left\{ \frac{- m v^2}{2 \kb T} \right\} \]

\noindent
This is derived from the Boltzmann distribution, which states that lower energy states are exponentially more likely to be occupied, but treats all degrees of freedom independently. Thus, the probability of all three components of the velocity being zero simultaneously is low and the distribution has a different shape than the classical Boltzmann distribution we think of.
A comparison of the Maxwell-Boltzmann distribution to the standard Boltzmann distribution is shown in Figure~\ref{fig:maxwell-boltzmann}.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/maxwell-boltzmann/out.pdf}
\caption{Comparison of Boltzmann and Maxwell--Boltzmann distributions.
\label{fig:maxwell-boltzmann}}
\end{figure}

Note that systems will tend to ``thermalize'' towards such a Maxwell--Boltzmann distribution on their own though interchange of momentum between particles. However, if we are doing averaging in our MD simulation we often have to throw away this ``burn in'' period, and starting in a realistic distribution will reduce the burn-in time. More will be said about temperature later.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/equipartition/drawing.pdf}
		  \caption{Equipartition of energy during a typical molecular dynamics simulation starting from a completely relaxed ($E_\mathrm{pot}=0$) state. \label{equipartition}}
\end{figure}

Also, it should be noted that systems interchange energy between potential and kinetic energy forms, in the \emph{equipartition of energy} theorem, which states that the energy tends to partition equally to each degree of freedom of the system, on average.
Thus, a system that starts out with all of its energy as potential energy (with all atoms fixed), about half of the energy will convert to kinetic energy.
Conversely, if a system starts out with all its energy as kinetic energy, about half of the energy will convert to potential energy.
A result of this is if you set up an MD simulation starting from a relaxed state (that is, a minimum on the potential energy surface), much of the kinetic energy you add will be converted to potential energy during the simulation, so your temperature will equilibrate at some lower value than what you initially specified.
See Figure~\ref{equipartition}.

In the limit that one starts from a fully relaxed structure and thermalizes to a temperature $T$, we would expect the system to equilibrate to a temperature of $T/2$.
Thus, a good rule-of-thumb is to thermalize to double the desired simulation temperature.


\subsection{Numerical integration: Velocity Verlet}

In position--velocity coordinates\footnote{Equivalently, many codes will work in position--momentum coordinates.} we are attempting to integrate the following system of differential equations

\begin{align*}
\frac{d \vec{x}_i}{dt} &= \vec{v}_i \\
		  \frac{d \vec{v}_i}{dt} &= \frac{\vec{F}_i}{m_i} \left(= \vec{a}_i \right)
\end{align*}

\noindent for $i = 1, 2,  \cdots, N$, where $N$ is the number of atoms for a total of 6$N$ ODEs. Recall that the only input is the forces, $\vec{F}_i$, which come from the electronic structure or interatomic potential method. Because we don't have a closed form for $\vec{F}_i$ as a function of position, we must integrate numerically.

The simplest way to numerically integrate is to use a first-order Taylors series truncation, resulting in the ``Explicit Euler'' (EE) method:

\[ f(t + \Delta t) = f(t) + f'(t) \cdot \Delta t \]

\noindent which for position and velocity becomes

\[ \vec{r}_i (t + \Delta t) = \vec{r}_i (t) + \vec{v}_i(t) \Delta t \]

\[ \vec{v}_i (t + \Delta t) = \vec{v}_i (t) + \vec{a}_i(t) \Delta t \]

\noindent
Although simple, this approach has a couple of drawbacks. First, it will give decent results only at tiny timesteps, which makes this a very expensive method. Second, it is not reversible---we should be able to run our simulation forwards or backwards and get the same result.\footnote{There are multiple reasons for this---primarily that the starting positions and momentums are arbitrary, so it shouldn't matter if we start at either end of a trajectory. It becomes important in some methods used to search for many reaction pathways, like TPS. Here, part of the technique involves starting from a position near the transition-state surface, and integrating in both directions to make a reaction path (or transition path). Only if we are reversible does one recover a sensible trajectory that is equivalent to starting at either end.}
This will not be the case for EE because the derivatives are evaluated at $t$ (one side of the range only). Thus, this basic approach is typically not used.

One of the most common approaches in use is known as the \underline{Velocity Verlet} algorithm. It also has subtly different forms, but a basic approach is below, where each step is propagated across all atoms $i=1,...,N$.


\begin{enumerate}
\item Estimate the velocities at the half-interval from the velocities and accelerations at the interval start:
		  \[ \vec{v}_i (t + \tfrac{1}{2} \Delta t) = \vec{v}_i (t) + \tfrac{1}{2} \vec{a}_i (t) \Delta t \]
\item Estimate the positions at the interval end from the positions at the interval start and the velocities at the half-interval:

		  \[ \vec{x}_i (t + \Delta t) = \vec{x}(t) + \vec{v}(t + \tfrac{1}{2} \Delta t) \Delta t \]

\item Perform a force call to get the accelerations at the interval close $\vec{a}_i(t + \Delta t)$ assuming the positions above. (This step is the one that calls the electronic structure or interatomic potential calculator.)

\item Calculate the velocity at the interval close from the half-interval velocities and the interval close acceleration:

		  \[ \vec{v}_i(t + \Delta t) = \vec{v}_i (t + \tfrac{1}{2} \Delta t) + \tfrac{1}{2} \vec{a}_i (t + \Delta t) \Delta t \]
\end{enumerate}


\subsection{Time steps}

Generally, the force call will be the expensive part---which above happens once per time step---so we want to have longer time steps to reduce the amount of computational work required to simulate a given amount of time. Understanding the normal modes of the system can help with this, and we are limited by the highest frequency modes, which tend to be things like \ce{O-H} bonds. Typically, time steps of 0.5-1~fs are required in cases where H bonds are present to capture this movement. This tends to make MD calculations rather expensive. In cases where no H's are present, like in bulk metals or graphene, longer time steps may be feasible. Occasionally, if H's are present but the researchers aren't too concerned with their precise physical treatment (e.g., if they are terminal H's on graphene), then the H's may be treated as having fixed bond lengths to get rid of the vibrational mode, and longer time steps can be used. (Note in this case this removes degrees of freedom from the system, so compensation must be made to the temperature, as described in the next paragraph.)


\subsection{Simulated temperature}

In principle, according to the equipartition of energy theorem, the energy is divided among all the degrees of freedom of the system with equal probability.
That is, if we have a large enough system and we examine the kinetic energy of each atomic degree of freedom, we'd expect to see a Boltzmann distribution among the energies.
In principle, this is also true of the potential energy per atom.
However, we can't easily examine the potential energy for a couple of reasons: (1) all we get out of an electronic structure calculation is a total potential energy for the system, it is not broken down per atom, and (2) even if it were to be broken down per atom, it might be challenging to assign where ``zero'' is on the potential energy scale, as the system can move around and find new minima.

For this reason, we typically rely on the kinetic energy to provide us with the system temperature.
Temperature in these simulations is given by the kinetic energy; specifically

\begin{equation}\label{temperature-kinetic}
\mathrm{K.E.} = \sum_{i = 1}^{N} \tfrac{1}{2} m_i {v}_i^2 = (3N) \tfrac{1}{2} \kb T
\end{equation}

\noindent where $v_i$ is the magnitude (norm) of the velocity of atom $i$.
Here $N$ is the number of atoms in the system and thus $3N$ is the number of degrees of freedom; thus each degree of freedom contains an amount of energy equal to $\frac{1}{2} \kb T$.
Note that it is common to try to reduce the degrees of freedom in the system, such as by fixing some bond lengths or fixing the atoms in a crystal far away from where chemical action is expected.
If this is the case, the $3N$ in equation~(\ref{temperature-kinetic}) must be reduced by the number of degrees of freedom removed.

Note that this comes from the average energy in a Boltzmann distribution.
If we consider only a single degree of freedom of the velocity $v_x$ the probability of observing velocity $v_x$ can be determined (after adding the right normalization, which consists of integrating from $-\infty$ to $+\infty$:

\[ \mathrm{prob}(v_x) = \sqrt{\frac{m}{2 \pi \kb T}} \exp \left\{ \frac{- m v_x^2}{2 \kb T} \right\} \]

\noindent we can get the average value of $v_x^2$ by using:

\[ \langle v_x^2 \rangle = \int_{-\infty}^{\infty} v_x^2 \cdot \mathrm{prob}(v_x) d v_z  = \frac{\kb T}{m} \]

\noindent
and so the average kinetic energy is $\frac{1}{2} m \langle v_x^2 \rangle = \frac{1}{2} \kb T$ for this single degree of freedom.


Since an NVE simulation keeps the \textit{total energy} constant, there is a constant interchange between potential and kinetic energies.
Hence, the kinetic energy is not constant and the temperature can vary wildly throughout the simulation.
Also, note that due to these variations that temperature is ill-defined in small systems (since it is really defined via the Boltzmann distribution, which assigns probabilities to each energy level and we do not have enough particles to have good statistics).
Often, we are interested instead in a simulating properties at a constant temperature, so changes in the simulation must be made in order to conduct it at NVT conditions. This is known as the canonical ensemble, which is discussed in the next section.

\section{Ensembles}

\subsection{Microcanonical: NVE}

Recall that MD propagates a system forward in time according to Newton's laws of motion.
The simplest way to set up a simulation is in a constant-size unit cell and with a fixed number of particles, (V and N).
If we propagate Newton's Laws directly we have constant total energy (E).
This gives us the ``NVE'' (or microcanonical) ensemble.
However, at constant total energy this means the kinetic energy is variable, and thus the temperature is variable.
Processes of interest tend to occur at constant temperature, however, and modifications can be made to run at NVT conditions (the canonical ensemble) instead.
Recall that temperature can be related to the kinetic energy by

\[ \sum_{i = 1}^{N} \frac{p_i^2}{2m_i} = (3N) \tfrac{1}{2} \kb T \]


\noindent where $3N$ is the number of degrees of freedom in the system and we have written in terms of momentum $p_i \equiv m_i v_i$ rather than velocity for compatibility with the next set of equations.

\subsection{Canonical ensemble: NVT}

We are often concerned with processes that take place at a constant, specified temperature $T$.
Yet if we blindly follow Newton's (or quantum) laws of motion, we'll end up with a system whose total energy, and not its temperature, is held constant.

There are multiple ways to create simulations that run with temperature fixed; that is, in the NVT (also known as ``canonical'') ensemble.
First, note that in thermodynamics treatments, we consider our system to be coupled to a large bath or reservoir to enforce constant temperature.
For this reason, some propose that in atomistic simulations that all interactoins that influence the temperature should only enter through the boundary, simulating a bath interaction.
Other treatments speed up or slow down atoms in the simulation directly, which gives a more uniform treatment of the thermal correction and avoids thermal gradients within the simulation.
All means of holding the temperature ``constant'' will introduce artifacts such as this, and they also will tend to reduce fluctuations in the temperature, rather than eliminate all fluctuations in the temperature.
You need to choose which is the best assumption for your particular application.
Here, we'll just look at one common means of holding the temperature constant, with an internal ``thermostat''.

\paragraph{Thermostat.}
To keep the temperature constant (or fluctuating around the expectation value), a ``thermostat'' is sometimes added to Newton's laws of motion.
One example is the \emph{Nos\'e-Hoover} thermostat, which modifies Newton's Laws of motion to give the following\footnote{This form of the Nos\'e-Hoover equations of motion is taken from Br\'azdov\'a and Bowler, \textit{Atomistic Computer Simulations}, 1st Edn, 2013. However, note that I believe they missed a factor of two in the equation for $d \zeta /dt$, which I added.} three differential equations.
Note these equations are written in momentum coordinates rather than velocity coordinates, where $\vec{p}_i = m_i \vec{v}_i$.

\[ \frac{d \vec{r}_i}{dt} = \frac{\vec{p}_i}{m_i}
\]

\[ \frac{d \vec{p}_i}{dt} = \vec{F}_i - \zeta \vec{p}_i
\]

\[ \frac{d \zeta}{dt} = \frac{1}{Q} 
		  \left(
					 \sum_{i=1}^{N} \frac{p_i^2}{2 m_i} - (3N) \tfrac{1}{2} \kb T
		  \right)
\]

\noindent $\zeta$ is sometimes referred to as a friction coefficient, and acts to apply a ``force'' back towards the desired temperature $T$. There is a parameter $Q$ that is chosen by the user and controls the timescale of the thermostat response.
The temperature adjustment acts like a ``drag'' or friction force on the momentum (but can also accelerate, not just decelerate).
In this particular formulation, all atoms are dragged proportionately rather than randomly.

There are many other NVT strategies that may be chosen for particular problems, such as other thermostats or Langevin dynamics. Similar strategies exist to fix the pressure by adjusting the system volume during the simulation.

\subsection{Link to fundamental equations}

\paragraph{Reminder: Legendre transforms}
If you recall your classical thermodynamics, you probably alread recognize that the link between the ensembles looks a lot like a Legendre transform.
As a reminder, the first time we see this is in classical thermodynamics is in the conversion of the internal energy, $E$ (usually referred to as $U$) to the enthalpy, $H$, and later to the Gibbs or Helmholtz free energies, $G$ and $A$.
Starting with $U(S,V,N)$ and its fundamental equation:

\[ U(S,V,N) \]

\[ dU = T \, dS - P \, dV + \mu dN \]

\noindent
The point of $H$ is to make it depend upon pressure, not volume, so a Legendre transform is created as $H \equiv U + PV$.
Taking the differential and plugging into the fundamental equation:

\[ dH = dU + P dV + V dP \]

\[ dH = T\, dS - P \, dV + \mu \, dN + P \, dV + V \, dP \]

\[ dH = T\, dS + V \, dP + \mu \, dN  \]

\noindent
and we are left with a fundamental equation in $H(S,P,N)$; that is, we transformed the volume and pressure.

\paragraph{The NVE and NVT ensembles.}
Recall in classical thermodynamics that the fundamental equation was originally defined as $S(E,V,N)$ (here, we'll use $E$ instead of $U$ for consistency with the common nomenclature in atomistics).
It's only under the assumption of monotonicity\footnote{For reasonably sized systems with no upper limit on energy levels, this is almost always a great assumption.} between $S$ and $E$ that the functional form can properly be switched to $E(S,V,N)$; thus, we see that in the molecular dynamics ensembles we are dealing with the ``more fundamental'' form of the fundamental equation; that is,

\[ dS = \frac{1}{T} \, dE + \frac{P}{T} \, dV - \frac{\mu}{T} \, dN \]

\noindent
and this would correspond to the microcanonical ensemble.
We can adopt a similar Legendre transform to switch to the canonical (NVT) ensemble.
For convenience, let's let $ \beta \equiv 1 / T $:

\[ dS = \beta dE + \beta P  dV - \beta \mu dN \]

\noindent
We'll define a new potential $X$:

\[ X \equiv S - \beta E \]

\[ dX = dS - \beta dE - E d\beta \]

\[ dX = - E \, d\beta + \beta P \,  dV - \beta \mu \, dN \]

\[ \therefore X(\beta, V, N) \]

\noindent
Changing back to temperature,

\[ dX = - E \, d\left(\frac{1}{T}\right) + \frac{P}{T} \, dV - \frac{\mu}{T} \, dN \]

\[ dX
= + \frac{E}{T^2} \, dT + \frac{P}{T} \, dV - \frac{\mu}{T} \, dN \]

\[ \therefore X(T, V, N) \]

Note that this closely related to the Helmholtz free energy, in the same way that $U$ and $S$ were previously related:

\[ A \equiv E - TS \]

\[ X = S - \frac{E}{T} \]

\[ \therefore A = - T \, X \]

\noindent
So equivalently, we tend to get Helmholtz free energy information out of canonical (NVT) simulations.


\section{Script(s)}

An example of an NVE simulation in ASE is shown below.
Note there is extensive documentation on the ASE website about how to run simulations in other ensembles, such as NVT or NVP.

\pcode{f/scripts/moldyn/run.py}

\endgroup
